function EditView(data, token, type) {
    //console.log(data);
    token = $.session.get('token')
    var id;
    var DocumentIds = [
        "0201042", "0201012", "0501021",
        '0201071', "0201011", "0201021",
        "0201022", "0201031", "0201032",
        "0201041", "0201051", "0201052",
        "0201062", "0201061",

    ];
    // if (data.Data.FormId == "0201042" || data.Data.FormId == "0201012" || data.Data.FormId == "0501021" || data.Data.FormId == '0201071' || data.Data.FormId == "0201011" || data.Data.FormId == "0201021" || data.Data.FormId == "0201022" || data.Data.FormId == "0201031" ||
    //   data.Data.FormId == "0201032" || data.Data.FormId == "0201041" || data.Data.FormId == "0201051" || data.Data.FormId == "0201052" || data.Data.FormId == "0201062" || data.Data.FormId == "0201061") {
    if (DocumentIds.indexOf(data.Data.FormId) != -1) {

        id = data.Data.Object.Document.id;

    } else {

        id = data.Data.Object.id;

    }
    //console.log(data.Data.Object);
    open_item = data;
    openedObjed = data;

    $.ajax({

        "url": "http://" + ip + "/EAkimat/edit/" + type + "/?id=" + id,

        cache: false,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("content-type", "application/json");
            xhr.setRequestHeader("Token", token);
        },
        success: function (data2) {

            $('.forcontent').html('');

            $.ajax({
                url: "views/" + data2.Data.FormId + ".html",
                cache: false,
                async: false,
                success: function (view) {

                    $('.forcontent').append(view);

                    genBreads(items)

                    if (data2.Data.FormId == '0201012' || data2.Data.FormId == '0201052') {

                        //$('.filegroup').show();
                        $('.filegroup #exampleInputFile').prop("required", false);
                    }
                    if (data2.Data.FormId == '0501022' || data2.Data.FormId == '0201062') {
                        $('.filegroup #exampleInputFile').remove();
                    }
                    Ribbon(data2, '.ribbon');
                    LoadRef();
                    LoadOutputRef();

                    if ($('#kladr')) {

                        getKladr('#kladr', '#kladr-bread span');

                    }
                    var token = $.session.get('token');
                    /*Номенклатура*/
                    //console.log(data.Data);
                    if (data.Data.FormId == "0103021"){
                        if ($('#Community').length) {

                            $('.CommunitySelect').hover(
                                function () {

                                    $('#CommunityList').show();

                                },
                                function () {
                                    $('#CommunityList').hide();
                                })

                            var L;
                            var token = $.session.get('token')
                            $.ajax({
                                url: 'http://' + ip + '/EAkimat/Department.List',
                                async: false,
                                method: 'post',
                                data: JSON.stringify({
                                    "onlyBusy": true,
                                    "ids": null,
                                    "filter": "",
                                    "include_enterprise": true, 
                                    "del_rec": false
                                }),
                                xhrFields: {
                                    withCredentials: true
                                },
                                beforeSend: function (xhr) {
                                    xhr.setRequestHeader("content-type", "application/json");
                                    xhr.setRequestHeader("Token", token);
                                },
                                success: function (data) {
                                    L = data.data;
                                    $('#CommunityList').jstree({
                                        'core': {
                                            'data': L,
                                            'multiple': false
                                        }
                                    })
                                }
                            })
                            $('#CommunityList').on('changed.jstree', function (e, data) {

                                if (data.selected[0] != null)
                                    if (data.instance.get_node(data.selected[0]).state.disabled == false) {
                                        $('#CommunityValue').val(data.instance.get_node(data.selected[0]).text);
                                        $('#Community').val(data.selected[0]);
                                    }
                            })

                        }
                       if ($('#DocumentType').length) {
                            var opt = '<option value="">Выбрать</option>';
                            $.each(reference.doctype, function (index, el) {
                                if(el['Value'] != 1){
                                    opt += '<option value="' + el['Value'] + '">' + el['Name'] + '</option>';
                                }
                            });
                            $('#DocumentType').html(opt);
                            $('#DocumentType').chosen();
                        }
                        if ($('#Counter').length) {
                            $('#Counter').chosen();
                            var DocType = data.Data.Object.DocumentType;
                            $('#DocumentType').on('change', function(){
                                DocType = $(this).val();
                                countSelect();
                            });
                            function countSelect(){
                                if(DocType){
                                    $.ajax({
                                        "url": "http://" + ip + "/EAkimat/Chancellery.Counter.Select?documentType=" + DocType,
                                        async: false,
                                        method: 'get',
                                        data: JSON.stringify({
                                            "onlyBusy": true,
                                            "ids": null,
                                            "filter": ""
                                        }),
                                        xhrFields: {
                                            withCredentials: true
                                        },
                                        beforeSend: function (xhr) {
                                            xhr.setRequestHeader("content-type", "application/json");
                                            xhr.setRequestHeader("Token", token);
                                        },
                                        success: function (data2) {
                                            dataSel = data2.Data.Object.Data;
                                            var opt = '<option value="">Выбрать</option>';
                                            $.each(dataSel, function (index, el) {
                                                opt += '<option value="' + el[0] + '">' + el[2] + '</option>';
                                            });
                                            $('#Counter').html(opt);
                                            $('#Counter').prop("disabled", false).trigger("chosen:updated");
                                        }
                                    })
                                }else{
                                    $('#Counter').prop("disabled", true).trigger("chosen:updated");  
                                }
                            }
                            countSelect();
                        }
                    }
                    /**/
                    if ($('#SignerID').length) {
                        $.ajax({
                            "url": "http://" + ip + "/EAkimat/Workplace.List",
                            async: false,
                            method: 'post',
                            data: JSON.stringify({
                                "onlyBusy": true,
                                "ids": null,
                                "filter": ""
                            }),
                            xhrFields: {
                                withCredentials: true
                            },
                            beforeSend: function (xhr) {
                                xhr.setRequestHeader("content-type", "application/json");
                                xhr.setRequestHeader("Token", token);
                            },
                            success: function (data2) {
                                var opt = '<option value="">Выбрать</option>';
                                $.each(data2.data, function (index, el) {
                                    opt += '<option data-w="' + el[0] + '" value="' + el[2] + '">' + el[1] + '</option>';
                                });
                                $('#SignerID').html(opt);
                                $('#SignerID').chosen();

                            }
                        })
                    }
                    if ($('#Recipients').length) {

                        $('.save-rec').on('click', function () {

                            var access = [];
                            var tr = '';

                            $('#access-view option').each(function (i, v) {

                                access.push({
                                    "Id": $(v).val(),
                                    "Name": $(v).text()
                                });
                                tr += '<div>' + $(v).text() + '</div>';

                            })

                            $('#Recipients').val(JSON.stringify(access));
                            $('#RecipientListTable').html(tr);

                        })

                    }
                    if ($('#Kontragent').length) {

                        var token = $.session.get("token");

                        $.ajax({
                            "url": " http://" + ip + "/EAkimat/Chancellery.Contractor.Select",
                            async: false,
                            method: 'post',
                            data: JSON.stringify({
                                "param": {
                                    "ContractorType": 0,
                                    "OnlyOuter": false
                                }
                            }),
                            beforeSend: function (xhr) {
                                xhr.setRequestHeader("content-type", "application/json");
                                xhr.setRequestHeader("Token", token);
                            },
                            success: function (data) {

                                var options = '';

                                $.each(data.Data.Object.Data, function (key, val) {

                                    options += '<option value="' + val[0] + '">' + val[1] + '</option>';

                                })
                                $('#Kontragent').html(options)
                                $('#Kontragent').chosen();
                                $('#Kontragent').val('');


                            }
                        })
                    }
                    if ($('#HigherAgreement').length) {
                        var token = $.session.get("token");

                        $.ajax({
                            "url": " http://" + ip + "/EAkimat/Chancellery.Contractor.Select",
                            async: false,
                            method: 'post',
                            data: JSON.stringify({
                                "param": {
                                    "ContractorType": 0,
                                    "OnlyOuter": false
                                }
                            }),
                            beforeSend: function (xhr) {
                                xhr.setRequestHeader("content-type", "application/json");
                                xhr.setRequestHeader("Token", token);
                            },
                            success: function (data) {

                                var options = '';

                                $.each(data.Data.Object.Data, function (key, val) {

                                    options += '<option value="' + val[0] + '">' + val[1] + '</option>';

                                })
                                $('#HigherAgreement').html(options)
                                $('#HigherAgreement').chosen();
                                $('#HigherAgreement').val('');

                            }
                        })
                    }
                    if ($('#Nomenclature').length) {
                        var token = $.session.get('token')
                        $.ajax({
                            url: 'http://' + ip + '/EAkimat/Nomenclature.List',
                            async: false,
                            xhrFields: {
                                withCredentials: true
                            },

                            beforeSend: function (xhr) {
                                xhr.setRequestHeader("content-type", "application/json");
                                xhr.setRequestHeader("Token", token);
                            },
                            success: function (data) {

                                $.each(data.Data.Object.Data, function (i, el) {
                                    $('#Nomenclature').append('<option value="' + el[0] + '">' + el[1] + "</option>");
                                });

                                $('#Nomenclature').chosen();
                            }
                        })

                    }
                    
                    if ($('#Department').length) {

                        $('.departmentSelect').hover(
                            function () {

                                $('#DepartmentList').show();

                            },
                            function () {
                                $('#DepartmentList').hide();
                            })

                        var D;
                        $.ajax({
                            url: 'http://' + ip + '/EAkimat/Department.List',
                            type: 'POST',
                            xhrFields: {
                                withCredentials: true
                            },
                            beforeSend: function (xhr) {
                                xhr.setRequestHeader("content-type", "application/json");
                                xhr.setRequestHeader("Token", token);
                            },
                            success: function (data) {

                                D = data.data;

                                $('#DepartmentList').jstree({
                                    'core': {
                                        'data': D,
                                        'multiple': false
                                    }
                                })

                            },
                            error: function (error) {

                            }
                        })
                        $('#DepartmentList').on('changed.jstree', function (e, data) {
                            if (data.selected[0] != null) {
                                $('#Department').val(data.instance.get_node(data.selected[0]).text);
                                $('#DepartmentId').val(data.selected[0]);
                            }
                        })


                    }
                    $('#Code').on('keyup', function () {
                        $(this).val($(this).val().replace(/^([^\d]{0,12})?$/g, ''));
                        if ($(this).val().search(/^([\d]{0,12})?$/) == -1) {
                            $(this).val($(this).val().substring(0, $(this).val().length - 1))
                        }
                    })
                    if ($('#Folder').length) {

                        $("#folderListOpenBtn").on('click', function(){
                            if($('#FolderList').html() == ""){
                                $('.ig-catalog-loader').addClass("igcl");
                                var L;
                                var token = $.session.get('token')
                                $.ajax({
                                    url: 'http://' + ip + '/EAkimat/User.Folder.List',
                                    async: true,
                                    xhrFields: {
                                        withCredentials: true
                                    },
                                    beforeSend: function (xhr) {
                                        xhr.setRequestHeader("content-type", "application/json");
                                        xhr.setRequestHeader("Token", token);
                                    },
                                    success: function (data) {

                                        L = data.Data.Object.data;

                                        $('#FolderList').jstree({
                                            'core': {
                                                'data': L,
                                                'multiple': false
                                            }
                                        });
                                        $('.ig-catalog-loader').removeClass("igcl");
                                        $('#FolderList').show();
                                    }
                                });
                            } else {
                                $('#FolderList').show();
                            }
                        });    

                        $('#FolderList').on('changed.jstree', function (e, data) {

                            if (data.selected[0] != null)
                                if (data.instance.get_node(data.selected[0]).state.disabled == false) {
                                    $('#FolderValue').val(data.instance.get_node(data.selected[0]).text);
                                    $('#Folder').val(data.selected[0]);
                                    $('#FolderList').hide();
                                }
                        })

                    }
                    if ($('#access-select').length) {

                        AccessList();

                    }
                    if ($('#AccessList2').length) {
                        AccessList2(openedObjed.Data.Object.SelectedEmsObjectsId, 2);
                        $(document).off('click', '#access-view2 option').on('click', '#access-view2 option', function () {
                            var group = $(this).data('group');
                            if ($('.access-select' + id).children('optgroup[label=' + group + ']').length) {
                                $(this).appendTo('.access-select2 optgroup[label=' + group + ']');
                            } else {
                                $('.access-select' + id).append('<optgroup label="' + group + '"></optgroup>');
                                $(this).appendTo('.access-select2 optgroup[label=' + group + ']');
                            }

                        })

                        $(document).off('click', '#access-select2 option').on('click', '#access-select2 option', function () {
                            var group = $(this).data('group');
                            var id = $(this).parent().parent().data('key');
                            if ($('.access-view' + id).children('optgroup[label=' + group + ']').length) {
                                $(this).appendTo('.access-view2 optgroup[label=' + group + ']');
                            } else {
                                $('.access-view' + id).append('<optgroup label="' + group + '"></optgroup>');
                                $(this).appendTo('.access-view2 optgroup[label=' + group + ']');
                            }

                        })
                        $(document).on('click', '.save-access2', function () {
                            var id = $(this).data('key');
                            var data = [];
                            $('.access-view2 option').each(function (i, v) {

                                data.push([
                                    $(v).val(),
                                    $(v).text(),
                                    $(v).parent().attr('label'),
                                    $(v).data('v')
                                ]);

                            })

                            $('#AccessList2').val(JSON.stringify(data));


                            $.ajax({
                                url: "http://" + ip + "/EAkimat/Documents.Resolution.NewExecuterList",

                                method: 'POST',
                                data: JSON.stringify({
                                    "resolutionId": openedObjed.Data.Object.ResolutionID,
                                    "taskId": openedObjed.Data.Object.id,
                                    "executerDataList": {
                                        "__type": "DataList:#Avrora.Objects.Web",
                                        "Data": data
                                    }
                                }),
                                beforeSend: function (xhr) {
                                    xhr.setRequestHeader("content-type", "application/json");
                                    xhr.setRequestHeader("Token", token);
                                },
                                success: function (data2) {
                                    $('#NewExecuterList2').val(JSON.stringify(data2.Data.Object));
                                    var nummpp = 0;
                                    var tr = '';
                                    $.each(data2.Data.Object, function (k, v) {
                                        nummpp++;
                                        tr += '<tr>' +
                                            '<td>' + nummpp + '</td>' +
                                            '<td>' + v.ExecuterName + '</td>' +
                                            '<td>' + v.EnterpriseName + '</td>' +
                                            '<td>' + getDataFromRef("WorkStatus", v.WorkStatus) + '</td>' +
                                            '<td>';
                                        if (v.CompleteDate != null) {
                                            tr += DateFromMSDate(v.CompleteDate);
                                        }

                                        tr += '</td>' +
                                            '<td><input id="' + v.id + '" value="' + nummpp + '" type="radio" ' + (v.Svod ? 'checked' : '') + ' name="r"></td>' +
                                            '</tr>';

                                    })

                                    $('#AccessListTable2').html(tr);
                                    //console.log( tr)

                                }
                            })


                            // $('#AccessList'+id).val(JSON.stringify(access));
                            // $('#AccessListTable'+id).html(tr);

                        })
                    }
                    if ($('#recipient-select').length) {

                        RecipientsList();

                        $('#entity').on('click', function (event) {
                            RecipientsList(0, $('#ESEDO').is(':checked'))
                        })
                        $('#individual').on('click', function () {
                            RecipientsList(1, $('#ESEDO').is(':checked'))
                        })
                        $('#applicant').on('click', function () {
                            RecipientsList(2, $('#ESEDO').is(':checked'))
                        })
                        $('#ESEDO').on('click', function () {
                            if ($('#ESEDO').is(':checked')) {
                                $('#ESEDO').parent().addClass('active')
                            } else {
                                $('#ESEDO').parent().removeClass('active')
                            }
                            RecipientsList($('input[type=radio]:checked').data('count'), $('#ESEDO').is(':checked'))
                        })
                        $('#recipient-search').on('keyup', function () {
                            var findData = "";
                            $.each($('#recipient-select option'), function (index, el) {
                                if ($(el).text().toLowerCase().indexOf($('#recipient-search').val().toLowerCase()) != -1)
                                    $(el).show();
                                else
                                    $(el).hide();
                                // findData+= '<option value="'+el.value+'"> '+el.text+'</option>';
                            });
                            // $('#recipient-search').html(findData);
                        })


                    }
                    if ($('#documentation-select').length) {
                        DocumentationList();
                    }
                    $('.breadcrumbs  li a').on('click', function () {

                        var view = $(this).data('action');
                        $.ajax({
                            url: "views/" + view + ".html",
                            cache: false,
                            success: function (data) {

                                $('.forcontent').html(data);

                            }

                        })

                    })

                    var obj = '';
                    // var DocumentIds = [
                    //   "0201042","0201012","0501021",
                    //   '0201071',"0201011","0201021",
                    //   "0201022","0201031","0201032",
                    //   "0201041","0201051","0201052",
                    //   "0201062","0201061",

                    // ];

                    // if (data.Data.FormId == "0501021" || data.Data.FormId == "0201071" || data.Data.FormId == "0201011" || data.Data.FormId == "0201021" || data.Data.FormId == "0201022" || data.Data.FormId == "0201052" ||
                    // data.Data.FormId == "0201031" || data.Data.FormId == "0201041" || data.Data.FormId == "0201051" || data.Data.FormId == "0201061") {
                    if (DocumentIds.indexOf(data.Data.FormId) != -1) {
                        obj = data.Data.Object.Document;
                        var pages = data.Data.Object.Pages;
                        var tabs = '';
                        var tab_content = '';

                        $.each(pages, function (k, v) {

                            v.load = false;

                        })
                        $.each(pages, function (k, v) {
                            var it = k + 2;

                            tabs += '<li role="presentation">' +
                                '<a aria-expanded="true" href="#tab' + it + '" data-item="' + k + '" aria-controls="tab' + it + '" role="tab" data-form="' + v.FormId + '" data-toggle="tab">' + v.Name + '</a>' +
                                '<span class="count">' + v.Count + '</span>' +
                                '</li>';

                            tab_content += '<div id="tab' + it + '" class="tab-pane" role="tabpanel">' + it + '</div>'

                        })

                        $('#tabs').append(tabs);
                        $('.tab-content').append(tab_content);

                        var forms = {
                            "0202010": "Attachment",
                            "0202050": "Signature",
                            "0202060": "History",
                            "0202070": "Link",
                            "0202030": "Resolution"
                        };

                        $('#tabs').on('click', 'li a', function () {

                            if ($(this).data('form') && $(this).data('form') != '') {

                                var item = $(this).data('item');
                                var form = $(this).data('form');
                                var control = $(this).attr('aria-controls');

                                if (pages[item]['load'] == true) {} else {
                                    $.ajax({
                                        url: 'views/' + form + '.html',
                                        method: 'get',
                                        async: false,
                                        cache: false,
                                        success: function (data2) {

                                            $('.tab-content #' + control).html(data2);

                                            pages[item]['load'] = true;

                                            $.ajax({
                                                "url": "http://" + ip + "/EAkimat/Document." + forms[form] + ".List?id=" + data.Data.Object.Document.id,
                                                async: false,
                                                cache: false,
                                                xhrFields: {
                                                    withCredentials: true
                                                },
                                                beforeSend: function (xhr) {
                                                    xhr.setRequestHeader("content-type", "application/json");
                                                    xhr.setRequestHeader("Token", token);
                                                },
                                                success: function (data) {

                                                    var attachments = '';

                                                    var input;

                                                    if (form == '0202060') {

                                                        input = data.Data.Object.Data;
                                                        var groups = []

                                                        $.each(input, function (index, el) {
                                                            if (el[1] != '') {

                                                                var groupEx = -1;

                                                                $.each(groups, function (ind, elem) {
                                                                    if (el[1] == elem) {
                                                                        groupEx = ind;
                                                                    }
                                                                });
                                                                if (groupEx == -1) {
                                                                    groups.push(el[1])
                                                                    $('#history tbody').prepend('<tr colspan="4" class="rowGr" id="' + (groups.length - 1) + '"><td colspan="4">' +
                                                                        '<i style="width: 33px; display: inline-block; padding: 10px!important;" class="jstree-icon"></i><span>' + el[1] + '</span></td></tr>')
                                                                    $('#history tbody #' + (groups.length - 1)).after('<tr class="group' + (groups.length - 1) + '">' +
                                                                        '<td>' + el[1] + '</td>' +
                                                                        '<td>' + el[2] + '</td>' +
                                                                        '<td>' + el[3] + '</td>' +
                                                                        '<td>' + el[4] + '</td>')
                                                                    $('.group' + (groups.length - 1)).hide();
                                                                } else {
                                                                    $('#history tbody #' + groupEx).after('<tr class="group' + groupEx + '">' +
                                                                        '<td>' + el[1] + '</td>' +
                                                                        '<td>' + el[2] + '</td>' +
                                                                        '<td>' + el[3] + '</td>' +
                                                                        '<td>' + el[4] + '</td>')
                                                                    $('.group' + groupEx).hide();
                                                                }
                                                            } else {
                                                                $('#history tbody').append('<tr>' +
                                                                    '<td>' + el[1] + '</td>' +
                                                                    '<td>' + el[2] + '</td>' +
                                                                    '<td>' + el[3] + '</td>' +
                                                                    '<td>' + el[4] + '</td>')
                                                            }

                                                        });
                                                        $(document).off('click', '.rowGr').on('click', '.rowGr', function () {
                                                            // $('group'+$(this).attr('id')).toogle();
                                                            if ($(this).hasClass('rowGrOpen')) {
                                                                $(this).removeClass('rowGrOpen')
                                                                $('.group' + $(this).attr('id')).hide();
                                                            } else {
                                                                $('.group' + $(this).attr('id')).show();
                                                                $(this).addClass('rowGrOpen')
                                                            }
                                                        })


                                                    } else if (form == '0202070') {
                                                        input = data.Data.Object.Data;
                                                        $.each(input, function (k, v) {

                                                            var tds = '';

                                                            $.each(v, function (k, val) {

                                                                if (k != 0 && k != 7) {

                                                                    var str = '';

                                                                    if (val != null) {

                                                                        if (k == 6) {
                                                                            tds += '<td>' +
                                                                                '<a class="actionBtnList" data-action="open" data-type="Documents.Document" data-id="' + val + '">' +
                                                                                '<img title="Открыть" alt="Открыть" src="img/ui/open_inactive.png">' +
                                                                                '</a>';
                                                                            if (v[7] == 'True') {
                                                                                tds += '<a class="actionBtnList" style="padding-left: 16px;" data-action="deleteLink" data-type="Documents.Document" data-id="' + v[0] + '"><img title="Удалить" alt="Удалить" src="img/ui/delete_inactive.png"></a>'
                                                                            }
                                                                            tds += '</td>';
                                                                            str = '';

                                                                        } else {

                                                                            str = val;

                                                                        }

                                                                    }

                                                                    tds += '<td>' + str + '</td>';

                                                                }


                                                            })

                                                            attachments += '<tr>' + tds + '</tr>';
                                                        })

                                                        $('#link tbody').html(attachments);
                                                    } else {
                                                        if (data.data)
                                                            input = data.data;
                                                        else input = data.Data.Object.Data;


                                                        if (open_item.Data.FormId != '0501021')
                                                            $('#' + control + ' .table thead tr').children().first().remove();
                                                        $.each(input, function (k, v) {
                                                            var ind = k;
                                                            var tds = '';
                                                            var tp = false;
                                                            $.each(v, function (k, val) {

                                                                if (open_item.Data.FormId == '0501021')
                                                                    if (tp == false) {
                                                                        tds += '<td><select class="attach-type" id="attach-type' + ind + '">' + attachTypeGen(v[4]) + '</select></td>';
                                                                        tp = true;
                                                                    }
                                                                if (k != 0 && k != 4 && k != 1) {

                                                                    var str = '';

                                                                    if (val != null) {

                                                                        str = val;

                                                                    }

                                                                    tds += '<td>' + str + '</td>';

                                                                } else if (k == 1) {
                                                                    tds += '<td><a href="http://' + ip + '/EAkimat/Attachment?id=' + v[0] + '">' + val + '</a></td>';
                                                                    downloadHref = 'http://' + ip + '/EAkimat/Attachment?id=' + v[0];
                                                                }
                                                            })
                                                            tds += '<td class="action-cell"><div class="hide"><a class="actionBtnList" data-action="delete" data-type="Document" data-id="' + v[0] + '"><img title="Удалить" alt="Удалить" src="img/ui/delete_inactive.png"></a>' +
                                                                '<a href=' + downloadHref + ' style="margin-left: 20px" data-action="download" data-type="Document" data-id="' + v[0] + '"><img title="Скачать" alt="Скачать" src="img/ui/save.png"></a></div></td>';
                                                            attachments += '<tr data-id="' + v[0] + '">' + tds + '</tr>';
                                                        })

                                                        $('#' + control + ' .table tbody').html(attachments);
                                                        $('.attach-type').chosen({
                                                            width: '100%'
                                                        })

                                                    }

                                                    $('.table tbody').on('mouseenter', 'tr', function () {
                                                        $(this).find('.hide').addClass("show");
                                                        $(this).find('.show').removeClass("hide");
                                                    });

                                                    $('.table tbody').on('mouseleave', 'tr', function () {
                                                        $(this).find('.show').addClass("hide");
                                                        $(this).find('.hide').removeClass("show");
                                                    });

                                                    $('.table tbody').on('click', '.actionBtnList', function () {
                                                        var thisElement = $(this);
                                                        var thisTr = $(this).parent().parent().parent();
                                                        var action = $(this).data('action');
                                                        var id = $(this).data('id');

                                                        if (action == 'open') {
                                                            dialog.confirm({
                                                                title: "Предупреждение",
                                                                message: 'Вы действительно хотите закрыть текущий документ и перейти на связанный?',
                                                                cancel: "Нет",
                                                                button: "Да",
                                                                required: true,
                                                                callback: function (value) {
                                                                    if (value == true) {
                                                                        $.ajax({
                                                                            "url": "http://" + ip + "/EAkimat/" + action + "/Chancellery|Documents.Document/?id=" + id,
                                                                            cache: false,
                                                                            beforeSend: function (xhr) {
                                                                                xhr.setRequestHeader("content-type", "application/json");
                                                                                xhr.setRequestHeader("Token", token);
                                                                            },
                                                                            success: function (data) {
                                                                                if (data.Result == 'OK') {
                                                                                    $('.forcontent').html('');
                                                                                    openedObjed = data;
                                                                                    View(data);
                                                                                } else {
                                                                                    $.notify(data.Message, {
                                                                                        type: 'danger'
                                                                                    });

                                                                                }
                                                                            }
                                                                        })
                                                                    }
                                                                }
                                                            })
                                                        } else if (action == 'deleteLink') {
                                                            dialog.confirm({
                                                                title: "Предупреждение",
                                                                message: 'Вы действительно хотите удалить связь?',
                                                                cancel: "Нет",
                                                                button: "Да",
                                                                required: true,
                                                                callback: function (value) {
                                                                    if (value == true) {
                                                                        $.ajax({


                                                                            "url": "http://" + ip + "/EAkimat/Document/DeleteLink",
                                                                            cache: false,
                                                                            method: 'POST',
                                                                            data: JSON.stringify({
                                                                                "id": open_item.Data.Object.Document.id,
                                                                                "linkId": id
                                                                            }),
                                                                            beforeSend: function (xhr) {
                                                                                xhr.setRequestHeader("content-type", "application/json");
                                                                                xhr.setRequestHeader("Token", token);
                                                                            },
                                                                            success: function (data) {
                                                                                if (data.Result == 'OK') {
                                                                                    $.notify(data.Message, {
                                                                                        type: 'success'
                                                                                    })
                                                                                    $(thisElement).parents('tr').remove();
                                                                                } else {
                                                                                    $.notify(data.Message, {
                                                                                        type: 'danger'
                                                                                    })
                                                                                }
                                                                            }
                                                                        })
                                                                    }
                                                                }
                                                            })
                                                        } else {
                                                            var type = $(this).data('type');
                                                            var cancelAdjustment = false;
                                                            var isAdj = false;
                                                            var adj = open_item.Data.Object.IsAdjusted;

                                                            if (open_item.Data.Object.Document.is_new_record == false) {
                                                                if (adj == true) {

                                                                    // isAdj = confirm('Изменение списка вложений приведет к отмене всех согласований после сохранения документа! Продолжить?');
                                                                    // if (isAdj == true) {
                                                                    dialog.confirm({
                                                                        title: "Предупреждение",
                                                                        message: "Изменение списка вложений приведет к отмене всех согласований после сохранения документа! Продолжить?",
                                                                        cancel: "Нет",
                                                                        button: "Да",
                                                                        required: true,
                                                                        callback: function (value) {
                                                                            if (value == true) {

                                                                                cancelAdjustment = true;

                                                                                deleteAdj(action, cancelAdjustment)

                                                                            }
                                                                        }
                                                                    })


                                                                    // }
                                                                } else {
                                                                    dialog.confirm({
                                                                        title: "Предупреждение",
                                                                        message: "Удалить вложение?",
                                                                        cancel: "Нет",
                                                                        button: "Да",
                                                                        required: true,
                                                                        callback: function (value) {
                                                                            if (value == true) {
                                                                                deleteAdj(action);
                                                                            }
                                                                        }
                                                                    })

                                                                }
                                                            } else {
                                                                attach = openedObjed.Data.Object.Document.Attachments;
                                                                if (attach.length > 1) {

                                                                    dialog.confirm({
                                                                        title: "Предупреждение",
                                                                        message: "Удалить вложение?",
                                                                        cancel: "Нет",
                                                                        button: "Да",
                                                                        required: true,
                                                                        callback: function (value) {
                                                                            if (value == true) {
                                                                                $.each(attach, function (ind, el) {
                                                                                    if (el.id == id) {
                                                                                        attach.splice(ind, 1)
                                                                                        $(thisTr).remove();
                                                                                        $('.nav-tabs li.active .count').text(Number($('.nav-tabs li.active .count').text()) - 1)
                                                                                    }
                                                                                })
                                                                            }
                                                                        }
                                                                    })


                                                                } else {
                                                                    $.notify('В документе должно быть хотя бы одно вложение!', {
                                                                        type: 'danger'
                                                                    })
                                                                }
                                                            }

                                                            function deleteAdj(action, cancelAdjustment) {
                                                                if (typeof cancelAdjustment == "undefined") {
                                                                    cancelAdjustment = false
                                                                }
                                                                $.ajax({
                                                                    "url": "http://" + ip + "/EAkimat/" + action + "/Attachment/?id=" + id + "&cancelAdjustment=" + cancelAdjustment,
                                                                    beforeSend: function (xhr) {
                                                                        xhr.setRequestHeader("content-type", "application/json");
                                                                        xhr.setRequestHeader("Token", token);
                                                                    },
                                                                    success: function (res) {

                                                                        if (action == 'delete') {

                                                                            if (res.Result == 'OK') {

                                                                                $.notify(res.Message, {
                                                                                    type: "success"
                                                                                });
                                                                                $.ajax({
                                                                                    "url": "http://" + ip + "/EAkimat/Document.Attachment.List?id=" + open_item.Data.Object.Document.id,
                                                                                    async: false,
                                                                                    cache: false,
                                                                                    xhrFields: {
                                                                                        withCredentials: true
                                                                                    },
                                                                                    beforeSend: function (xhr) {
                                                                                        xhr.setRequestHeader("content-type", "application/json");
                                                                                        xhr.setRequestHeader("Token", token);
                                                                                    },
                                                                                    success: function (data) {

                                                                                        var attachments = '';
                                                                                        var input;

                                                                                        input = data.Data.Object.Data;

                                                                                        var downloadHref = '';

                                                                                        attachments = setAttachments(input, 'tab2', 'update')

                                                                                        $('#tab2 .table tbody').html(attachments);
                                                                                        $('.tab2').html(input.length);

                                                                                    }
                                                                                })

                                                                            } else {

                                                                                $.notify("Ошибка удаления данных: " + res.Message, {
                                                                                    type: "danger"
                                                                                });

                                                                            }

                                                                        }


                                                                    }
                                                                })
                                                            }
                                                        }

                                                    })





                                                }
                                            })

                                        }
                                    })

                                }

                            }

                        })

                    } else {

                        obj = data.Data.Object;


                    }

                    var Gcount = 0;

                    if(data.Data.AdditionalObjects != null){
                        $.each(data.Data.AdditionalObjects[0], function (k, v) {
                            if (k == 'IsMinisterstvo' || k == 'IsOblOrgan' || k == 'IsRegionAkimat' || k == 'IsOblBusiness') {
                                $('#' + k).prop('checked', v);
                            }
                        });    
                    }
                    
                    $.each(obj, function (k, v) {

                        // if ($('#' + k) && k != 'ContactList' && k != 'IsResident' && k != 'AddressId' && k != 'Type' && k != 'Sector' && k != 'GuitEnterprise' && k != 'INN' &&
                        //   k != 'BirthDate' && k != 'Year' && k != 'DocumentType' && k != 'RegistrationPlace' && k != 'Template' && k != 'AccessList' &&
                        //   k != 'Card' && k != 'Nomenclature' && k != 'Counter' && k != 'Folder' && k != 'CreateDate' && k != 'ExecuterName' && k != 'SignerName' && k != 'CounterReserve' && k != 'ResolutionDate' && k != 'Document' && k != 'Tasks' && k != 'SignerID' 
                        // && k != 'DocumentStatus' && k != 'WorkStatus' && k != 'Resolution' && k != 'InnerLimit' && k != 'Executers') {

                        //   if (v) {

                        //     $('#' + k).val(v);

                        //   }

                        // } else
                        if (k == 'SignerID') {

                            $('#' + k).val(v);
                            $('#' + k).trigger("chosen:updated");

                        } else if(k == 'CounterId' && data.Data.FormId == "0103021"){
                            $('#Counter').val(v);
                            $('#Counter').trigger("chosen:updated");
                        } else if (k == 'IsClosed') {
                            $('#' + k).prop('checked', v);

                        } else if (k == 'IsTransit') {
                            $('#' + k).prop('checked', v);

                        } else if (k == 'Executers') {
                            $('#NewExecuterList2').val(JSON.stringify(v))
                            var tr = '';
                            var nummpp = 0;

                            $.each(v, function (key, val) {
                                nummpp++;
                                tr += '<tr>' +
                                    '<td>' + nummpp + '</td>' +
                                    '<td>' + val.ExecuterName + '</td>' +
                                    '<td>' + val.EnterpriseName + '</td>' +
                                    '<td>' + getDataFromRef("WorkStatus", val.WorkStatus) + '</td>' +
                                    '<td>';
                                if (val.CompleteDate != null) {
                                    tr += DateFromMSDate(val.CompleteDate);
                                }

                                tr += '</td>' +
                                    '<td><input id="' + val.id + '"value="' + nummpp + '" type="radio" ' + (val.Svod ? 'checked' : '') + ' name="r"></td>' +
                                    '</tr>';

                            })
                            $('#AccessListTable2').html(tr);
                        } else if (k == 'InnerLimit') {
                            if (v)
                                $('#' + k).val(DateFromMSDateForForm(v))
                        } else if (k == 'DocumentStatus' || k == 'WorkStatus') {
                            $.each(reference.Status, function (index, el) {
                                if (v == el.Value) {
                                    $('#' + k).val(el.Description)
                                }
                            });
                        } else if (k == 'Resolution') {

                            $('#DocumentNumber').text(v.DocumentNumber)

                        } else if (k == 'Tasks') {
                            if (open_item.Data.FormId == '0401022' || open_item.Data.FormId == '0401021') {

                                var taskState;
                                $.each(v, function (i, e) {
                                    var obb = obj;
                                    TaskParse(i, e, obj, "0401022");

                                    $.ajax({
                                        "url": "http://" + ip + "/EAkimat/Workplace.List",
                                        async: false,
                                        method: 'post',
                                        data: JSON.stringify({
                                            "onlyBusy": true,
                                            "ids": null,
                                            "filter": ""
                                        }),
                                        xhrFields: {
                                            withCredentials: true
                                        },
                                        beforeSend: function (xhr) {
                                            xhr.setRequestHeader("content-type", "application/json");
                                            xhr.setRequestHeader("Token", token);
                                        },
                                        success: function (data2) {
                                             var opt = '<option value="">Выбрать</option>';
                                            //var opt = '';
                                            $.each(data2.data, function (index, el) {
                                                opt += '<option data-w="' + el[0] + '" value="' + el[2] + '" ' + (el[2] == e.ControlerEmployeeId ? 'selected' : '') + '>' + el[1] + '</option>';
                                            });
                                            $('#tab' + i + ' #ControlerName').html(opt);
                                            $('#tab' + i + ' #ControlerName').chosen();

                                        }
                                    })

                                })
                                if (v.length <= 1) {
                                    $('.del-task').remove();
                                }
                                LoadOutputRef();
                                refToSelect();
                                $(document).on('click', '#access-view option', function () {
                                    var group = $(this).data('group');
                                    var id = $(this).parent().parent().data('key');
                                    if ($('.access-select' + id).children('optgroup[label=' + group + ']').length) {
                                        $(this).appendTo('.access-select' + id + ' optgroup[label=' + group + ']');
                                    } else {
                                        $('.access-select' + id).append('<optgroup label="' + group + '"></optgroup>');
                                        $(this).appendTo('.access-select' + id + ' optgroup[label=' + group + ']');
                                    }
                                    // $(this).remove();
                                    // $(this).appendTo("#access-select");

                                })

                                $(document).on('click', '#access-select option', function () {
                                    var group = $(this).data('group');
                                    var id = $(this).parent().parent().data('key');
                                    if ($('.access-view' + id).children('optgroup[label=' + group + ']').length) {
                                        $(this).appendTo('.access-view' + id + ' optgroup[label=' + group + ']');
                                    } else {
                                        $('.access-view' + id).append('<optgroup label="' + group + '"></optgroup>');
                                        $(this).appendTo('.access-view' + id + ' optgroup[label=' + group + ']');
                                    }
                                    // $(this).remove();
                                    // $(this).appendTo("#access-view");

                                })
                            } else {


                                var taskState;
                                $.each(v, function (i, e) {
                                    var obb = obj;
                                    TaskParse(i, e, obj, '0205022');
                                    var token = $.session.get('token')
                                    $.ajax({
                                        "url": "http://" + ip + "/EAkimat/Workplace.List",
                                        async: false,
                                        method: 'post',
                                        data: JSON.stringify({
                                            "onlyBusy": true,
                                            "ids": null,
                                            "filter": ""
                                        }),
                                        xhrFields: {
                                            withCredentials: true
                                        },
                                        beforeSend: function (xhr) {
                                            xhr.setRequestHeader("content-type", "application/json");
                                            xhr.setRequestHeader("Token", token);
                                        },
                                        success: function (data2) {
                                             var opt = '<option value="">Выбрать</option>';
                                            //var opt = '';
                                            $.each(data2.data, function (index, el) {
                                                opt += '<option data-w="' + el[0] + '" value="' + el[2] + '">' + el[1] + '</option>';
                                            });
                                            $('#tab' + i + ' #ControlerName').html(opt);
                                            $('#tab' + i + ' #ControlerName').chosen();

                                        }
                                    })

                                })
                                if (v.length <= 1) {
                                    $('.del-task').remove();
                                }
                                LoadOutputRef();
                                refToSelect();
                                $.each($('select'), function (ind, el) {
                                    if ($(el).data('default')) {
                                        $(el).val($(el).data('default') ? $(el).data('default') : null)
                                        $(el).trigger('chosen:updated');
                                    }
                                })
                                $(document).off('click', '.del-task');
                                $(document).on('click', '.del-task', function () {
                                    var thisElem = $(this);
                                    var id = $(thisElem).data('id')
                                    var action = $(thisElem).data('action');
                                    if (action == 'delete') {

                                        dialog.confirm({
                                            title: "Предупреждение",
                                            message: "Вы уверены, что хотите удалить пункт?",
                                            cancel: "Нет",
                                            button: "Да",
                                            required: true,
                                            callback: function (value) {
                                                if (value == true) {
                                                    $('.AL2Links li').removeClass('active');
                                                    $.each($('.AL2Links li'), function (k, v) {
                                                        if (k == 0) {
                                                            $(v).addClass('active');
                                                            var id = $(v).find('a').data('id');
                                                            $('.AL2Tabs #tab' + id).show();
                                                        }
                                                    })

                                                    if (obj.Tasks[id]) {
                                                        obj.Tasks[id].DelRec = true;
                                                    }

                                                    $(thisElem).parent().remove();
                                                    $('#tab' + id).remove();
                                                    if ($('.tab-pane').length == 1) {
                                                        $('.del-task').remove();
                                                    }
                                                }
                                            }
                                        })


                                    }

                                });



                                $(document).on('click', '#access-view option', function () {
                                    var group = $(this).data('group');
                                    var id = $(this).parent().parent().data('key');
                                    if ($('.access-select' + id).children('optgroup[label=' + group + ']').length) {
                                        $(this).appendTo('.access-select' + id + ' optgroup[label=' + group + ']');
                                    } else {
                                        $('.access-select' + id).append('<optgroup label="' + group + '"></optgroup>');
                                        $(this).appendTo('.access-select' + id + ' optgroup[label=' + group + ']');
                                    }
                                    // $(this).remove();
                                    // $(this).appendTo("#access-select");

                                })

                                $(document).on('click', '#access-select option', function () {
                                    var group = $(this).data('group');
                                    var id = $(this).parent().parent().data('key');
                                    if ($('.access-view' + id).children('optgroup[label=' + group + ']').length) {
                                        $(this).appendTo('.access-view' + id + ' optgroup[label=' + group + ']');
                                    } else {
                                        $('.access-view' + id).append('<optgroup label="' + group + '"></optgroup>');
                                        $(this).appendTo('.access-view' + id + ' optgroup[label=' + group + ']');
                                    }
                                    // $(this).remove();
                                    // $(this).appendTo("#access-view");

                                })

                            }
                            // var taskState;

                            // $.each(v,function(i, e) {
                            //   //console.log(v)
                            //   var obb = obj;
                            //   TaskParse(i,e, obj, "0205022");
                            //   //
                            //     $.ajax({
                            //       "url": "http://" + ip + "/EAkimat/Workplace.List",
                            //       async: false,
                            //       method: 'post',
                            //       data: JSON.stringify({
                            //         "onlyBusy": true,
                            //         "ids": null,
                            //         "filter": ""
                            //       }),
                            //       xhrFields: {
                            //         withCredentials: true
                            //       },
                            //       beforeSend: function (xhr) {
                            //         xhr.setRequestHeader("content-type", "application/json");
                            //         xhr.setRequestHeader("Token", token);
                            //       },
                            //       success: function (data2) {
                            //         var opt = '<option value="">Выбрать</option>';

                            //         $.each(data2.data, function (index, el) {
                            //           var sel='';
                            //           //console.log(v)
                            //           if (el[2] == e.ControlerEmployeeId) {
                            //             sel = 'selected';
                            //           }
                            //           opt += '<option '+sel+' data-w="'+el[0]+'" value="' + el[2] + '">' + el[1] + '</option>';
                            //         });
                            //         $('#tab'+i+' #ControlerName').html(opt);
                            //         //$('#tab'+i+' #ControlerName').chosen();

                            //       }
                            //     })

                            // })
                            // if (v.length <= 1) {
                            //   $('.del-task').remove();
                            // }
                            // LoadOutputRef();
                            // refToSelect();
                            // $.each($('select'), function(ind, el) {
                            //   if ($(el).data('default')) {
                            //     $(el).val($(el).data('default')?$(el).data('default') : null)
                            //     $(el).trigger('chosen:updated');
                            //   }
                            // })
                            // $(document).off('click','.del-task');
                            //   $(document).on('click','.del-task', function() {
                            //     var thisElem = $(this);
                            //     var id = $(thisElem).data('id')
                            //     var action = $(thisElem).data('action');
                            //     if (action == 'delete') {

                            //       dialog.confirm({
                            //         title: "Предупреждение",
                            //         message: "Вы уверены, что хотите удалить пункт?",
                            //         cancel: "Нет",
                            //         button: "Да",
                            //         required: true,
                            //         callback: function (value) {
                            //           if (value == true) {
                            //             $('.AL2Links li').removeClass('active');
                            //             $.each($('.AL2Links li'), function(k, v){
                            //               if (k == 0) {
                            //                 $(v).addClass('active');
                            //                 var id = $(v).find('a').data('id');
                            //                 $('.AL2Tabs #tab'+id).show();
                            //               }
                            //             })

                            //             if (obj.Tasks[id]) {
                            //               obj.Tasks[id].DelRec = true;
                            //             }

                            //             $(thisElem).parent().remove();
                            //             $('#tab'+id).remove();
                            //             if ($('.tab-pane').length == 1) {
                            //               $('.del-task').remove();
                            //             }
                            //           }
                            //         }
                            //       })


                            //     }

                            //   });



                            //   $(document).on('click', '#access-view option', function () {
                            //     var group = $(this).data('group');
                            //     var id = $(this).parent().parent().data('key');
                            //     if ($('.access-select'+id).children('optgroup[label='+group+']').length) {
                            //       $(this).appendTo('.access-select'+id+' optgroup[label='+group+']');
                            //     } else {
                            //       $('.access-select'+id).append('<optgroup label="'+group+'"></optgroup>');
                            //       $(this).appendTo('.access-select'+id+' optgroup[label='+group+']');
                            //     }
                            //     // $(this).remove();
                            //     // $(this).appendTo("#access-select");

                            //   })

                            //   $(document).on('click', '#access-select option', function () {
                            //     var group = $(this).data('group');
                            //     var id = $(this).parent().parent().data('key');
                            //     if ($('.access-view'+id).children('optgroup[label='+group+']').length) {
                            //       $(this).appendTo('.access-view'+id+' optgroup[label='+group+']');
                            //     } else {
                            //       $('.access-view'+id).append('<optgroup label="'+group+'"></optgroup>');
                            //       $(this).appendTo('.access-view'+id+' optgroup[label='+group+']');
                            //     }
                            //     // $(this).remove();
                            //     // $(this).appendTo("#access-view");

                            //   })

                            //   $(document).on('click','.save-access'+id, function () {
                            //     var id = $(this).data('key');
                            //     var data = [];
                            //     $('.access-view'+id+' option').each(function (i, v) {

                            //       data.push([
                            //         $(v).val(),
                            //         $(v).text(),
                            //         $(v).parent().attr('label'),
                            //         $(v).data('v')
                            //       ]);

                            //     })

                            //     $('#tab'+id+' #AccessList'+id).val(JSON.stringify(data));


                            //     $.ajax({
                            //       url: "http://" + ip + "/EAkimat/Documents.Resolution.NewExecuterList",

                            //       method: 'POST',
                            //       data : JSON.stringify(
                            //         {
                            //         "resolutionId": obj.id, 
                            //         "taskId": obj.Tasks[id].id, 
                            //         "executerDataList": {
                            //         "__type": "DataList:#Avrora.Objects.Web",
                            //         "Data": data
                            //         }
                            //       }),
                            //       beforeSend: function (xhr) {
                            //         xhr.setRequestHeader("content-type", "application/json");
                            //         xhr.setRequestHeader("Token", token);
                            //       },
                            //       success:function(data2){
                            //         $('#tab'+id+' #NewExecuterList'+id).val(JSON.stringify(data2.Data.Object));
                            //         var nummpp=0;
                            //         var tr='';
                            //         $.each(data2.Data.Object, function(k, v){
                            //           nummpp++;
                            //           tr += '<tr>'+
                            //                   '<td>'+nummpp+'</td>'+
                            //                   '<td>'+v.ExecuterName+'</td>'+
                            //                   '<td>'+v.EnterpriseName+'</td>'+
                            //                   '<td>'+getDataFromRef("WorkStatus", v.WorkStatus)+'</td>'+
                            //                   '<td>';
                            //                   if (v.CompleteDate != null) {
                            //                     tr += DateFromMSDate(v.CompleteDate);
                            //                   }

                            //             tr += '</td>'+
                            //                   '<td><input type="checkbox"></td>'+
                            //               '</tr>';

                            //         })

                            //         $('#AccessListTable'+id).html(tr);

                            //       }
                            //     })


                            //     // $('#AccessList'+id).val(JSON.stringify(access));
                            //     // $('#AccessListTable'+id).html(tr);

                            //   })


                        } else if (k == 'Folder') {

                            if (v) {
                                $('#FolderValue').val(v.Name);
                                $('#Folder').val(v.id);
                            }

                        } else if(k == 'Community'){
                            if (v) {
                                $('#CommunityValue').val(v.Name);
                                $('#Community').val(v.id);
                            }      
                        } else if (k == 'IsResident') {

                            $('#' + k).prop('checked', v);

                        } else if (k == 'CounterReserve') {
                            if (v)
                                $('#' + k).val(v.ReserveText);

                        } else if (k == 'Card') {

                            $.each(v, function (i, val) {
                                // if ($('#' + i) && i != 'RegDate' && i != 'DocumentType' && i != 'QuestCharacter' && i != 'OutDate' && i != 'DeliveryType' &&
                                //   i != 'Recipients' && i != 'Correspondent' && i != 'ContractSubject' && i != "ExecuterName" && i != 'SignerName' &&
                                //   i != 'Signer' && i != 'Kontragent' && i != 'HigherAgreement' && i != 'Responsible' && i != 'Department' && i != 'ConcludeDate' &&
                                //   i != 'Importance' && i != 'ContractType' && i != 'ContractTimeBegin' && i != 'ContractTimeEnd' && i != 'ExecutionLimit' && i != 'NDSRate' &&
                                //   i != 'Currency' && i != 'DocumentationAgreement' && i != 'Summ' && i != 'NDS' && i != 'NationalCurrencyRate' && i != 'ConclusionBy' && 
                                //   i != 'WorkGroupChiefEmployeeId' && i != 'ControlDate' ) {

                                //   if (val) {

                                //     $('#' + i).val(val);

                                //   }

                                // } else 
                                if (i == 'NationalCurrencyRate') {
                                    if (Number(val))
                                        $('#' + i).val(Number(val).toFixed(2));
                                } else if (i == 'CarrierType') {
                                    
                                    if (val != null){
                                        $('#' + i).val(val.id);
                                        $('#' + i).trigger("chosen:updated");
                                    }else{
                                        $('#CarrierType').val('');
                                    }
                                } else if (i == 'Languages'){
                                    if(val != null){
                                        if(val.data_list.length > 0){
                                            var chosenLangs = [];
                                            $.each(val.data_list, function (i, el) {
                                                chosenLangs.push(el['id']);
                                            });
                                            $('#Languages').val(chosenLangs).trigger('chosen:updated');
                                        }
                                    }
                                } else if (i == 'Summ') {
                                    function calcContractSummNDS() {
                                        $('#ContractSummNDS').val(($('#Summ').val() * $('#NationalCurrencyRate').val()).toFixed(2))
                                        var nds = ($('#Summ').val() * $('#NDSRate :selected').text() / 100).toFixed(2)
                                        if (typeof Number(nds) == 'number')
                                            $('#NDS').val(nds)
                                        else
                                            $('#NDS').val('')
                                    }
                                    $('#NationalCurrencyRate').on('keyup', function () {
                                        $(this).val($(this).val().replace(/[^\d.]/g, ''));
                                        if ($(this).val().search(/^([0-9]{1,}[.]{0,1}[0-9]{0,2})?$/) == -1) {
                                            $(this).val($(this).val().substring(0, $(this).val().length - 1))
                                        } else calcContractSummNDS()
                                    })
                                    $('#NationalCurrencyRate').on('focusout', function () {
                                        $(this).val($(this).val().replace(/[^\d.]/g, ''));
                                    })
                                    $('#Summ').on('keyup', function () {
                                        $(this).val($(this).val().replace(/[^\d.]/g, ''));
                                        if ($(this).val().search(/^([0-9]{1,}[.]{0,1}[0-9]{0,2})?$/) == -1) {
                                            $(this).val($(this).val().substring(0, $(this).val().length - 1))
                                        } else calcContractSummNDS()
                                    })
                                    $('#Summ').on('focusout', function () {
                                        $(this).val($(this).val().replace(/[^\d.]/g, ''));
                                    })
                                    $('#NDSRate').on('change', function () {
                                        if ($(this).val().search(/^([0-9]{1,}[.]{0,1}[0-9]{0,2})?$/) != -1)
                                            calcContractSummNDS()

                                    })
                                    if (Number(val)) {
                                        $('#' + i).val(Number(val).toFixed(2))
                                        if (val && obj.Card.NationalCurrencyRate)
                                            $('#ContractSummNDS').val((val * obj.Card.NationalCurrencyRate).toFixed(2))
                                    }
                                } else if (i == 'NDS') {
                                    if (Number(val))
                                        $('#' + i).val(Number(val).toFixed(2))

                                } else if (i == 'DocumentationAgreement' && val != null) {
                                    var docList = [];
                                    DocumentationList();
                                    if (val.data_list) {
                                        $('#DocumentationListTable').append('<tbody></tbody>')
                                        $.each(val.data_list, function (index, el) {
                                            $('#DocumentationListTable tbody').append('<tr><td>' + el.Value + '</td></tr>');
                                            docList.push(el.id);
                                        });
                                        $('#DocumentationList').val(JSON.stringify(docList));
                                        $.each(val.data_list, function (index, elem) {
                                            $.each($('#documentation-select').children(), function (index, elem1) {
                                                if ($(elem1).val() == elem.id) {
                                                    $('#documentation-view').append(elem1)
                                                    $('#documentation-select').remove('option[value="' + $(elem1).val() + '"]');

                                                }
                                            });
                                        });
                                    }
                                } else if (i == 'AnswerDate' || i == 'OuterControlDate' || i == 'RegDate' || i == 'OutDate' || i == 'ConcludeDate' || i == 'ContractTimeBegin' || i == 'ContractTimeEnd' || i == 'ExecutionLimit' || i == 'ControlDate') {

                                    if (val) {
                                        $('#' + i).val(DateFromMSDateForForm(val));
                                    }

                                } else if (i == 'Correspondent') {
                                    var jData = {
                                        "includeContragent": "true",
                                        "include_del_rec": "false",
                                        "filter": ""
                                    };
                                    if (val) {
                                        $('#' + i).val(val.Name);
                                        $('#' + i).next().val(val.Id);
                                    }
                                    var token = $.session.get('token')
                                    $.ajax({
                                        url: 'http://' + ip + '/EAkimat/Enterprise.List',
                                        type: 'POST',
                                        xhrFields: {
                                            withCredentials: true
                                        },
                                        data: JSON.stringify(jData),
                                        dataType: 'json',
                                        beforeSend: function (xhr) {
                                            xhr.setRequestHeader("content-type", "application/json");
                                            xhr.setRequestHeader("Token", token);
                                        },
                                        success: function (data) {
                                            $.each(data.data, function (i, el) {
                                                $('#Correspondent').append('<option  value="' + el[0] + '">' + el[1] + "</option>")
                                            });
                                            if (val)
                                                $('#Correspondent').val(val.id)
                                            else
                                                $('#Correspondent').val(null)
                                            $('#Correspondent').chosen();
                                        }
                                    })
                                } else if (i == 'NDSRate') {
                                    if (val) {
                                        $('#' + i).val(val.id);
                                        $('#' + i).trigger("chosen:updated");
                                        $('#NDS').val((obj.Card.Summ * obj.Card.NDSRate.Value / 100).toFixed(2))
                                    }
                                } else if (i == 'DocumentType' || i == 'QuestCharacter' || i == 'DeliveryType' || i == 'Currency' || i == 'StatementType' ||
                                    i == 'IncomingChannel' || i == 'IncomingForm' || i == 'SocialStatus' || i == 'BenefitCategory') {

                                    if (val)
                                        $('#' + i).val(val.id);
                                    $('#' + i).trigger("chosen:updated");

                                } else if (i == 'SubjectQuestion') {
                                    if (val) {
                                        $('#' + i).val(val.id);
                                        $('#' + i).trigger("chosen:updated");
                                    } else {
                                        $('#' + i).parent().parent().remove();
                                    }
                                } else if (i == 'ConclusionBy') {
                                    if (val) {
                                        $('#' + i).val(val);
                                        $('#' + i).trigger("chosen:updated");
                                    }
                                } else if (i == 'Kontragent') {
                                    if (val) {
                                        $('#' + i).val(val.id);
                                    }
                                    $('#' + i).trigger("chosen:updated");
                                } else if ((i == 'Importance' || i == 'ContractType') && val != null) {

                                    $('#' + i).val(val.id);
                                    $('#' + i).trigger("chosen:updated");

                                } else if (i == 'Department') {
                                    if (val) {
                                        $('#' + i).val(val.Name);
                                    }
                                    $('#' + i).trigger("chosen:updated");
                                } else if (i == 'HigherAgreement') {
                                    if (val) {
                                        $('#' + i).val(val.id);
                                    }
                                    $('#' + i).trigger("chosen:updated");
                                } else if (i == 'ContractSubject') {
                                    if (val) {
                                        $('#' + i).val(val.id);
                                    }
                                    $('#' + i).trigger("chosen:updated");
                                } else if (i == 'Recipients') {

                                    var tr = '';
                                    var opts = '';
                                    var access = [];

                                    $(val).each(function (i, value) {

                                        tr += '<div>' + value.Name + '</div>';
                                        opts += '<option value=' + value.Id + '>' + value.Name + '</tr>';
                                        access.push({
                                            "Id": value.Id,
                                            "Name": value.Name,
                                            "ContractorType": value.ContractorType,
                                            "IsEmployee": value.IsEmployee
                                        });

                                    })

                                    $('#Recipients').val(JSON.stringify(access));
                                    $('#AccessList').val(JSON.stringify(access));
                                    $('#RecipientListTable').html(tr);
                                    $('#AccessListTable').html(tr);
                                    if ($('#access-view').length) {
                                        $('#access-view').html(opts);
                                        $.each($('#access-view').children(), function (index, el) {
                                            $.each($('#access-select').children(), function (ind, elem) {
                                                if ($(el).attr('value') == $(elem).attr('value')) {
                                                    $(elem).remove()

                                                }
                                            });
                                        });
                                    } else {
                                        $('#recipient-view').html(opts);
                                        $.each($('#recipient-view').children(), function (index, el) {
                                            $.each($('#recipient-select').children(), function (ind, elem) {
                                                if ($(el).attr('value') == $(elem).attr('value')) {
                                                    $(elem).remove()

                                                }
                                            });
                                        });
                                    }

                                    $('#ContractorType').val();

                                } else if (i == 'ExecuterName' || i == 'SignerName') {
                                    if (data2.Data.FormId == "0201012") {

                                        $('#' + i).val(val);

                                    } else {

                                        var token = $.session.get("token");

                                        $.ajax({
                                            "url": "http://" + ip + "/EAkimat/Workplace.List",
                                            async: false,
                                            method: 'post',
                                            data: JSON.stringify({
                                                "onlyBusy": true,
                                                "ids": null,
                                                "filter": ""
                                            }),
                                            beforeSend: function (xhr) {
                                                xhr.setRequestHeader("content-type", "application/json");
                                                xhr.setRequestHeader("Token", token);
                                            },
                                            success: function (data) {
                                                var have = 0;
                                                var field = i;
                                                $(data.data).each(function (ind, value) {
                                                    $('#' + field).append('<option data-workplaceid=' + value[0] + ' value=' + value[2] + '>' + value[1] + '</tr>');
                                                    if (value[1] == val) {
                                                        have++;
                                                        $('#' + field).last().attr('selected', 'selected')
                                                    }
                                                })
                                                if (have == 0) {
                                                    switch (field) {
                                                        case 'ExecuterName':
                                                            if (v.ExecuterId && v.ExecuterName) {
                                                                $('#' + field).append('<option data-workplaceid=' + v.ExecuterWorkplaceId + ' value=' + v.ExecuterId + ' selected>' + v.ExecuterName + '</option>');
                                                                $('#' + field).val(v.ExecuterId);
                                                            } else
                                                                $('#' + field).val('');
                                                            break;
                                                        case 'SignerName':
                                                            if (v.SignerEmployeeId && v.SignerName) {
                                                                $('#' + field).append('<option data-workplaceid=' + v.SignerWorkplaceId + ' value=' + v.SignerEmployeeId + ' selected>' + v.SignerName + '</option>');
                                                                $('#' + field).val(v.SignerEmployeeId);
                                                            } else
                                                                $('#' + field).val('');

                                                            break;
                                                            // case 'Signer':
                                                            //   if (v.SignerEmployeeId && v.Signer) 
                                                            //     $('#'+field).append('<option value='+v.SignerEmployeeId+' selected>'+v.Signer+'</tr>');
                                                            //   else
                                                            //     $('#'+field).val('');
                                                            // break;
                                                            // case 'Responsible':
                                                            //  if (v.ResponsibleEmployeeId && v.Responsible) 
                                                            //     $('#'+field).append('<option value='+v.ResponsibleEmployeeId+' selected>'+v.Responsible+'</tr>');
                                                            //   else
                                                            //     $('#'+field).val('');
                                                            // break;
                                                    }
                                                }
                                                $('#' + field).chosen();
                                            }

                                        })

                                    }

                                } else if (i == "Signer" || i == 'Responsible') {

                                    var token = $.session.get("token");

                                    $.ajax({
                                        "url": "http://" + ip + "/EAkimat/Workplace.List",
                                        async: false,
                                        method: 'post',
                                        data: JSON.stringify({
                                            "onlyBusy": true,
                                            "ids": null,
                                            "filter": ""
                                        }),
                                        beforeSend: function (xhr) {
                                            xhr.setRequestHeader("content-type", "application/json");
                                            xhr.setRequestHeader("Token", token);
                                        },
                                        success: function (data) {
                                            var have = 0;
                                            var field = i;

                                            $(data.data).each(function (ind, value) {
                                                $('#' + field).append('<option data-workplaceid=' + value[0] + ' value=' + value[2] + '>' + value[1] + '</tr>');
                                                if (value[1] == val) {
                                                    have++;
                                                    $('#' + field).last().attr('selected', 'selected')
                                                }
                                            })
                                            if (have == 0) {
                                                switch (field) {
                                                    // case 'ExecuterName': 
                                                    //   if (v.ExecuterId && v.ExecuterName)
                                                    //     $('#'+field).append('<option value='+v.ExecuterId+' selected>'+v.ExecuterName+'</tr>');
                                                    //   else
                                                    //     $('#'+field).val('');
                                                    // break;
                                                    // case 'SignerName':
                                                    //   if (v.SignerEmployeeId && v.SignerName) 
                                                    //     $('#'+field).append('<option value='+v.SignerEmployeeId+' selected>'+v.SignerName+'</tr>');
                                                    //   else
                                                    //     $('#'+field).val('');

                                                    // break;
                                                    case 'Signer':
                                                        if (v.SignerEmployeeId && v.Signer)
                                                            $('#' + field).append('<option data-workplaceid=' + v.SignerWorkplaceId + ' value=' + v.SignerEmployeeId + ' selected>' + val.Owner.FirstName + ' ' + val.Owner.MiddleName + ' ' + val.Owner.LastName + ' (' + val.Parent.JobTitle.Name + ')</option>');
                                                        else
                                                            $('#' + field).val('');
                                                        break;
                                                    case 'Responsible':
                                                        if (v.ResponsibleEmployeeId && v.Responsible)
                                                            $('#' + field).append('<option data-workplaceid=' + v.ResponsibleWorkPlaceId + ' value=' + v.ResponsibleEmployeeId + ' selected>' + val.Owner.FirstName + ' ' + val.Owner.MiddleName + ' ' + val.Owner.LastName + ' (' + val.Parent.JobTitle.Name + ')</option>');
                                                        else
                                                            $('#' + field).val('');
                                                        break;
                                                }
                                            }
                                            $('#' + field).chosen();
                                        }

                                    })
                                } else if (i == 'WorkGroupChiefEmployeeId') {
                                    $.ajax({
                                        "url": "http://" + ip + "/EAkimat/Workplace.List",
                                        async: false,
                                        method: 'post',
                                        data: JSON.stringify({
                                            "onlyBusy": true,
                                            "ids": null,
                                            "filter": ""
                                        }),
                                        beforeSend: function (xhr) {
                                            xhr.setRequestHeader("content-type", "application/json");
                                            xhr.setRequestHeader("Token", token);
                                        },
                                        success: function (data) {
                                            $.each(data.data, function (index, element) {
                                                $('#' + i).append('<option data-w="' + element[0] + '" value="' + element[2] + '">' + element[1] + '</option>')
                                            })
                                            $('#' + i).val(val);
                                            $('#' + i).chosen();
                                        }
                                    })
                                } else if (i == 'StatementForm') {
                                    var opts = '';
                                    $.each(reference.StatementForm, function (index, value) {
                                        opts += '<option value="' + value.Value + '">' + value.Description + '</option>'
                                    })
                                    $('#' + i).html(opts);
                                    $('#' + i).val(val);
                                    $('#' + i).chosen({
                                        "disable_search": true
                                    });
                                } else if (i == 'StatementCategory') {
                                    if (val != 0)
                                        $('#DeclarantId').prop('required', false)
                                    $('#' + i).on('change', function () {
                                        if ($(this).val() == 0) {
                                            $('#DeclarantId').prop('required', true)
                                        } else {
                                            $('#DeclarantId').prop('required', false)
                                        }
                                    })
                                    var opts = '';
                                    $.each(reference.StatementCategory, function (index, value) {
                                        opts += '<option value="' + value.Value + '">' + value.Description + '</option>'
                                    })
                                    $('#' + i).html(opts);
                                    $('#' + i).val(val);
                                    $('#' + i).chosen({
                                        "disable_search": true
                                    });
                                } else if (i == 'RequestFormat') {
                                    var opts = '';
                                    $.each(reference.CitizenStatementRequestFormat, function (index, value) {
                                        opts += '<option value="' + value.Value + '">' + value.Description + '</option>'
                                    })
                                    $('#' + i).html(opts);
                                    $('#' + i).val(val);
                                    $('#' + i).chosen({
                                        "disable_search": true
                                    });
                                } else if (i == 'AddressId') {
                                    if (val) {
                                        $('#' + i).val(val);
                                        $('#Address').val(getAddress(val));
                                        if (String(val).length > 15) {
                                            $('#AddressStreet').prop('readonly', true);
                                            $('#AddressStreet').val('')
                                        }
                                    }
                                } else if (i == 'IsDirect') {
                                    $('#' + i).prop('checked', val)
                                } else if (i == 'Declarant') {
                                    if (val)
                                        $('#DeclarantName').val(val.FirstName + ' ' + val.LastName + ' ' + val.MiddleName)
                                } else {
                                    if (val) {

                                        $('#' + i).val(val);

                                    }
                                }

                            })

                        } else if (k == 'Folder') {

                            if (v != null && v.Name) {

                                $('#' + k).val(v.Name);

                            }

                        } else if (k == 'Counter') {

                            if (v != null && v.Name) {

                                $('#' + k).val(v.Name);

                            }

                        } else if (k == 'Nomenclature') {
                            if (v)
                                $('#' + k).val(obj.NomenclatureId);
                            $('#' + k).trigger("chosen:updated");

                        } else if (k == 'Year') {

                            var date = new Date();
                            var now = date.getFullYear()
                            var opts = '';

                            for (var i = 2000; i <= now; i++) {

                                var select = '';

                                if (i == v) {
                                    select = 'selected';
                                }

                                opts += '<option ' + select + ' value="' + i + '">' + i + '</option>';

                            }

                            $('#' + k).html(opts);
                            $('#' + k).chosen();

                        } else if (k == 'Template') {

                            $('#' + k).val(v);

                            TemplateParse(v);

                        } else if (k == 'BirthDate' || k == 'CreateDate' || k == 'ResolutionDate' || k == 'BeginDate' || k == 'EndDate') {

                            if (v != null) {

                                $('#' + k).val(DateFromMSDateForForm(v));

                            }

                        } else if (k == 'Document') {

                            $.each(v, function (i, val) {

                                if ($('#' + i)) {
                                    if (val) {

                                        $('#' + i).val(val);

                                    }
                                }

                            })

                        } else if (k == 'AddressId') {

                            if (v && v != 0) {

                                $('#Address').val(getAddress(v));

                                $('#' + k).val(v);

                                if (String(v).length == 13) {

                                    $('.Street').show();

                                } else {

                                    $('.Street').hide();
                                }

                            }

                        } else if (k == 'Type') {

                            $('#' + k).val(v);

                            $('#' + k).trigger("chosen:updated");

                        } else if (k == 'DocumentType') {

                            $('#' + k).val(v);

                            $('#' + k).trigger("chosen:updated");

                        } else if (k == 'AccessList') {

                            $('#' + k).val(JSON.stringify(v));

                            var token = $.session.get("token");

                            $.ajax({
                                "url": "http://" + ip + "/EAkimat/Workplace.List",
                                async: false,
                                method: 'post',
                                data: JSON.stringify({
                                    "onlyBusy": true,
                                    "ids": null,
                                    "filter": ""
                                }),
                                beforeSend: function (xhr) {
                                    xhr.setRequestHeader("content-type", "application/json");
                                    xhr.setRequestHeader("Token", token);
                                },
                                success: function (data) {

                                    var tr = '';
                                    var opts = '';

                                    $(data.data).each(function (i, val) {

                                        tr += '<div>' + val[1] + '</div>';
                                        opts += '<option value=' + val[0] + '>' + val[1] + '</tr>';

                                    })

                                    $('#AccessListTable').html(tr);
                                    $('#access-view').append(opts);
                                    $.each($('#access-view').children(), function (index, el) {
                                        $.each($('#access-select').children(), function (ind, elem) {
                                            if ($(el).attr('value') == $(elem).attr('value')) {
                                                $(elem).remove()

                                            }
                                        });
                                    });

                                }

                            })

                        } else if (k == 'RegistrationPlace') {
                            if(v != null){
                                $('#' + k).val(v.id);
                                $('#' + k).trigger("chosen:updated");
                            }

                        } else if (k == 'LifePeriod') {
                            if(v != null){
                                $('#' + k).val(v.id);
                                $('#' + k).trigger("chosen:updated");
                            }

                        }else if (k == 'INN') {
                            $('#INN').on('keyup input', function () {
                                $(this).val($(this).val().replace(/^([^\d]{0,12})?$/g, ''));
                                if ($(this).val().search(/^([\d]{0,12})?$/) == -1) {
                                    $(this).val($(this).val().substring(0, $(this).val().length - 1))
                                }
                            })
                            $('#' + k).val(v);

                        } else if (k == 'Sector') {

                            if(v != null){
                                sectorArrChosen = [];
                                sectorArrRef = reference['industry'];
                                var i = 0;
                                while (i < sectorArrRef.length) {
                                    if((sectorArrRef[i]['Value'] & v) > 0){
                                        sectorArrChosen.push(sectorArrRef[i]['Value']);
                                    }
                                    i++;
                                }
                                $('#' + k).val(sectorArrChosen).trigger('chosen:updated');
                            }    

                        } else if (k == 'GuitEnterprise' && v != null) {

                            $('.GuitEnterprise .Name').val(v.Name);
                            $('.GuitEnterprise .DepartmentCode').val(v.DepartmentCode);
                            $('.GuitEnterprise .Email').val(v.Email);

                            if (v.DelRec == false) {

                                $('.GuitEnterprise .DelRec').prop('checked', true);

                            } else {

                                $('.GuitEnterprise .DelRec').prop('checked', false);

                            }

                        } else if (k == 'ContactList') {

                            var phone_row = ' <div class="phone-row ">' +
                                '<input type="hidden" name="id" class="id">' +
                                '<div class="col-xs-3">' +
                                '<select class="w100 Type form-control ref" required name="Type" data-ref="phone"></select>' +
                                '</div>' +
                                '<div class="col-xs-4">' +
                                '<input type="text" class="from-control text-field w100 Value" required name="Value" />' +
                                '</div>' +
                                '<div class="col-xs-4">' +
                                '<input type="text" class="from-control text-field w100 Description" name="Description" />' +
                                '</div>' +
                                '<div class="col-xs-1"><a class="delete-phone-row"><img src="img/ui/delete_inactive.png" alt="Удалить" title="Удалить"></a></div>' +
                                '<div style="clear: both;"></div>' +
                                '</div>';

                            var email_row = ' <div class="email-row ">' +
                                '<input type="hidden" name="id" class="id">' +
                                '<div class="col-xs-3">' +
                                '<select class="w100 Type form-control ref" required data-ref="email" name="Type"></select>' +
                                '</div>' +
                                '<div class="col-xs-4">' +
                                '<input type="text" class="from-control text-field w100 Value" required name="Value" />' +
                                '</div>' +
                                '<div class="col-xs-4">' +
                                '<input type="text" class="from-control text-field w100 Description" name="Description" />' +
                                '</div>' +
                                '<div class="col-xs-1"><a class="delete-email-row"><img src="img/ui/delete_inactive.png" alt="Удалить" title="Удалить"></a></div>' +
                                '<div style="clear: both;"></div>' +
                                '</div>';

                            var tbody1 = '';
                            var tbody2 = '';

                            if (v == null) {

                            } else {

                                $.each(v.data_list, function (i, obj) {

                                    if (obj.__type == 'ContactPhoneNew:#Avrora.Objects.Common') {

                                        $('.phone-list').append(phone_row);
                                        Count(++Gcount);
                                        LoadRefAtOne('phone', '.phone-row .Type', obj.Type);
                                        $('.phone-list .id').val(obj.id);
                                        $('.phone-list .Value').val(obj.Value);
                                        $('.phone-list .Description').val(obj.Description);

                                    } else if (obj.__type == 'ContactEMailNew:#Avrora.Objects.Common') {

                                        $('.email-list').append(email_row);
                                        Count(++Gcount);
                                        LoadRefAtOne('email', '.email-row .Type', obj.Type);
                                        $('.email-list .id').val(obj.id);
                                        $('.email-list .Value').val(obj.Value);
                                        $('.email-list .Description').val(obj.Description);

                                    }

                                })

                            }

                            function remove_phone() {

                                $('.delete-phone-row').unbind('click');

                                $('.delete-phone-row').on('click', function () {

                                    Count(--Gcount);

                                    $(this).closest('.phone-row').remove();

                                    var id = $(this).closest('.phone-row').find('.id').val();

                                    if (id && id != '') {

                                        var object = data.Data.Object;

                                        $.each(object.ContactList.data_list, function (k, val) {

                                            if (val.id == id) {

                                                object.ContactList.data_list[k].DelRec = true;

                                            }

                                        })

                                    }


                                })

                            }

                            function remove_email() {

                                $('.delete-email-row').unbind('click');
                                $('.delete-email-row').on('click', function () {

                                    Count(--Gcount);

                                    $(this).closest('.email-row').remove();

                                    var id = $(this).closest('.email-row').find('.id').val();

                                    if (id && id != '') {

                                        var object = data.Data.Object;

                                        $.each(object.ContactList.data_list, function (k, val) {

                                            if (val.id == id) {

                                                object.ContactList.data_list[k].DelRec = true;

                                            }

                                        })

                                    }

                                })

                            }

                            $('.add-phone-row').on('click', function () {

                                $('.phone-list').append(phone_row);

                                Count(++Gcount);

                                LoadRef();
                                remove_phone();

                            })

                            $('.add-email-row').on('click', function () {

                                Count(++Gcount);

                                $('.email-list').append(email_row);

                                LoadRef();
                                remove_email();

                            })



                            remove_phone();
                            remove_email();
                        } else if (k == 'Questions') {
                            $(document).on('click', '.select-doc', function () {
                                var index = $(this).data('id');
                                $('.modal').remove()
                                $('body').append(

                                    '<div class="modal" id="sel-doc" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">' +
                                    '<div class="modal-dialog modal-lg" role="document">' +
                                    '<div class="modal-content">' +
                                    '<div class="modal-header">' +
                                    '<div class="title">Выбрать документ</div>' +
                                    '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times-rectangle"></i></button>' +
                                    '</div>' +
                                    '<div class="modal-body">' +
                                    '<div class="filters">' +
                                    '</div>' +
                                    '<table class="duplicateSelect"></table>' +
                                    '<input type="hidden" id="DocId">' +
                                    '<input type="hidden" id="SelectQ">' +
                                    '</div>' +
                                    '<div class="formsep"></div>' +
                                    '<div class="modal-footer">' +
                                    '<div class="buttons"><a class="save attach-duplicate">ОТПРАВИТЬ</a><a class="cancel" data-dismiss="modal">Отмена</a></div>' +
                                    '</div>' +
                                    '</div>' +
                                    '</div>' +
                                    '</div>'
                                )
                                $('#SelectQ').val(index);
                                $('#sel-doc').modal('show')
                                var table = $('.duplicateSelect').DataTable({
                                    "processing": true,
                                    "serverSide": true,
                                    "paging": true,
                                    "destroy": true,
                                    // "scrollY": 20,
                                    "serching": true,
                                    "ajax": {
                                        "url": "http://" + ip + "/EAkimat/Correspondence.Document.Select",
                                        xhrFields: {
                                            withCredentials: true
                                        },
                                        beforeSend: function (xhr) {
                                            xhr.setRequestHeader("Token", token);
                                        },
                                        error: function (data, d1, d2) {

                                            if (d2 == 'Unauthorized') {
                                                if (data.responseText == '"Отсутствуют права на данную операцию"') {
                                                    $('.forcontent').empty();
                                                    $.notify(data.responseText.replace(/"/g,''), {type: 'danger'});
                                                } else {
                                                   $.session.set("token", '');
                                                   window.location = "http://"+site_url+"/";
                                                }
                                                
                                            }

                                        },

                                    },
                                    "language": {
                                        "url": "/js/russian.js"
                                    },
                                    "columnDefs": [{
                                        "targets": [5],
                                        "render": function (data, type, row) {
                                            return '<span id="idDupl" class="hide">' + row[0] + '</span>'
                                        },
                                        "orderable": false,

                                    }, ],
                                    "aoSearchCols": [
                                        null,
                                        null,
                                        null,
                                        {
                                            "sSearch": "Avrora.Objects.Modules.Docflow.DocflowObjects.OutgoingDocument"
                                        },
                                        {
                                            "sSearch": "Registered"
                                        },
                                        {
                                            "sSearch": false
                                        },
                                    ],

                                    "order": [
                                        [1, "asc"]
                                    ],
                                    "responsive": true,
                                    // "autoWidth": true,
                                    // filter: true,
                                    columns: [{
                                            title: "Рег. номер",
                                            data: 1,
                                            width: "15%",
                                            orderable: false
                                        },
                                        {
                                            title: "Рег. дата",
                                            data: 2,
                                            width: "15%"
                                        },
                                        {
                                            title: "Тип",
                                            data: 3,
                                            width: "15%",
                                            orderable: false,
                                            searchable: true
                                        },
                                        {
                                            title: "Корреспондент",
                                            data: 4,
                                            width: "35%",
                                            orderable: false,
                                            searchable: true
                                        },
                                        {
                                            title: "Тема",
                                            data: 5,
                                            width: "20%",
                                            searchable: true,
                                            orderable: false,
                                        },
                                        {
                                            title: "",
                                            data: 0,
                                            width: "1%"
                                        },
                                    ],
                                    "drawCallback": function (settings) {
                                        //$('.dataTables_filter').appendTo('.selectval');
                                        $('.dataTables_length select').chosen({
                                            disable_search: true,
                                            width: 'auto'
                                        });
                                        $('.dataTables_length .chosen-search-input').hide();
                                    }
                                });
                                $(document).off('click', '.duplicateSelect tbody tr').on('click', '.duplicateSelect tbody tr', function () {
                                    $('.selectedRow').removeClass('selectedRow')
                                    $(this).addClass('selectedRow')
                                    $('#DocId').val($('.selectedRow #idDupl').text());
                                })
                                $('.attach-duplicate').on('click', function () {
                                    var docId = $('#DocId').val();
                                    var qIndex = $('#SelectQ').val();
                                    var row = $('.selectedRow').children();
                                    var docText = '№' + $(row[0]).text() + ' от ' + $(row[1]).text() + ' "' + $(row[4]).text() + '"'
                                    $('#tab' + qIndex + ' #Answer').val(docText)
                                    $('#tab' + qIndex + ' #AnswerId').val(docId)
                                    $('#tab' + qIndex + ' .Attachment').hide()
                                    $('#sel-doc').modal('hide')
                                    $('#sel-doc').remove()
                                    $('.modal-backdrop').remove();
                                })
                            })
                            $(document).on('change', '#Status', function () {
                                var tabIndex = $(this).data('id');
                                var value = $(this).val();
                                $('#tab' + tabIndex + ' #Answ').show()
                                $('#tab' + tabIndex + ' #Trans').show()
                                $('#tab' + tabIndex + ' #Answ .Answer').show()
                                $('#tab' + tabIndex + ' #Answ .Attachment').show()
                                $('#tab' + tabIndex + ' #Answ .ActionsTaken').show()

                                if (value == 0 || value == 10) {
                                    $('#tab' + tabIndex + ' #Answ').hide()
                                    $('#tab' + tabIndex + ' #Trans').hide()
                                } else if (value == 50) {
                                    $('#tab' + tabIndex + ' #Answ').hide()
                                } else {
                                    $('#tab' + tabIndex + ' #Trans').hide()
                                    if (value == 70) {
                                        $('#tab' + tabIndex + ' #Answ .Answer').hide()
                                        $('#tab' + tabIndex + ' #Answ .Attachment').hide()
                                    }
                                    if (value != 30) {
                                        $('#tab' + tabIndex + ' #Answ .ActionsTaken').hide()
                                    }
                                }
                            })
                            $(document).on('click', '.clear-doc', function () {
                                var index = $(this).data('id');
                                $('#tab' + index + ' #AnswerId').val('');
                                $('#tab' + index + ' #Answer').val('');
                                $('#tab' + index + ' .Attachment').show()

                            })
                            $(document).on('change', '#exampleInputFile', function () {
                                var index = $(this).data('id');
                                if (this.files[0].size > 10485760) {
                                    $.notify('Размер вложения не должен превышать 10Mb', {
                                        type: 'danger'
                                    });
                                    $('#tab' + index + ' #exampleInputFile').val(null);
                                } else {
                                    $('#tab' + index + ' #Attachment').val(this.files[0].name);
                                    $('#tab' + index + ' .Answer').hide()
                                }

                            })
                            $(document).off('click', '.clear-attach').on('click', '.clear-attach', function () {
                                var index = $(this).data('id');
                                $('#tab' + index + ' #Attachment').val('');
                                $('#tab' + index + ' #exampleInputFile').val(null);
                                $('#tab' + index + ' .Answer').show()
                            })
                            $(document).off('click', '.select-attach').on('click', '.select-attach', function () {
                                var index = $(this).data('id');
                                $('#tab' + index + ' #exampleInputFile').click()
                            })
                            $(document).off('click', '#Answer .actionBtnList').on('click', '#Answer .actionBtnList', function () {
                                var action = $(this).data('action');
                                var id = $(this).data('id');

                                if (action == 'open') {
                                    dialog.confirm({
                                        title: "Предупреждение",
                                        message: 'Вы действительно хотите закрыть текущий документ и перейти на связанный?',
                                        cancel: "Нет",
                                        button: "Да",
                                        required: true,
                                        callback: function (value) {
                                            if (value == true) {
                                                $.ajax({
                                                    "url": "http://" + ip + "/EAkimat/" + action + "/Chancellery|Documents.Document/?id=" + id,
                                                    cache: false,
                                                    beforeSend: function (xhr) {
                                                        xhr.setRequestHeader("content-type", "application/json");
                                                        xhr.setRequestHeader("Token", token);
                                                    },
                                                    success: function (data) {
                                                        if (data.Result == 'OK') {
                                                            $('.forcontent').html('');
                                                            openedObjed = data;
                                                            View(data);
                                                        } else {
                                                            $.notify(data.Message, {
                                                                type: 'danger'
                                                            });

                                                        }
                                                    }
                                                })
                                            }
                                        }
                                    })
                                }
                            })
                            $(document).on('click', '.select-Questions', function () {
                                index = $(this).data('id');
                                $('#exampleModal2').remove();
                                $('body').append(
                                    '<div class="modal" id="select-Questions" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">' +
                                    '<div class="modal-dialog modal-lg" role="document">' +
                                    '<div class="modal-content">' +
                                    '<div class="modal-header">' +
                                    '<div class="title">Адрес</div>' +
                                    '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times-rectangle"></i></button>' +
                                    '</div>' +
                                    '<div class="modal-body">' +
                                    '<div class="row">' +
                                    '<div class="col-xs-6 col-sm-10 col-md-10 col-lg-10">' +
                                    '<button id="close-tree">Свернуть все</button>' +
                                    '</div>' +
                                    '<div class="col-xs-6 col-sm-2 col-md-2 col-lg-2">' +
                                    '<input id="tree-search" type="search" class="form-control input-sm w100" style="margin-left: 0 !important" placeholder="Фильтр">' +
                                    '</div>' +
                                    '</div>' +
                                    '<div id="Questions" >' +
                                    '</div>' +
                                    '</div>' +
                                    '<div class="formsep"></div>' +
                                    '<div class="modal-footer"> ' +
                                    '<div class="buttons"><a class="save save-Questions" >ОТПРАВИТЬ</a><a class="cancel" data-dismiss="modal">Отмена</a></div>' +
                                    '</div>' +
                                    '</div>' +
                                    '</div>' +
                                    '</div>');
                                $('#select-Questions').modal('show');
                                var L;
                                $(document).off('click', '.save-Questions').on('click', '.save-Questions', function () {
                                    var item = $('#Questions').jstree('get_selected', true)[0];
                                    $('#tab' + index + ' #Code').val(item.text)
                                    var id = item.id % 1000000;
                                    $('#tab' + index + ' #CodeId').val(id)
                                    $.ajax({
                                        "url": " http://" + ip + "/EAkimat/References.ByParent?refType=1409&parentid=" + id,
                                        async: false,
                                        beforeSend: function (xhr) {
                                            xhr.setRequestHeader("content-type", "application/json");
                                            xhr.setRequestHeader("Token", token);
                                        },
                                        success: function (data) {

                                            var options = '<option value="0"></option>';

                                            $.each(data, function (key, val) {

                                                options += '<option value="' + val.id + '">' + val.Value + '</option>';

                                            })

                                            $('#tab' + index + ' #SubQuestion').html(options);
                                            $('#tab' + index + ' #SubQuestion').trigger('chosen:updated');
                                            // $('#tab' + index+' #'+index).chosen({
                                            //   width: "100%",
                                            //   allow_single_deselect: false
                                            // });

                                        }
                                    })
                                    $('#select-Questions').modal('hide');
                                    $('#select-Questions').remove();
                                })

                                $.ajax({
                                    url: 'http://' + ip + '/EAkimat/CitizenStatements.SubjectQuestions',
                                    xhrFields: {
                                        withCredentials: true
                                    },
                                    beforeSend: function (xhr) {
                                        xhr.setRequestHeader("content-type", "application/json");
                                        xhr.setRequestHeader("Token", token);
                                    },
                                    success: function (data) {

                                        L = data.Data.Object.data;
                                        var q = '';
                                        $('#Questions').jstree({
                                            'core': {
                                                'data': L,
                                                'multiple': false
                                            },
                                            "plugins": [
                                                "search",
                                            ]
                                        })
                                        $('#Questions').on('refresh.jstree', function (e, data) {
                                            $('#Questions').jstree(true).search(q);
                                        })
                                        $('#tree-search').on('keyup', function () {
                                            // if ($(this).val().length >= 3)
                                            // $('#Questions').jstree(true).search($(this).val());

                                            q = $(this).val();

                                            $.ajax({
                                                url: 'http://' + ip + '/EAkimat/CitizenStatements.SubjectQuestions?search=' + q,
                                                xhrFields: {
                                                    withCredentials: true
                                                },
                                                beforeSend: function (xhr) {
                                                    xhr.setRequestHeader("content-type", "application/json");
                                                    xhr.setRequestHeader("Token", token);
                                                },
                                                success: function (data) {
                                                    L = data.Data.Object.data;
                                                    $('#Questions').jstree(true).settings.core.data = L;
                                                    $('#Questions').jstree(true).refresh();

                                                }
                                            })

                                        })
                                        $('#close-tree').on('click', function () {
                                            $("#Questions").jstree("close_all");
                                        })

                                    }
                                })
                            })
                            $(document).on('click', '.del-quest', function () {
                                var tab = $(this).parent()
                                var id = $(this).data('id');
                                var dataId = $(this).data('data');
                                dialog.confirm({
                                    title: "Предупреждение",
                                    message: 'Удалить вопрос?',
                                    cancel: "Нет",
                                    button: "Да",
                                    required: true,
                                    callback: function (value) {
                                        if (value == true) {
                                            $('#tab' + id).remove();
                                            open_item.Data.Object.Document.Questions[dataId].DelRec = true;
                                            $(tab).remove()
                                            if ($('.quest-item').length == 1) {
                                                $('.del-quest').remove();
                                            }
                                        }
                                    }
                                })

                            })
                            $.each(v, function (key, val) {
                                QuestionParse(key, val);
                            })
                            if ($('.quest-item').length == 1) {
                                $('.del-quest').remove();
                            }
                            // if () {

                            // }
                        } else if (k == 'Number') {
                            if (data.Data.FormId == '0401022' || data.Data.FormId == '0401021') {
                                if (obj.CounterId == '00000000-0000-0000-0000-000000000000') {
                                    $('#' + k).prop('readonly', false);
                                } else {
                                    $('#CounterId').val(obj.CounterId);
                                    $('#CounterValue').val(obj.CounterValue);
                                }
                                $('#' + k).val(v);
                                $('.NumberClear').on('click', function () {
                                    $('#Number').val('')
                                    $('#NumberId').val('00000000-0000-0000-0000-000000000000')
                                    $('#' + k).prop('readonly', false);
                                })
                                $('.NumberSelect').off('click')
                                $('.NumberSelect').on('click', function () {
                                    $.ajax({
                                        "url": "http://" + ip + "/EAkimat/Orders.Counter.List",
                                        xhrFields: {
                                            withCredentials: true
                                        },
                                        beforeSend: function (xhr) {
                                            xhr.setRequestHeader("content-type", "application/json");
                                            xhr.setRequestHeader("Token", token);
                                        },
                                        success: function (data) {
                                            data = data.Data.Object.Data;
                                            if (data.length == 0) {
                                                $.ajax({
                                                    "url": "http://" + ip + "/EAkimat/new/Orders.Counter",
                                                    async: false,
                                                    cache: false,
                                                    beforeSend: function (xhr) {
                                                        xhr.setRequestHeader("content-type", "application/json");
                                                        xhr.setRequestHeader("Token", token);
                                                    },
                                                    success: function (data2) {
                                                        if (data2.Result == 'OK') {
                                                            $.ajax({
                                                                url: "views/0401062.html",
                                                                cache: false,
                                                                async: false,
                                                                success: function (data) {
                                                                    $('#AddNumber').remove();
                                                                    $('#AddNumberTemplate').remove();
                                                                    $('#AddNumber').remove();
                                                                    $('body').append(data);
                                                                    $('#AddNumber').modal('show');
                                                                    if (data2.Data.Object.Name) {
                                                                        $('#title-addnumber').text(data2.Data.Object.Name);
                                                                    } else {
                                                                        $('#title-addnumber').parent().text("Новый счетчик");
                                                                    }
                                                                    $('#Title').val(data2.Data.Object.Name);
                                                                    $('#Index').val(data2.Data.Object.Index);
                                                                    $('#CounterValue').val(data2.Data.Object.Value);
                                                                    $('#Template').val(data2.Data.Object.Template);
                                                                    TemplateParse(data2.Data.Object.Template);

                                                                    $('.save-AddNumber').on('click', function () {
                                                                        if ($('#Title').val()) {
                                                                            if ($('#Template').val()) {
                                                                                var object = data2.Data.Object;
                                                                                object.Name = $('#Title').val();
                                                                                object.Index = $('#Index').val();
                                                                                object.Template = $('#Template').val();


                                                                                var result = {
                                                                                    "parent": object
                                                                                };
                                                                                $.ajax({
                                                                                    "url": "http://" + ip + "/EAkimat/Save/Orders.Counter",
                                                                                    data: JSON.stringify(result),
                                                                                    method: 'POST',
                                                                                    beforeSend: function (xhr) {
                                                                                        xhr.setRequestHeader("content-type", "application/json");
                                                                                        xhr.setRequestHeader("Token", token);
                                                                                    },
                                                                                    success: function (data) {
                                                                                        if (data.Result == 'OK') {
                                                                                            $('#AddNumber').modal('hide');
                                                                                            $('#AddNumber').remove();
                                                                                            var id = data.Data.Object.id;
                                                                                            var cVal = data.Data.Object.Value;
                                                                                            var fromReserve = false;
                                                                                            $.ajax({
                                                                                                "url": "http://" + ip + "/EAkimat/Orders.Counter.TemplateNumber?id=" + id + "&value=" + cVal + "&fromReserve=" + fromReserve,
                                                                                                beforeSend: function (xhr) {
                                                                                                    xhr.setRequestHeader("content-type", "application/json");
                                                                                                    xhr.setRequestHeader("Token", token);
                                                                                                },
                                                                                                success: function (data) {
                                                                                                    console.log(data);
                                                                                                    if (data.Result == 'OK') {
                                                                                                        $('#Number').val(data.Message);
                                                                                                        $('#Number').prop('readonly', true);
                                                                                                        $('#CounterId').val(id);
                                                                                                        $('#CounterValue').val(cVal);
                                                                                                        $('#FromReserv').val(fromReserve);
                                                                                                        $('#NumberSelect').modal('hide');
                                                                                                        $('#NumberSelect').remove();
                                                                                                    }
                                                                                                }
                                                                                            })


                                                                                        } else {
                                                                                            $.notify(data.Message, {
                                                                                                type: 'danger'
                                                                                            })
                                                                                        }
                                                                                    }
                                                                                })
                                                                            } else $.notify('Шаблон', {
                                                                                type: 'danger'
                                                                            })
                                                                        } else $.notify('Наименование', {
                                                                            type: 'danger'
                                                                        })



                                                                    })
                                                                }
                                                            })
                                                        }
                                                    }
                                                })
                                            } else {
                                                $.ajax({
                                                    url: "views/0401030.html",
                                                    cache: false,
                                                    async: false,
                                                    success: function (view) {
                                                        $('#NumberSelect').remove();
                                                        $('body').append(view);
                                                        $('#NumberSelect').modal('show');
                                                        var tds = '';
                                                        $.each(data, function (index, element) {
                                                            tds += '<tr data-id="' + element[0] + '">';
                                                            tds += '<td>' + element[1] + '</td>'
                                                            tds += '<td>' + element[2] + '</td>'
                                                            tds += '<td>' + element[3] + '</td>'
                                                            tds += '</tr>';

                                                        })

                                                        $('#Numbers').html(tds);
                                                        $('#inReserve').prop('disabled', true)

                                                        $('.counterType').on('click', function () {
                                                            if ($(this).val() == "0") {
                                                                $('#inReserve').prop('disabled', true)
                                                            } else {
                                                                if ($('#inReserve').text() != '')
                                                                    $('#inReserve').prop('disabled', false)
                                                            }
                                                        })

                                                        $('#Numbers tr').on('click', function () {
                                                            $('.selectedRow').removeClass('selectedRow')
                                                            $(this).addClass('selectedRow')
                                                            var reserve = $(this).children()[2].textContent;
                                                            $('#inReserve').html('')
                                                            if (reserve) {
                                                                if ($('.counterType:checked').val() == "1")
                                                                    $('#inReserve').prop('disabled', false)
                                                                var res = reserve.split(',');
                                                                $.each(res, function (key, val) {
                                                                    $('#inReserve').append('<option ' + val + '>' + val + '</option>')
                                                                })
                                                            } else {
                                                                $('#inReserve').prop('disabled', true)
                                                            }
                                                        })
                                                        $('.save-number').on('click', function () {
                                                            var tr = $('.selectedRow')[0];

                                                            if (tr) {
                                                                var id = $(tr).data('id');
                                                                var fromReserve = false;
                                                                if ($('.counterType:checked').val() == "0") {

                                                                    var cVal = $(tr).children()[1].textContent;

                                                                } else {
                                                                    if ($('#inReserve').text() != '') {
                                                                        var cVal = $('#inReserve').val();
                                                                        fromReserve = true;
                                                                    } else {
                                                                        $.notify('Резерв отсутствует', {
                                                                            type: 'danger'
                                                                        });
                                                                        return false;
                                                                    }


                                                                }
                                                                if (id && cVal) {
                                                                    $.ajax({
                                                                        "url": "http://" + ip + "/EAkimat/Orders.Counter.TemplateNumber?id=" + id + "&value=" + cVal + "&fromReserve=" + fromReserve,
                                                                        beforeSend: function (xhr) {
                                                                            xhr.setRequestHeader("content-type", "application/json");
                                                                            xhr.setRequestHeader("Token", token);
                                                                        },
                                                                        success: function (data) {
                                                                            console.log(data);
                                                                            if (data.Result == 'OK') {
                                                                                $('#Number').val(data.Message);
                                                                                $('#Number').prop('readonly', true);
                                                                                $('#CounterId').val(id);
                                                                                $('#CounterValue').val(cVal);
                                                                                $('#FromReserv').val(fromReserve);
                                                                                $('#NumberSelect').modal('hide');
                                                                                $('#NumberSelect').remove();
                                                                            }
                                                                        }
                                                                    })
                                                                }
                                                            } else {
                                                                $.notify('Счетчик не выбран', {
                                                                    type: 'danger'
                                                                });
                                                            }

                                                        })

                                                    }
                                                })
                                            }
                                        }
                                    })

                                })
                            }
                        } else if (k == 'SignerName') {
                            
                        }else {
                            if (v) {

                                $('#' + k).val(v);

                            }
                        }

                    })

                    $('.ribbonBtn').on('click', function () {
                        var action = $(this).data('action');
                        if (action == 'AddQuestion') {
                            $.ajax({
                                "url": 'http://' + ip + '/EAkimat/CitizenStatement/AddQuestion?id=' + open_item.Data.Object.Document.id,

                                async: false,
                                cache: false,
                                beforeSend: function (xhr) {
                                    xhr.setRequestHeader("content-type", "application/json");
                                    xhr.setRequestHeader("Token", token);
                                },
                                success: function (data) {
                                    data = data.Data.Object;
                                    open_item.Data.Object.Document.Questions.push(data);
                                    var key = open_item.Data.Object.Document.Questions.length - 1;
                                    QuestionParse(key, data);
                                    $('.del-quest').remove();
                                    $.each($('.quest-item'), function (i, e) {
                                        var delBtn = '<a class="del-quest" data-action="delete" data-data="' + $(e).data('data') + '" data-id="' + $(e).data('tab') + '"><img title="Удалить" alt="Удалить" src="img/ui/delete_inactive.png"></a>';

                                        $(e).after(delBtn)
                                    })


                                    if ($('.quest-item').length == 1) {
                                        $('.del-quest').remove();
                                    }
                                }
                            })
                        } else {
                            if(action != "Close"){
                                var valid = validate($('#object'));
                            }    
                            var type = $(this).data('type');
                            if (openedObjed.Data.Object.Document) {
                                var obb = openedObjed.Data.Object.Document
                            } else {
                                var obb = openedObjed.Data.Object
                            }
                            if (obb.__type.indexOf('ControllingDocument') != -1) {
                                if (obb.DocumentStatus > 20) {
                                    dialog.confirm({
                                        title: "Предупреждение",
                                        message: "Сохранение документа приведет к переходу на этап 'Формирование плана мероприятий'. Продолжить?",
                                        cancel: "Нет",
                                        button: "Да",
                                        required: true,
                                        callback: function (value) {
                                            if (value == true) {
                                                SaveOrSaveAndClose(action, type)
                                            }
                                        }
                                    })
                                } else SaveOrSaveAndClose(action, type)

                            } else SaveOrSaveAndClose(action, type)

                            function SaveOrSaveAndClose(action, type) {
                                if (action == 'Save' || action == 'SaveAndClose') {

                                    if (openedObjed.Data.Object.Document && openedObjed.Data.FormId != '0205021') {
                                        // if (openedObjed.Data.FormId == '0201071' || openedObjed.Data.FormId == '0201011' || openedObjed.Data.FormId == '0201021' || openedObjed.Data.FormId == '0201031' ||
                                        // openedObjed.Data.FormId == '0501021' || openedObjed.Data.FormId == '0201041' || openedObjed.Data.FormId == '0201051') {

                                        var object = openedObjed.Data.Object.Document;

                                    } else {

                                        var object = openedObjed.Data.Object;

                                    }

                                    if(object.__type == "ContractorEnterprise:#Avrora.Objects.Common"){
                                        AdditionalObject = openedObjed.Data.AdditionalObjects[0];
                                        AdditionalObject.IsMinisterstvo = $('#IsMinisterstvo').prop('checked');
                                        AdditionalObject.IsOblOrgan = $('#IsOblOrgan').prop('checked');
                                        AdditionalObject.IsRegionAkimat = $('#IsRegionAkimat').prop('checked');
                                        AdditionalObject.IsOblBusiness = $('#IsOblBusiness').prop('checked');
                                    }

                                    if (type == 'Chancellery|Documents.Document') {

                                        object.Theme = $('#Theme').val();

                                        if ($('#Folder').val() != "") {
                                            object.FolderId = $('#Folder').val();
                                        }
                                        if ($('#RegNumber').val()) {
                                            object.Card.RegNumber = $('#RegNumber').val();
                                        } else {
                                            object.Card.RegNumber = null;
                                        }

                                        if (object.Card.ContractSubject && object.Card.ContractSubject != null) {

                                            object.Card.ContractSubject.id = $('#ContractSubject').val();
                                            object.Card.ContractSubject.Value = $('#ContractSubject option:selected').text();

                                        } else if (object.Card.ContractSubject == null) {

                                            var data = {
                                                id: $('#ContractSubject').val(),
                                                ReferenceType: 1306

                                            };

                                            object.Card.ContractSubject = data;

                                        }

                                        /*if ($('#RegDate').val() && $('#RegDate').val() != "") {
                                          object.Card.RegDate = '/Date('+Date.parse($('#RegDate').val())+')/';
                                        } else {
                                          object.Card.RegDate = null;
                                        }*/
                                        object.NomenclatureId = $('#Nomenclature').val();

                                        //Поля тип носителя и языки документа
                                        if(object.Card.CarrierType){
                                            object.Card.CarrierType.id = $('#CarrierType').val();
                                        }else{
                                            object.Card.CarrierType = {id: $('#CarrierType').val()};
                                        }
                                        
                                        if($('#Languages').val().length > 0){
                                            var arrLangs = [];
                                            $.each($('#Languages').val(), function (i, el) {
                                                arrLangs[i] = { ReferenceType: 1000, id: el };
                                            });
                                        }

                                        if (object.Card.Languages && object.Card.Languages != null) {
                                            object.Card.Languages.data_list = arrLangs;
                                        }else{
                                            object.Card.Languages = { data_list: arrLangs}
                                        }
                                        //
                                        if (object.Card.DocumentType && object.Card.DocumentType != null) {

                                            object.Card.DocumentType.id = $('#DocumentType').val();
                                            object.Card.DocumentType.Value = $('#DocumentType option:selected').text();

                                        } else {

                                            var data = {
                                                id: $('#DocumentType').val(),
                                                ReferenceType: 1001

                                            };

                                            object.Card.DocumentType = data;

                                        }
                                        if ($('ConclusionBy')) {
                                            object.Card.ConclusionBy = $('#ConclusionBy').val();
                                        }
                                        // if ($('ConcludeDate')) {
                                        //   object.Card.ConcludeDate = $('ConcludeDate').val();
                                        // }
                                        // if ($('ContractNumber')) {
                                        //   object.Card.ContractNumber = $('ContractNumber').val();
                                        // } 
                                        // if ($('Currency')) {
                                        //   object.Card.Currency = $('Currency').val();
                                        // }
                                        if ($('#DepartmentId').val()) object.Card.DepartmentId = $('#DepartmentId').val();
                                        if ($('#NationalCurrencyRate').val()) object.Card.NationalCurrencyRate = $('#NationalCurrencyRate').val() ? $('#NationalCurrencyRate').val() : 0;
                                        if ($('#Kontragent').val()) object.Card.KontragentId = $('#Kontragent').val()
                                        if ($('#HigherAgreement').val()) object.Card.HigherAgreementId = $('#HigherAgreement').val()


                                        if ($('#Signer').val()) {

                                            object.Card.SignerEmployeeId = $('#Signer').val()
                                            object.Card.SignerWorkplaceId = $('#Signer :selected').data('workplaceid')

                                        }

                                        if ($('#Responsible').val()) {
                                            object.Card.ResponsibleEmployeeId = $('#Responsible').val()
                                            object.Card.ResponsibleWorkPlaceId = $('#Responsible :selected').data('workplaceid')
                                        }


                                        if ($('#Summ').val()) object.Card.Summ = $('#Summ').val() ? $('#Summ').val() : 0;
                                        if ($('#KontragentSigner').val()) object.Card.KontragentSigner = $('#KontragentSigner').val()
                                        if ($('#KontragentSignerPhone').val()) object.Card.KontragentSignerPhone = $('#KontragentSignerPhone').val()
                                        if ($('#ConcludeDate').val() && $('#ConcludeDate').val() != '')
                                            object.Card.ConcludeDate = '/Date(' + Date.parse($('#ConcludeDate').val()) + ')/';
                                        else
                                            object.Card.ConcludeDate = null;

                                        if ($('#ContractTimeBegin').val() && $('#ContractTimeBegin').val() != '')
                                            object.Card.ContractTimeBegin = '/Date(' + Date.parse($('#ContractTimeBegin').val()) + ')/';
                                        else
                                            object.Card.OutDate = null;

                                        if ($('#ContractTimeEnd').val() && $('#ContractTimeEnd').val() != '')
                                            object.Card.ContractTimeEnd = '/Date(' + Date.parse($('#ContractTimeEnd').val()) + ')/';
                                        else
                                            object.Card.ContractTimeEnd = null;

                                        if ($('#ExecutionLimit').val() && $('#ExecutionLimit').val() != '')
                                            object.Card.ExecutionLimit = '/Date(' + Date.parse($('#ExecutionLimit').val()) + ')/';
                                        else
                                            object.Card.ExecutionLimit = null;
                                        if ($('#ContractNumber')) object.Card.ContractNumber = $('#ContractNumber').val()
                                        if ($('#NDS')) object.Card.NDS = $('#NDS').val() ? $('#NDS').val() : 0;
                                        if ($('#NDSRate').val())
                                            if (object.Card.NDSRate != null) {

                                                object.Card.NDSRate.id = $('#NDSRate').val();
                                                object.Card.NDSRate.Value = $('#NDSRate option:selected').text();

                                            } else {

                                                var data = {
                                                    id: $('#NDSRate').val(),
                                                    ReferenceType: 1308

                                                };

                                                object.Card.NDSRate = data;

                                            }
                                        if ($('#ContractType').val())
                                            if (object.Card.ContractType != null) {

                                                object.Card.ContractType.id = $('#ContractType').val();
                                                object.Card.ContractType.Value = $('#ContractType option:selected').text();

                                            } else {

                                                var data = {
                                                    id: $('#ContractType').val(),
                                                    ReferenceType: 1300

                                                };

                                                object.Card.ContractType = data;

                                            }
                                        if ($('#Importance').val())
                                            if (object.Card.Importance != null) {

                                                object.Card.Importance.id = $('#Importance').val();
                                                object.Card.Importance.Value = $('#Importance option:selected').text();

                                            } else {

                                                var data = {
                                                    id: $('#Importance').val(),
                                                    ReferenceType: 1301

                                                };

                                                object.Card.Importance = data;

                                            }
                                        if ($('#ContractSubject').val())
                                            if (object.Card.ContractSubject != null) {

                                                object.Card.ContractSubject.id = $('#ContractSubject').val();
                                                object.Card.ContractSubject.Value = $('#ContractSubject option:selected').text();

                                            } else {

                                                var data = {
                                                    id: $('#ContractSubject').val(),
                                                    ReferenceType: 1306

                                                };

                                                object.Card.ContractSubject = data;
                                            }

                                        if ($('#Currency').val())
                                            if (object.Card.Currency != null) {

                                                object.Card.Currency.id = $('#Currency').val();
                                                object.Card.Currency.Value = $('#Currency option:selected').text();

                                            } else {

                                                var data = {
                                                    id: $('#Currency').val(),
                                                    ReferenceType: 1307

                                                };

                                                object.Card.Currency = data;
                                            }
                                        if (object.Card.DeliveryType != null) {
                                            if ($('#DeliveryType').val()) {
                                                object.Card.DeliveryType.id = $('#DeliveryType').val();
                                                object.Card.DeliveryType.Value = $('#DeliveryType option:selected').text();
                                            }

                                        } else {
                                            if ($('#DeliveryType').val()) {
                                                var data = {
                                                    id: $('#DeliveryType').val(),
                                                    ReferenceType: 1018

                                                };

                                                object.Card.DeliveryType = data;
                                            }
                                        }
                                        if ($('#DocumentationList').val()) {
                                            var a = [];
                                            $.each(JSON.parse($('#DocumentationList').val()), function (index, el) {
                                                a.push({
                                                    id: el,
                                                    ReferenceType: 1305
                                                })
                                            });
                                            object.Card.DocumentationAgreement = {
                                                data_list: a
                                            };
                                        }
                                        if (object.Card.QuestCharacter && object.Card.QuestCharacter != null) {

                                            object.Card.QuestCharacter.id = $('#QuestCharacter').val();
                                            object.Card.QuestCharacter.Value = $('#QuestCharacter option:selected').text();

                                        } else {

                                            var data = {
                                                id: $('#QuestCharacter').val(),
                                                ReferenceType: 1003

                                            };

                                            object.Card.QuestCharacter = data;

                                        }

                                        if (object.Card.DeliveryType && object.Card.DeliveryType != null) {

                                            object.Card.DeliveryType.id = $('#DeliveryType').val();
                                            object.Card.DeliveryType.Value = $('#DeliveryType option:selected').text();

                                        } else {

                                            var data = {
                                                id: $('#DeliveryType').val(),
                                                ReferenceType: 1018

                                            };

                                            object.Card.DeliveryType = data;

                                        }

                                        if (object.Card.Recipients) {
                                            var data = [];
                                            if ($('#recipient-view').length) {
                                                // object.Card.Recipients = JSON.parse($('#Recipients').val());
                                                $.each($('#recipient-view').children(), function (index, el) {
                                                    data.push({
                                                        "ContractorType": 0,
                                                        "Id": $(el).val(),
                                                        "IsEmployee": true,
                                                        "Name": $(el).text()
                                                    })
                                                });

                                            } else {
                                                // object.Card.Recipients = JSON.parse($('#AccessList').val());
                                                $.each($('#access-view').children(), function (index, el) {
                                                    data.push({
                                                        "ContractorType": 0,
                                                        "Id": $(el).val(),
                                                        "IsEmployee": true,
                                                        "Name": $(el).text()
                                                    })
                                                });
                                            }
                                            object.Card.Recipients = data
                                        }


                                        object.Card.ListCount = $('#ListCount').val();
                                        object.Card.AttachCount = $('#AttachCount').val();
                                        object.Card.CopyCount = $('#CopyCount').val();
                                        object.Card.CorrespondentId = $('#Correspondent').val();

                                        if ($('#SignerName').is("select")) {
                                            if ($('#SignerName')) {
                                                object.Card.SignerEmployeeId = $('#SignerName').val();
                                                object.Card.SignerWorkplaceId = $('#SignerName option:selected').data('workplaceid');
                                                object.Card.SignerName = $('#SignerName option:selected').text();
                                            }
                                        } else {
                                            if ($('#SignerName'))
                                                object.Card.SignerName = $('#SignerName').val();
                                        }
                                        if ($('#ExecuterName').is("select")) {
                                            if ($('#ExecuterName')) {
                                                object.Card.ExecuterId = $('#ExecuterName').val();
                                                object.Card.ExecuterWorkplaceId = $('#ExecuterName option:selected').data('workplaceid');
                                                object.Card.ExecuterName = $('#ExecuterName option:selected').text();
                                            }
                                        } else {
                                            if ($('#ExecuterName'))
                                                object.Card.ExecuterName = $('#ExecuterName').val();
                                        }

                                        object.Card.Phone = $('#Phone').val();
                                        object.Card.OutNumber = $('#OutNumber').val();

                                        if ($('#OutDate').val() && $('#OutDate').val() != "") {
                                            object.Card.OutDate = '/Date(' + Date.parse($('#OutDate').val()) + ')/';
                                        } else {
                                            object.Card.OutDate = null;
                                        }
                                        if ($('#SignerName option:selected').text() != '') {
                                            object.Card.SignerEmployeeId = $('#SignerName').val();
                                            object.Card.SignerName = $('#SignerName option:selected').text();
                                        }
                                        object.AuthorName = $('#AuthorName').val();

                                        /*if ($('#CreateDate').val() && $('#CreateDate').val() != "") {
                                          object.CreateDate = '/Date('+Date.parse($('#CreateDate').val())+')/';
                                        } else {
                                          object.CreateDate = null;
                                        }*/


                                    } else if (type == 'Chancellery.Nomenclature') {
                                        object.Header = $('#Header').val();
                                        object.Code = $('#Code').val();
                                        if ($('#BeginDate').val() && $('#BeginDate').val() != "") {
                                            object.BeginDate = '/Date(' + Date.parse($('#BeginDate').val()) + ')/';
                                        }else{
                                            object.BeginDate = null;
                                        }
                                        if ($('#EndDate').val() && $('#EndDate').val() != "") {
                                            object.EndDate = '/Date(' + Date.parse($('#EndDate').val()) + ')/';
                                        }else{
                                            object.EndDate = null;
                                        }
                                        object.Comment = $('#Comment').val();
                                        object.IsClosed = $('#IsClosed').prop('checked');
                                        object.IsTransit = $('#IsTransit').prop('checked');
                                        object.ParentId = $('#Community').val();
                                        object.DocumentType = $('#DocumentType').val();
                                        if ($('#Counter').val() && $('#Counter').val() != "") {
                                            object.CounterId = $('#Counter').val();
                                        }else{
                                            object.CounterId = "00000000-0000-0000-0000-000000000000";
                                        }
                                        object.RegistrationPlace = {ReferenceType: 1019, id: $('#RegistrationPlace').val()};
                                        object.LifePeriod = {ReferenceType: 1020, id: $('#LifePeriod').val()};
                                        
                                    } else if (type == 'Chancellery.ContractorEnterprise') {

                                        object.Name = $('#Name').val();
                                        object.ShortName = $('#ShortName').val();
                                        object.IsResident = $('#IsResident').prop('checked');
                                        object.INN = $('#INN').val();
                                        object.KPP = $('#KPP').val();
                                        object.Type = $('#Type').val();

                                        if ($('#Sector').val().length != 0) {

                                            var res = null;
                                            var sector = $('#Sector').val();
                                            var i = 0;
                                            while (i < sector.length) {

                                                res = res | sector[i];

                                                i++;
                                            }

                                            object.Sector = res;

                                        } else {

                                            object.Sector = 0;

                                        }

                                        if (object.GuitEnterprise != null) {

                                            if ($('.DelRec').prop('checked') == true) {

                                                object.GuitEnterprise.DelRec = false;
                                                object.GuitEnterprise.Name = $('.GuitEnterprise .Name').val();
                                                object.GuitEnterprise.DepartmentCode = $('.GuitEnterprise .DepartmentCode').val();
                                                object.GuitEnterprise.Email = $('.GuitEnterprise .Email').val();

                                            } else {

                                                object.GuitEnterprise.DelRec = true;

                                            }

                                        } else {

                                            if ($('.DelRec').prop('checked') == true) {

                                                var NewGuitEnterprise = {
                                                    DelRec: false,
                                                    Name: $('.GuitEnterprise .Name').val(),
                                                    DepartmentCode: $('.GuitEnterprise .DepartmentCode').val(),
                                                    Email: $('.GuitEnterprise .Email').val(),
                                                    id: object.id,
                                                    is_new_record: true
                                                };

                                                object.GuitEnterprise = NewGuitEnterprise;

                                            }

                                        }

                                    } else if (type == 'Chancellery.ContractorPerson' || type == 'Chancellery.Declarant') {

                                        object.LastName = $('#LastName').val();
                                        object.FirstName = $('#FirstName').val();
                                        object.MiddleName = $('#MiddleName').val();

                                        if ($('#BirthDate').val() && $('#BirthDate').val() != "") {
                                            object.BirthDate = '/Date(' + Date.parse($('#BirthDate').val()) + ')/';
                                        } else {
                                            object.BirthDate = null;
                                        }

                                        object.Sex = $('#Sex').val();
                                        object.INN = $('#INN').val();
                                        object.Street = $('#Street').val();

                                    } else if (type == 'Chancellery.Counter') {

                                        object.Template = $('#Template').val();
                                        object.AccessList = JSON.parse($('#AccessList').val());
                                        object.DocumentType = $('#DocumentType').val();
                                        object.Year = $('#Year').val();
                                        object.Name = $('#Name').val();
                                        object.Index = $('#Index').val();
                                        object.Value = $('#Value').val();

                                        if (object.RegistrationPlace != null) {

                                            object.RegistrationPlace.id = $('#RegistrationPlace').val();
                                            object.RegistrationPlace.Value = $('#RegistrationPlace').text();

                                        } else {

                                            var data = {
                                                id: $('#RegistrationPlace').val(),
                                                ReferenceType: 1019

                                            };

                                            object.RegistrationPlace.push(data);

                                        }


                                    } else if (type == 'CitizenStatements.CitizenStatement') {

                                        object.FolderId = $('#Folder').val() ? $('#Folder').val() : '00000000-0000-0000-0000-000000000000';

                                        object.Card.ShortContent = $('#ShortContent').val();
                                        object.Theme = $('#ShortContent').val();
                                        object.Card.StatementForm = $('#StatementForm').val();

                                        if (object.Card.StatementType) {
                                            object.Card.StatementType.id = $('#StatementType').val();
                                            object.Card.StatementType.Value = $('#StatementType option:selected').text();
                                        } else if (object.Card.StatementType == null) {
                                            var data = {
                                                id: $('#StatementType').val(),
                                                ReferenceType: 1408
                                            };
                                            object.Card.StatementType = data;
                                        }

                                        object.Card.StatementCategory = $('#StatementCategory').val();

                                        object.Card.ListCount = $('#ListCount').val();
                                        object.Card.AttachCount = $('#AttachCount').val();

                                        if (object.Card.IncomingChannel) {
                                            object.Card.IncomingChannel.id = $('#IncomingChannel').val();
                                            object.Card.IncomingChannel.Value = $('#IncomingChannel option:selected').text();
                                        } else if (object.Card.IncomingChannel == null) {
                                            var data = {
                                                id: $('#IncomingChannel').val(),
                                                ReferenceType: 1400
                                            };
                                            object.Card.IncomingChannel = data;
                                        }

                                        if (object.Card.IncomingForm) {
                                            object.Card.IncomingForm.id = $('#IncomingForm').val();
                                            object.Card.IncomingForm.Value = $('#IncomingForm option:selected').text();
                                        } else if (object.Card.IncomingForm == null) {
                                            var data = {
                                                id: $('#IncomingForm').val(),
                                                ReferenceType: 1401
                                            };
                                            object.Card.IncomingForm = data;
                                        }

                                        object.Card.RequestFormat = $('#RequestFormat').val();
                                        object.Card.IsDirect = $('#IsDirect').prop('checked');

                                        if (object.Card.SubjectQuestion) {
                                            object.Card.SubjectQuestion.id = $('#SubjectQuestion').val();
                                            object.Card.SubjectQuestion.Value = $('#SubjectQuestion option:selected').text();
                                        } else if (object.Card.SubjectQuestion == null) {
                                            var data = {
                                                id: $('#SubjectQuestion').val(),
                                                ReferenceType: 1407
                                            };
                                            object.Card.SubjectQuestion = data;
                                        }

                                        object.Card.DeclarantId = $('#DeclarantId').val();;

                                        if (object.Card.SocialStatus) {
                                            object.Card.SocialStatus.id = $('#SocialStatus').val();
                                            object.Card.SocialStatus.Value = $('#SocialStatus option:selected').text();
                                        } else if (object.Card.SocialStatus == null) {
                                            var data = {
                                                id: $('#SocialStatus').val(),
                                                ReferenceType: 1402
                                            };
                                            object.Card.SocialStatus = data;
                                        }

                                        if (object.Card.BenefitCategory) {
                                            object.Card.BenefitCategory.id = $('#BenefitCategory').val();
                                            object.Card.BenefitCategory.Value = $('#BenefitCategory option:selected').text();
                                        } else if (object.Card.BenefitCategory == null) {
                                            var data = {
                                                id: $('#BenefitCategory').val(),
                                                ReferenceType: 1403
                                            };
                                            object.Card.BenefitCategory = data;
                                        }

                                        object.Card.AddressId = $('#AddressId').val();
                                        object.Card.AddressStreet = $('#AddressStreet').val();
                                        object.Card.AddressHouse = $('#AddressHouse').val();
                                        object.Card.AddressKorpus = $('#AddressKorpus').val();
                                        object.Card.AddressFlat = $('#AddressFlat').val();
                                        object.Card.AddressDetail = $('#AddressDetail').val();
                                        object.Card.DeclarantPhone = $('#DeclarantPhone').val();
                                        object.Card.DeclarantEmail = $('#DeclarantEmail').val();
                                        object.Card.Note = $('#Note').val();

                                        object.Card.CorrespondentId = $('#Correspondent').val() ? $('#Correspondent').val() : '00000000-0000-0000-0000-000000000000';
                                        object.Card.CorrespondentAuthor = $('#CorrespondentAuthor').val();
                                        object.Card.OutNumber = $('#OutNumber').val();
                                        object.Card.OutDate = $('#OutDate').val() ? '/Date(' + Date.parse($('#OutDate').val()) + ')/' : null
                                        object.Card.OuterControlDate = $('#OuterControlDate').val() ? '/Date(' + Date.parse($('#OuterControlDate').val()) + ')/' : null
                                        object.Card.AnswerDate = $('#AnswerDate').val() ? '/Date(' + Date.parse($('#AnswerDate').val()) + ')/' : null

                                        // object.Card.Recipients = JSON.parse($('#AccessList').val());

                                        var data = [];
                                        $.each($('#access-view').children(), function (index, el) {
                                            data.push({
                                                "ContractorType": 0,
                                                "Id": $(el).val(),
                                                "IsEmployee": true,
                                                "Name": $(el).text()
                                            })
                                        });
                                        object.Card.Recipients = data

                                        var id = $('li a[data-form="0202010"]').attr('href');
                                        if (id) {
                                            if ($(id).length) {
                                                var ob = [];
                                                $.each($('.attachments tbody tr'), function (ind, el) {
                                                    var type = $(el).find('.attach-type').val()
                                                    var data = {
                                                        "__type": "CitizenStatementAttachment:#Avrora.Objects.Modules.Docflow.DocflowObjects",
                                                        "id": $(el).data('id'),
                                                        "Group": type //Тип вложения

                                                    }
                                                    if ($(el).data('del') == true) {
                                                        data['DelRec'] = true;
                                                    }
                                                    ob.push(data);
                                                })
                                                object.Attachments = ob;
                                            }
                                        }

                                        $.each($('.question-tab'), function (i, e) {
                                            var index = $(e).data('data');
                                            var tab = $(e).attr('id');
                                            var item = object.Questions[index];

                                            if (item.Code) {
                                                item.Code.id = $('#' + tab + ' #CodeId').val();
                                                item.Code.Value = $('#' + tab + ' #Code').val();
                                            } else if (item.Code == null) {
                                                var data = {
                                                    id: $('#' + tab + ' #CodeId').val(),
                                                    ReferenceType: 1407
                                                };
                                                item.Code = data;
                                            }
                                            if (item.Code)
                                                if (item.SubQuestion) {
                                                    item.SubQuestion.id = $('#' + tab + ' #SubQuestion').val();
                                                    item.SubQuestion.Value = $('#' + tab + ' #SubQuestion option:selected').text();
                                                } else if (item.SubQuestion == null) {
                                                var data = {
                                                    id: $('#' + tab + ' #SubQuestion').val(),
                                                    ReferenceType: 1409
                                                };
                                                item.SubQuestion = data;
                                            }

                                            item.Subject = $('#' + tab + ' #Subject').val();
                                            item.Status = Number($('#' + tab + ' #Status').val());


                                            if (item.Status == 0 || item.Status == 10) {} else if (item.Status == 50) {
                                                if (item.TransferDepartment) {
                                                    item.TransferDepartment.id = $('#' + tab + ' #TransferDepartment').val();
                                                    item.TransferDepartment.Value = $('#' + tab + ' #TransferDepartment option:selected').text();
                                                } else if (item.TransferDepartment == null) {
                                                    var data = {
                                                        id: $('#' + tab + ' #TransferDepartment').val(),
                                                        ReferenceType: 1406
                                                    };
                                                    item.TransferDepartment = data;
                                                }
                                                item.TransferDate = $('#' + tab + ' #TransferDate').val() ? '/Date(' + Date.parse($('#' + tab + ' #TransferDate').val()) + ')/' : null
                                                item.TransferNumber = $('#' + tab + ' #TransferNumber').val();
                                            } else {
                                                item.ResponseDate = $('#' + tab + ' #ResponseDate').val() ? '/Date(' + Date.parse($('#' + tab + ' #ResponseDate').val()) + ')/' : null
                                                if (item.Status != 70) {
                                                    var empty = "00000000-0000-0000-0000-000000000000"
                                                    var noempty = "00000000-0000-0000-0000-000000000001"
                                                    if ($('#' + tab + ' #AnswerId').val() && $('#' + tab + ' #AnswerId').val() != empty) {
                                                        item.AnswerId = $('#' + tab + ' #AnswerId').val();
                                                        item.AttachmentId = empty;
                                                    } else {
                                                        item.AnswerId = empty;
                                                        if ($('#' + tab + ' #Attachment').val() && $('#' + tab + ' #Attachment').val() != empty) {
                                                            if (item.Attachment) {
                                                                if (item.Attachment.Name != $('#' + tab + ' #Attachment').val()) {
                                                                    item.AttachmentId = noempty;
                                                                }
                                                            } else {
                                                                item.AttachmentId = noempty;
                                                            }

                                                        } else {
                                                            item.AttachmentId = empty;
                                                        }
                                                    }

                                                }
                                                if (item.Status == 30) {
                                                    item.ActionsTaken = $('#' + tab + ' #ActionsTaken').prop('checked');
                                                    // $('#tab' + tabIndex+' #Answ .ActionsTaken').hide()
                                                }
                                            }

                                        })
                                    }


                                    if (type == 'Orders.Order') {

                                        object.SignerID = $('#SignerID').val();
                                        object.SignerWorkPlaceId = $('#SignerID option:selected').data('w');
                                        object.SignerName = $('#SignerID option:selected').text();

                                        object.Number = $('#Number').val();
                                        object.CounterId = $('#CounterId').val() ? $('#CounterId').val() : "00000000-0000-0000-0000-000000000000";
                                        object.CounterValue = $('#CounterValue').val() ? $('#CounterValue').val() : null;

                                        object.ResolutionDate = $('#ResolutionDate').val() ? '/Date(' + Date.parse($('#ResolutionDate').val()) + ')/' : null;

                                        var task = object.Tasks[0]
                                        task.ResolutionText = $('#ResolutionText').val();

                                        if (task.ControlType) {
                                            task.ControlType.id = $('#ControlType').val();
                                            task.ControlType.Value = $('#ControlType option:selected').text();
                                        } else if (task.ControlType == null) {
                                            var data = {
                                                id: $('#ControlType').val(),
                                                ReferenceType: 1004
                                            };
                                            task.ControlType = data;
                                        }
                                        task.InnerLimit = $('#InnerLimit').val() ? '/Date(' + Date.parse($('#InnerLimit').val()) + ')/' : null;
                                        task.Periodicity = $('#Periodicity').val()
                                        task.PeriodEndDate = $('#PeriodEndDate').val() ? '/Date(' + Date.parse($('#PeriodEndDate').val()) + ')/' : null;

                                        if (task.ControlPriority) {
                                            task.ControlPriority.id = $('#ControlPriority').val();
                                            task.ControlPriority.Value = $('#ControlPriority option:selected').text();
                                        } else if (task.ControlPriority == null) {
                                            var data = {
                                                id: $('#ControlPriority').val(),
                                                ReferenceType: 1008
                                            };
                                            task.ControlPriority = data;
                                        }

                                        task.IsEdited = true;
                                        task.ControlerEmployeeId = $('#ControlerName').val() ? $('#ControlerName').val() : "00000000-0000-0000-0000-000000000000";
                                        task.ControlerName = $('#ControlerName option:selected').text();
                                        task.ControlerWorkPlaceId = $('#ControlerName option:selected').data('w');
                                        task.IsSignerControl = $('#IsSignerControl').prop('checked')

                                        task.WebExecuterDataList = $('#AccessList0').val() ? {
                                            "Data": JSON.parse($('#AccessList0').val())
                                        } : null;

                                        if ($('#NewExecuterList0').val()) {
                                            ex = JSON.parse($('#NewExecuterList0').val());
                                            $.each(ex, function (index, element) {
                                                if (element.Svod != $('#' + element.id).prop('checked')) {
                                                    element.Svod = $('#' + element.id).prop('checked');
                                                }
                                            })
                                            task.Executers = ex;
                                        }

                                        result = {
                                            "order": object,
                                            "registerParam": !$("#FromReserv").val() ? null : {

                                                "SelectedCounterId": object.CounterId,
                                                "FromReserv": $("#FromReserv").val(),
                                                "ReserveItem": Boolean($("#FromReserv").val()) ? {
                                                    "Value": object.CounterValue
                                                } : null

                                            }
                                        }
                                        if (valid == 0) {

                                            $.ajax({
                                                "url": "http://" + ip + "/EAkimat/Order/Save",
                                                data: JSON.stringify(result),
                                                method: 'POST',
                                                beforeSend: function (xhr) {
                                                    xhr.setRequestHeader("content-type", "application/json");
                                                    xhr.setRequestHeader("Token", token);

                                                },
                                                success: function (data) {
                                                    if (data.Result == 'OK') {
                                                        $.notify(data.Message, {
                                                            type: "success"
                                                        });
                                                        var token = $.session.get("token");
                                                        setTimeout(function () {
                                                            if (action == 'Save') {} else if (action == 'SaveAndClose') {
                                                                if (openedObjed.Data.Object.Document) {
                                                                    objectId = openedObjed.Data.Object.Document.id
                                                                } else {
                                                                    objectId = openedObjed.Data.Object.id
                                                                }
                                                                var type = $('.ribbonBtn[data-action="' + action + '"]').data('type');
                                                                $.ajax({
                                                                    "url": "http://" + ip + "/EAkimat/open/" + type + "/?id=" + objectId,
                                                                    cache: false,
                                                                    beforeSend: function (xhr) {
                                                                        xhr.setRequestHeader("content-type", "application/json");
                                                                        xhr.setRequestHeader("Token", token);
                                                                    },
                                                                    success: function (data) {
                                                                        if (data.Result == 'OK') {
                                                                            $('.forcontent').html('');
                                                                            openedObjed = data;
                                                                            View(data);
                                                                        } else {
                                                                            $.notify(data.Message, {
                                                                                type: 'danger'
                                                                            });
                                                                        }
                                                                    }
                                                                })
                                                            }
                                                        }, 1000);
                                                    } else {
                                                        $.notify("Ошибка записи данных: " + data.Message, {
                                                            type: "danger"
                                                        });
                                                    }
                                                }
                                            })
                                        }

                                        return false;
                                    } else {

                                        if ($('#ResolutionText').length) {
                                            object.ResolutionText = $('#ResolutionText').val();

                                        }
                                        if ($('#InnerLimit').length) {
                                            object.InnerLimit = '/Date(' + Date.parse($('#InnerLimit').val()) + ')/';

                                        }
                                        if ($('#ExecutionResult').length) {
                                            object.ExecutionResult = $('#ExecutionResult').val() ? $('#ExecutionResult').val() : null;

                                        }
                                        if ($('#ExecutionComment').length) {
                                            object.ExecutionComment = $('#ExecutionComment').val() ? $('#ExecutionComment').val() : null;

                                        }
                                        if ($('#NewExecuterList2').val()) {
                                            ex = JSON.parse($('#NewExecuterList2').val());
                                            $.each(ex, function (index, element) {
                                                if (element.Svod != $('#' + element.id).prop('checked')) {
                                                    element.Svod = $('#' + element.id).prop('checked');
                                                }
                                            })
                                            object.Executers = ex;
                                            object.WebExecuterDataList = $('#AccessList2').val() ? {
                                                "Data": JSON.parse($('#AccessList2').val())
                                            } : null;
                                        }
                                        if ($('#ResolutionDate').length) {
                                            object.ResolutionDate = '/Date(' + Date.parse($('#ResolutionDate').val()) + ')/';
                                        }
                                        if ($('#SignerID').length) {
                                            object.SignerID = $('#SignerID').val();
                                            object.SignerName = $('#SignerID option:selected').text();
                                            object.SignerWorkPlaceId = $('#SignerID option:selected').data('w');
                                        }
                                        if ($('#WorkGroupChiefEmployeeId').length) {
                                            object.Card.WorkGroupChiefEmployeeId = $('#WorkGroupChiefEmployeeId').val()
                                            object.Card.WorkGroupChiefName = $('#WorkGroupChiefEmployeeId option:selected').text()
                                            object.Card.WorkGroupChiefWorkPlaceId = $('#WorkGroupChiefEmployeeId option:selected').data('w')
                                        }
                                        if ($('#ControlDate').length) {
                                            object.Card.ControlDate = '/Date(' + Date.parse($('#ControlDate').val()) + ')/'
                                        }
                                        if ($('#Annotation').length) {
                                            object.Card.Annotation = $('#Annotation').val();
                                        }
                                        if ($('.AL2Tabs').length) {
                                            $.each($('.AL2Tabs').children(), function (ind, el) {
                                                var id = $(el).data('id');
                                                var data = {
                                                    ResolutionText: $('#tab' + id + ' #ResolutionText').val(),
                                                    ControlType: $('#tab' + id + ' #ControlType').val(),
                                                    ControlValue: $('#tab' + id + ' #ControlType option:selected').text(),
                                                    ControlerEmployeeId: $('#tab' + id + ' #ControlerName').val() ? $('#tab' + id + ' #ControlerName').val() : "00000000-0000-0000-0000-000000000000",
                                                    Periodicity: Number($('#tab' + id + ' #Periodicity').val()),
                                                    PeriodEndDate: $('#tab' + id + ' #PeriodEndDate').val() ? $('#tab' + id + ' #PeriodEndDate').val() : null,
                                                    InnerLimit: $('#tab' + id + ' #InnerLimit').val() ? $('#tab' + id + ' #InnerLimit').val() : null,
                                                    ProlongationDateList: $('#tab' + id + ' #ProlongationDateList').val() ? $('#tab' + id + ' #ProlongationDateList').val() : null,
                                                    ControlPriority: $('#tab' + id + ' #ControlPriority').val(),
                                                    IsSignerControl: $('#tab' + id + ' #IsSignerControl').prop('checked')
                                                }

                                                object.Tasks[id].ResolutionText = data.ResolutionText;
                                                object.Tasks[id].IsSignerControl = data.IsSignerControl;

                                                object.Tasks[id].WebExecuterDataList = $('#tab' + id + ' #AccessList' + id).val() ? {
                                                    "Data": JSON.parse($('#tab' + id + ' #AccessList' + id).val())
                                                } : null;

                                                if ($('#tab' + id + ' #NewExecuterList' + id).val()) {
                                                    ex = JSON.parse($('#tab' + id + ' #NewExecuterList' + id).val());
                                                    $.each(ex, function (index, element) {
                                                        if (element.Svod != $('#' + element.id).prop('checked')) {
                                                            object.Tasks[id].IsEdited = true;
                                                            element.Svod = $('#' + element.id).prop('checked');
                                                        }
                                                    })
                                                    object.Tasks[id].Executers = ex;
                                                }

                                                
                                                object.Tasks[id].ControlType = {"id": data.ControlType, "value": data.ControlValue};

                                                object.Tasks[id].ControlerName = $('#tab' + id + ' #ControlerName option:selected').text();
                                                object.Tasks[id].ControlerEmployeeId = data.ControlerEmployeeId;
                                                object.Tasks[id].ControlerWorkPlaceId = $('#tab' + id + ' #ControlerName option:selected').data('w');
                                                object.Tasks[id].Periodicity = data.Periodicity;
                                                object.Tasks[id].PeriodEndDate = data.PeriodEndDate ? '/Date(' + Date.parse(data.PeriodEndDate) + ')/' : null;
                                                object.Tasks[id].InnerLimit = data.InnerLimit ? '/Date(' + Date.parse(data.InnerLimit) + ')/' : null;
                                                object.Tasks[id].ProlongationDateList = data.ProlongationDateList;
                                                object.Tasks[id].ControlPriority = {
                                                    id: data.ControlPriority,
                                                    ReferenceType: 1008
                                                };

                                                if (object.Tasks[id].IsEdited == false)

                                                console.log(object)
                                                console.log("============")
                                                console.log(data)
                                                console.log("============")
                                                console.log(taskState)

                                                    if (taskState[id].ResolutionText == object.Tasks[id].ResolutionText) {
                                                        if (taskState[id].ControlType != null && taskState[id].ControlType.id == object.Tasks[id].ControlType.id) {
                                                            if (taskState[id].ControlerEmployeeId == object.Tasks[id].ControlerEmployeeId) {
                                                                if (taskState[id].Periodicity == object.Tasks[id].Periodicity) {
                                                                    if ((taskState[id].PeriodEndDate ? DateFromMSDateForForm(taskState[id].PeriodEndDate) : null) == data.PeriodEndDate) {
                                                                        if ((taskState[id].InnerLimit ? DateFromMSDateForForm(taskState[id].InnerLimit) : null) == data.InnerLimit) {
                                                                            if (taskState[id].ProlongationDateList == object.Tasks[id].ProlongationDateList) {
                                                                                if (taskState[id].ControlPriority ? taskState[id].ControlPriority.id : null == object.Tasks[id].ControlPriority.id) {
                                                                                    if (taskState[id].IsSignerControl == object.Tasks[id].IsSignerControl) {
                                                                                        object.Tasks[id].IsEdited = false;
                                                                                    } else object.Tasks[id].IsEdited = true;
                                                                                } else object.Tasks[id].IsEdited = true;
                                                                            } else object.Tasks[id].IsEdited = true;
                                                                        } else object.Tasks[id].IsEdited = true;
                                                                    } else object.Tasks[id].IsEdited = true;
                                                                } else object.Tasks[id].IsEdited = true;
                                                            } else object.Tasks[id].IsEdited = true;
                                                        } else object.Tasks[id].IsEdited = true;
                                                    } else object.Tasks[id].IsEdited = true;

                                            })

                                        }

                                    }

                                    object.AddressId = $('#AddressId').val();
                                    object.Street = $('#Street').val();
                                    object.House = $('#House').val();
                                    object.Hull = $('#Hull').val();
                                    object.Apartment = $('#Apartment').val();
                                    

                                    if (object.ContactList != null) {
                                        
                                        $.each($('.phone-row'), function (i, v) {

                                            var phone_data = {};

                                            $(v).find('select, input').each(function (k, val) {

                                                if (val.name) {

                                                    phone_data[val.name] = val.value;

                                                }

                                            });

                                            if (phone_data.id != '') {

                                                $.each(object.ContactList.data_list, function (k, val) {

                                                    if (val.id == phone_data.id) {

                                                        object.ContactList.data_list[k].Type = phone_data.Type;
                                                        object.ContactList.data_list[k].Value = phone_data.Value;
                                                        object.ContactList.data_list[k].Description = phone_data.Description;

                                                    }

                                                })

                                            } else {

                                                var NewPhone = {
                                                    __type: "ContactPhoneNew:#Avrora.Objects.Common",
                                                    DelRec: false,
                                                    Type: phone_data.Type,
                                                    Value: phone_data.Value,
                                                    Description: phone_data.Description,
                                                    id: newGuid(),
                                                    is_new_record: true


                                                };

                                                object.ContactList.data_list.push(NewPhone);

                                            }

                                        })

                                        $.each($('.email-row'), function (i, v) {

                                            var email_data = {};

                                            $(v).find('select, input').each(function (k, val) {

                                                if (val.name) {

                                                    email_data[val.name] = val.value;

                                                }


                                            });

                                            if (email_data.id != '') {

                                                $.each(object.ContactList.data_list, function (k, val) {

                                                    if (val.id == email_data.id) {

                                                        object.ContactList.data_list[k].Type = email_data.Type;
                                                        object.ContactList.data_list[k].Value = email_data.Value;
                                                        object.ContactList.data_list[k].Description = email_data.Description;

                                                    }

                                                })

                                            } else {

                                                var NewEmail = {
                                                    __type: "ContactEMailNew:#Avrora.Objects.Common",
                                                    DelRec: false,
                                                    Type: email_data.Type,
                                                    Value: email_data.Value,
                                                    Description: email_data.Description,
                                                    id: newGuid(),
                                                    is_new_record: true


                                                };

                                                object.ContactList.data_list.push(NewEmail);

                                            }

                                        })

                                    } else if (object.ContactList || object.ContactList == null) {
                                        
                                        object.ContactList = {
                                            "data_list": []
                                        };
                                        
                                        $.each($('.phone-row'), function (i, v) {

                                            var phone_data = {};

                                            $(v).find('select, input').each(function (k, val) {

                                                if (val.name) {

                                                    phone_data[val.name] = val.value;

                                                }


                                            });

                                            if (phone_data.id != '') {

                                            } else {

                                                var NewPhone = {
                                                    __type: "ContactPhoneNew:#Avrora.Objects.Common",
                                                    DelRec: false,
                                                    Type: phone_data.Type,
                                                    Value: phone_data.Value,
                                                    Description: phone_data.Description,
                                                    id: newGuid(),
                                                    is_new_record: true


                                                };

                                                object.ContactList.data_list.push(NewPhone);

                                            }

                                        })

                                        $.each($('.email-row'), function (i, v) {

                                            var email_data = {};

                                            $(v).find('select, input').each(function (k, val) {

                                                if (val.name) {

                                                    email_data[val.name] = val.value;

                                                }


                                            });

                                            if (email_data.id != '') {

                                            } else {

                                                var NewEmail = {
                                                    __type: "ContactEMailNew:#Avrora.Objects.Common",
                                                    DelRec: false,
                                                    Type: email_data.Type,
                                                    Value: email_data.Value,
                                                    Description: email_data.Description,
                                                    id: newGuid(),
                                                    is_new_record: true

                                                };

                                                object.ContactList.data_list.push(NewEmail);

                                            }

                                        })

                                    }


                                    var result = {
                                        "parent": object
                                    };
                                    if(object.__type == "ContractorEnterprise:#Avrora.Objects.Common"){
                                        result["childrens"] = [AdditionalObject];
                                    }

                                    if (valid == 0) {
                                        var token = $.session.get('token');
                                        $.ajax({
                                            "url": "http://" + ip + "/EAkimat/Save/" + type,
                                            data: JSON.stringify(result),
                                            method: 'POST',
                                            beforeSend: function (xhr) {
                                                xhr.setRequestHeader("content-type", "application/json");
                                                xhr.setRequestHeader("Token", token);

                                            },
                                            success: function (data) {

                                                if (data.Result == 'OK') {
                                                    if (open_item.Data.FormId == '0501021') {
                                                        var files = [];
                                                        $.each($('.question-tab'), function (i, e) {
                                                            var index = $(e).data('data');
                                                            var tab = $(e).attr('id');
                                                            if ($('#' + tab + ' #exampleInputFile')[0].files[0])
                                                                files.push({
                                                                    file: $('#' + tab + ' #exampleInputFile')[0].files[0],
                                                                    quest: open_item.Data.Object.Document.Questions[index].id
                                                                })

                                                        })

                                                        function uploadFile(files, id) {
                                                            var token = $.session.get("token");
                                                            var xhr = new XMLHttpRequest();
                                                            var array = {
                                                                "Attachments": [

                                                                ]
                                                            };
                                                            var formd = new FormData();
                                                            $.each(files, function (i, e) {
                                                                array.Attachments.push({
                                                                    "__type": "Attachment:#Avrora.Objects.Modules.Docflow.CommonObjects",
                                                                    "ParentId": e.quest,
                                                                    "Name": e.file.name
                                                                })
                                                                formd.append('file' + i, e.file);
                                                            })

                                                            array = encodeURIComponent(JSON.stringify(array))
                                                            xhr.open('POST', 'http://' + ip + '/EAkimat/Save/CitizenStatement.Question.Attachments?documentId=' + id + '&attachmentArray=' + array, true);
                                                            xhr.setRequestHeader("Token", token);

                                                            xhr.upload.onprogress = function (e) {
                                                                if (e.lengthComputable) {
                                                                    $('.progress-bar').css('width', (e.loaded / e.total) * 100 + '%');
                                                                    $('.progress-bar').html(Math.round((e.loaded / e.total) * 100) + '%');
                                                                    $('.progress-bar').attr('aria-valuenow', (e.loaded / e.total) * 100 + '%');
                                                                }
                                                            };
                                                            xhr.onreadystatechange = function () {
                                                                if (xhr.readyState == 4 && xhr.status == 200) {
                                                                    var res = JSON.parse(xhr.response);

                                                                    if (res.Result == 'OK') {

                                                                        $.notify(res.Message, {
                                                                            type: "success"
                                                                        });
                                                                        if (action == 'Save') {

                                                                        } else if (action == 'SaveAndClose') {

                                                                            if (openedObjed.Data.Object.Document) {
                                                                                objectId = openedObjed.Data.Object.Document.id
                                                                            } else {
                                                                                objectId = openedObjed.Data.Object.id
                                                                            }
                                                                            var type = $('.ribbonBtn[data-action="' + action + '"]').data('type');
                                                                            $.ajax({
                                                                                "url": "http://" + ip + "/EAkimat/open/" + type + "/?id=" + objectId,
                                                                                cache: false,
                                                                                beforeSend: function (xhr) {
                                                                                    xhr.setRequestHeader("content-type", "application/json");
                                                                                    xhr.setRequestHeader("Token", token);
                                                                                },
                                                                                success: function (data) {
                                                                                    //debugger;
                                                                                    if (data.Result == 'OK') {
                                                                                        $('.forcontent').html('');
                                                                                        openedObjed = data;
                                                                                        View(data);
                                                                                    } else {
                                                                                        $.notify(data.Message, {
                                                                                            type: 'danger'
                                                                                        });
                                                                                    }
                                                                                }
                                                                            })
                                                                        }
                                                                    } else {

                                                                        $.notify("Ошибка: " + res.Message, {
                                                                            type: "danger"
                                                                        });

                                                                    }

                                                                }
                                                            };
                                                            xhr.send(formd);
                                                        }

                                                        uploadFile(files, open_item.Data.Object.Document.id);


                                                    } else {
                                                        $.notify(data.Message, {
                                                            type: "success"
                                                        });
                                                        var token = $.session.get("token");
                                                        setTimeout(function () {
                                                            if (action == 'Save') {
                                                                // $('.forcontent').empty()
                                                                // EditView(openedObjed, token, type)
                                                                // Ribbon(openedObjed)
                                                            } else if (action == 'SaveAndClose') {
                                                                //debugger;
                                                                // var form = openedObjed.Data.FormId;
                                                                // form = parseInt(form)-1
                                                                // openedObjed.Data.FormId = '0'+form;
                                                                var type = $('.ribbonBtn[data-action="' + action + '"]').data('type');
                                                                if (open_item.Data.FormId == '0205021') {
                                                                    objectId = open_item.Data.Object.Tasks[0].id
                                                                    type+='Task';
                                                                } else if (openedObjed.Data.Object.Document) {
                                                                    objectId = openedObjed.Data.Object.Document.id
                                                                } else {
                                                                    objectId = openedObjed.Data.Object.id
                                                                }
                                                                $.ajax({
                                                                    "url": "http://" + ip + "/EAkimat/open/" + type + "/?id=" + objectId,
                                                                    cache: false,
                                                                    beforeSend: function (xhr) {
                                                                        xhr.setRequestHeader("content-type", "application/json");
                                                                        xhr.setRequestHeader("Token", token);
                                                                    },
                                                                    success: function (data) {
                                                                        //debugger; 
                                                                        if (data.Result == 'OK') {
                                                                            $('.forcontent').html('');
                                                                            openedObjed = data;
                                                                            View(data);
                                                                        } else {
                                                                            $.notify(data.Message, {
                                                                                type: 'danger'
                                                                            });
                                                                        }
                                                                    }
                                                                })
                                                            }
                                                            //   setTimeout(function(){

                                                            //     $.ajax({
                                                            //   url: "views/"+$('.view-after-save').data('action')+".html",
                                                            //   cache: false,
                                                            //   success:function(data) {

                                                            //     $('.forcontent').html(data);

                                                            //   }

                                                            // })

                                                        }, 1000);
                                                    }


                                                } else {

                                                    $.notify("Ошибка записи данных: " + data.Message, {
                                                        type: "danger"
                                                    });

                                                }


                                            }
                                        })

                                    }

                                } else if (action == 'Close') {
                                    var token = $.session.get("token");
                                    var type = $('.ribbonBtn[data-action="' + action + '"]').data('type');
                                                                if (open_item.Data.FormId == '0205021') {
                                                                    objectId = open_item.Data.Object.Tasks[0].id
                                                                    type+='Task';
                                                                } else if (openedObjed.Data.Object.Document) {
                                                                    objectId = openedObjed.Data.Object.Document.id
                                                                } else {
                                                                    objectId = openedObjed.Data.Object.id
                                                                }
                                                                $.ajax({
                                                                    "url": "http://" + ip + "/EAkimat/open/" + type + "/?id=" + objectId,
                                                                    cache: false,
                                                                    beforeSend: function (xhr) {
                                                                        xhr.setRequestHeader("content-type", "application/json");
                                                                        xhr.setRequestHeader("Token", token);
                                                                    },
                                                                    success: function (data) {
                                                                        //debugger; 
                                                                        if (data.Result == 'OK') {
                                                                            $('.forcontent').html('');
                                                                            openedObjed = data;
                                                                            View(data);
                                                                        } else {
                                                                            $.notify(data.Message, {
                                                                                type: 'danger'
                                                                            });
                                                                        }
                                                                    }
                                                                })
                                } else if (action == 'AddTask') {
                      
                                    $.ajax({
                                      url: " http://" + ip + "/EAkimat/Documents.Resolution.AddTask?resolutionId="+obj.id,
                                      method: 'GET',
                                      async: false,
                                      beforeSend: function (xhr) {
                                        xhr.setRequestHeader("content-type", "application/json");
                                        xhr.setRequestHeader("Token", token);
                                      },
                                      success: function (data) {
                                       
                                        obj.Tasks.push(data.Data.Object);
                                        TaskParse(obj.Tasks.length-1, obj.Tasks[obj.Tasks.length-1], obj, '0205022')
                                        $.each($('.refToSelect, .refOut'),function(i, e){
                                          $('#'+$(e).attr('id')+'_chosen').remove();
                                          $(e).show();
                                        })
                                        LoadOutputRef('#tab'+(obj.Tasks.length-1));
                                        refToSelect('#tab'+(obj.Tasks.length-1));
                                        $.each($('#tab'+(obj.Tasks.length-1)+' select'), function(ind, el) {
                                          if ($(el).data('default')) {
                                            $(el).val($(el).data('default')?$(el).data('default') : null)
                                            $(el).trigger('chosen:updated');
                                          }
                                        })
                                        if ($('.AL2Links').children().length == 2) {
                                          $('.AL2Links').children().first().append('<a class="del-task" data-action="delete" data-id="'+$('.AL2Links').children().first().data('id')+'"><img title="Удалить" alt="Удалить" src="img/ui/delete_inactive.png"></a>')
                                        }
                                      }
                                    })
                              }
                            }
                        }
                    })

                }
            })

        }

    })

}