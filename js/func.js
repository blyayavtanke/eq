$.ajaxSetup({
  //async:false,
  xhrFields: {
    withCredentials: true
  }
});

// $.ajaxPrefilter(function( options, original_Options, jqXHR ) {
//   options.async = true;
// });

var myWorker;
var loadTime = 0;

var reference = [
  // "own",
  // "industry",
  // "phone",
  // "email",
  // "doctype",
  // "ContractConclusionTypes",
  // "WorkStatus",
  // "ResolutionPeriods",
  // "WorkStatus",
  // "StatementForm",
  // "StatementCategory",
  // "CitizenStatementRequestFormat",
  // "CitizenStatementQuestionStatus",
  // "AttachmentGroups",
  // "LinkTypesIncoming",
  // "LinkTypesOutgoing",
];

reference.own = [
  {
    Name: "Государственная собственность",
    Value: 1
  },
  {
    Name: "Частная собственность",
    Value: 2
  },
  {
    Name: "Муниципальная собственность",
    Value: 3
  },
  {
    Name: "Региональная собственность",
    Value: 4
  },
  {
    Name: "Федеральная собственность",
    Value: 5
  },
  {
    Name: "Смешанная собственность",
    Value: 6
  }
];
reference.industry = [
  {
    Name: "Не определено",
    Value: 0x00
  },
  {
    Name: "Промышленность",
    Value: 0x01
  },
  {
    Name: "Сельское хозяйство",
    Value: 0x02
  },
  {
    Name: "Лесное хозяйство",
    Value: 0x04
  },
  {
    Name: "Транспорт",
    Value: 0x08
  },
  {
    Name: "Связь",
    Value: 0x10
  },
  {
    Name: "Строительство",
    Value: 0x20
  },
  {
    Name: "Торговля и питание",
    Value: 0x40
  },
  {
    Name: "Материально-техническое обеспечение производства, сбыт",
    Value: 0x80
  },
  {
    Name: "Информационно-вычислительное обслуживание",
    Value: 0x100
  },
  {
    Name: "Общая коммерческая деятельность",
    Value: 0x200
  },
  {
    Name: "Геология, геодезия",
    Value: 0x400
  },
  {
    Name: "Жилищное хозяйство",
    Value: 0x800
  },
  {
    Name: "Коммунальное хозяйство",
    Value: 0x1000
  },
  {
    Name: "Здравоохранение и физическая культура",
    Value: 0x2000
  },
  {
    Name: "Образование",
    Value: 0x4000
  },
  {
    Name: "Культура и искусство",
    Value: 0x8000
  },
  {
    Name: "Наука (все фундаментальные НИИ)",
    Value: 0x10000
  },
  {
    Name: "Финансирование, кредитование, страхование",
    Value: 0x20000
  },
  {
    Name: "Управление",
    Value: 0x40000
  },
  {
    Name: "Государственная собственность",
    Value: 0x80000
  }
];
reference.phone = [
  {
    Name: "Мобильный",
    Value: 1
  },
  {
    Name: "Служебный",
    Value: 2
  },
  {
    Name: "Домашний",
    Value: 3
  }
];
reference.email = [
  {
    Name: "Служебная",
    Value: 1
  },
  {
    Name: "Личная",
    Value: 2
  }
];
reference.sex = [
  {
    Name: "Не определено",
    Value: 0
  },
  {
    Name: "Мужской",
    Value: 1
  },
  {
    Name: "Женский",
    Value: 2
  }
];
reference.doctype = [
  {
    Name: "",
    Value: 0
  },
  {
    Name: "ВСЕ",
    Value: 1
  },
  {
    Name: "Черновик",
    Value: 2
  },
  {
    Name: "Входящий документ",
    Value: 3
  },
  {
    Name: "Исходящий документ",
    Value: 4
  },
  {
    Name: "Внутренний документ",
    Value: 5
  },
  {
    Name: "Акт",
    Value: 6
  },
  {
    Name: "Договор",
    Value: 7
  },
  {
    Name: "Организационно-распорядительный документ",
    Value: 8
  },
  {
    Name: "Обращение граждан",
    Value: 9
  },
  {
    Name: "Контрольный документ",
    Value: 10
  }
];
reference.ContractConclusionTypes = [
  {
    Name: "44 ФЗ",
    Value: 0
  },
  {
    Name: "223 ФЗ",
    Value: 1
  },
  {
    Name: "Иные",
    Value: 2
  }
];
reference.Status = [
  {
    Description: "Создан",
    Value: 0
  },
  {
    Description: "Зарегистрирован",
    Value: 10
  },
  {
    Description: "Формирование плана мероприятий",
    Value: 20
  },
  {
    Description: "Отправлен на согласование",
    Value: 30
  },
  {
    Description: "Согласован",
    Value: 40
  },
  {
    Description: "Согласован с замечаниями",
    Value: 50
  },
  {
    Description: "Не согласован",
    Value: 60
  },
  //[Description("Утвержден")]
  //У,твержден = 70,
  {
    Description: "В работе",
    Value: 80
  },
  {
    Description: "Исполнен",
    Value: 90
  },
  {
    Description: "На доработке",
    Value: 100
  },
  {
    Description: "Подписан",
    Value: 110
  }
];
reference.ResolutionPeriods = [
  {
    Name: "",
    Value: 0
  },
  {
    Name: "единовременно",
    Value: 1
  },
  {
    Name: "ежедневно",
    Value: 7
  },
  {
    Name: "неделя",
    Value: 2
  },
  {
    Name: "месяц",
    Value: 3
  },
  {
    Name: "квартал",
    Value: 4
  },
  {
    Name: "полугодие",
    Value: 5
  },
  {
    Name: "год",
    Value: 6
  }
];
reference.WorkStatus = [
  {
    Name: "Новое",
    Value: 0
  },
  {
    Name: "В работе",
    Value: 1
  },
  {
    Name: "Дан ответ",
    Value: 2
  },
  {
    Name: "Выполнено",
    Value: 3
  },
  {
    Name: "Доработать",
    Value: 4
  },
  {
    Name: "Просрочено",
    Value: 5
  },
  {
    Name: "Отменено",
    Value: 7
  },
  {
    Name: "Закрыто",
    Value: 15
  }
];
reference.StatementForm = [
  {
    Description: "Письменное",
    Value: 0
  },
  {
    Description: "Устное",
    Value: 1
  },
  {
    Description: "Личный прием",
    Value: 2
  }
];
reference.StatementCategory = [
  {
    Description: "Личное",
    Value: 0
  },
  {
    Description: "Коллективное",
    Value: 1
  },
  {
    Description: "Анонимное",
    Value: 2
  }
];
reference.CitizenStatementRequestFormat = [
  {
    Description: "Другое",
    Value: 0
  },
  {
    Description: "Электронный",
    Value: 1
  }
];
reference.CitizenStatementQuestionStatus = [
  {
    Description: "Не зарегистрировано",
    Value: 0
  },
  {
    Description: "Находится на рассмотрении",
    Value: 10
  },
  {
    Description: "Рассмотрено. Разъяснено",
    Value: 20
  },
  {
    Description: "Рассмотрено. Поддержано",
    Value: 30
  },
  {
    Description: "Рассмотрено. Не поддержано",
    Value: 40
  },
  {
    Description: "Направлено по компетенции",
    Value: 50
  },
  {
    Description: "Дан ответ автору",
    Value: 60
  },
  {
    Description: "Оставлено без ответа автору",
    Value: 70
  }
];
reference.AttachmentGroups = [
  {
    Description: "Другое",
    Value: 0
  },
  {
    Description: "Электронный образ обращения",
    Value: 1
  },
  {
    Description: "Электронный образ входящего документа",
    Value: 2
  }
];
reference.LinkTypesIncoming = [
  {
    Description: "Ответный документ",
    Value: 1
  },
  {
    Description: "Документ, на который дается ответ",
    Value: 2
  }
];
reference.LinkTypesOutgoing = [
  {
    Description: "Документ, на который дается ответ",
    Value: 1
  },
  {
    Description: "Ответный документ",
    Value: 2
  }
];

function getMCounter() {
  var token = $.session.get("token");

  $.ajax({
    url: "http://" + ip + "/EAkimat/NotificationCenter.Message.Counter",
    //async: false,
    beforeSend: function(xhr) {
      xhr.setRequestHeader("content-type", "application/json");
      xhr.setRequestHeader("Token", token);
    },
    success: function(data) {
      $(".CM").html(data);
    }
  });
}

function validate(form) {
  var errors = "";

  $(form)
    .find("input, text, select, textarea")
    .each(function(k, v) {
      if ($(v).attr("required")) {
        if ($(v).attr("id") == "INN") {
          if (
            String($(v).val()).length != 10 &&
            String($(v).val()).length != 12 &&
            $("#IsResident").prop("checked") == true
          ) {
            $(v).addClass("has-error");
            errors +=
              "<div>" +
              $(v)
                .prev()
                .text() +
              "</div>";
          } else {
            $(v).removeClass("has-error");
          }
        } else if ($(v).attr("id") == "NewExecuterList2") {
          if ($(v).val() == "") {
            $(v).addClass("has-error");
            errors += "<div>Исполнители</div>";
          } else {
            var propC = 0;
            $.each($("input[name=r]"), function(index, elem) {
              if ($(elem).prop("checked") == true) propC++;
            });
            if (propC == 0) {
              errors += "<div>Свод</div>";
            }
            $(v).removeClass("has-error");
          }
        } else if ($(v).attr("id") == "exampleInputFile") {
          if ($(v).val() == "") {
            $(v).addClass("has-error");
            errors += "<div>Выберите файл вложения</div>";
          } else {
            $(v).removeClass("has-error");
          }
        } else if ($(v).attr("id") == "KPP" && $(v).val() != "") {
          var regexp = /\d{4}(\d|[A-Z])(\d|[A-Z])\d{3}/;
          if (String($(v).val()).search(regexp) == -1) {
            $(v).addClass("has-error");
            errors +=
              "<div>" +
              $(v)
                .prev()
                .text() +
              "</div>";
          } else {
            $(v).removeClass("has-error");
          }
        } else if (
          $(v).attr("id") == "Name" ||
          $(v).attr("id") == "Text" ||
          $(v).attr("id") == "FromID" ||
          $(v).attr("id") == "ReceiveDate" ||
          $(v).attr("id") == "RegNumber" ||
          $(v).attr("id") == "RegDate" ||
          $(v).attr("id") == "Executer" ||
          $(v).attr("id") == "ExecuterPhone"
        ) {
          if ($(v).val() == "") {
            $(v).addClass("has-error");
            errors +=
              "<div>" +
              $(v)
                .prev()
                .text() +
              "</div>";
          } else {
            $(v).removeClass("has-error");
          }
        } else if ($(v).attr("id") == "Year") {
          if ($(v).val() == "") {
            $(v).addClass("has-error");
            errors +=
              "<div>" +
              $(v)
                .prev()
                .text() +
              "</div>";
          } else {
            $(v).removeClass("has-error");
          }
        } else if ($(v).attr("id") == "Index") {
          if ($(v).val() == "") {
            $(v).addClass("has-error");
            errors +=
              "<div>" +
              $(v)
                .prev()
                .text() +
              "</div>";
          } else {
            $(v).removeClass("has-error");
          }
        } else if ($(v).attr("id") == "DocumentType") {
          if ($(v).val() == 0) {
            $(v).addClass("has-error");
            errors +=
              "<div>" +
              $(v)
                .prev()
                .text() +
              "</div>";
          } else {
            $(v).removeClass("has-error");
          }
        } else if ($(v).attr("id") == "RegistrationPlace") {
          if ($(v).val() == "" || $(v).val() == null) {
            $(v).addClass("has-error");
            errors +=
              "<div>" +
              $(v)
                .prev()
                .text() +
              "</div>";
          } else {
            $(v).removeClass("has-error");
          }
        } else if ($(v).attr("id") == "Template") {
          if ($(v).val() == "") {
            $(v).addClass("has-error");
            errors +=
              "<div>" +
              $(v)
                .prev()
                .text() +
              "</div>";
          } else {
            $(v).removeClass("has-error");
          }
        } else if ($(v).attr("id") == "AccessList") {
          if ($(v).val() == "") {
            $(v).addClass("has-error");
            errors +=
              "<div>" +
              $(v)
                .prev()
                .text() +
              "</div>";
          } else {
            $(v).removeClass("has-error");
          }
        } else if ($(v).attr("id") == "FirstName") {
          if ($(v).val() == "") {
            $(v).addClass("has-error");
            errors +=
              "<div>" +
              $(v)
                .prev()
                .text() +
              "</div>";
          } else {
            $(v).removeClass("has-error");
          }
        } else if ($(v).attr("id") == "LastName") {
          if ($(v).val() == "") {
            $(v).addClass("has-error");
            errors +=
              "<div>" +
              $(v)
                .prev()
                .text() +
              "</div>";
          } else {
            $(v).removeClass("has-error");
          }
        } else if ($(v).attr("id") == "Sex") {
          if ($(v).val() == "") {
            $(v).addClass("has-error");
            errors +=
              "<div>" +
              $(v)
                .prev()
                .text() +
              "</div>";
          } else {
            $(v).removeClass("has-error");
          }
        } else if ($(v).attr("id") == "Correspondent") {
          if ($(v).val() == "" || $(v).val() == null) {
            errors +=
              "<div>" +
              $(v)
                .prev()
                .text() +
              "</div>";
            $(v).addClass("has-error");
          } else {
            $(v).removeClass("has-error");
          }
        } else if ($(v).attr("name") == "Type") {
          if ($(v).val() == "") {
            $(v).addClass("has-error");
            errors +=
              "<div>" +
              $(v)
                .prev()
                .text() +
              "</div>";
          } else {
            $(v).removeClass("has-error");
          }
        } else if ($(v).attr("name") == "Value") {
          if (
            $(v)
              .parent()
              .parent("div.email-row").length != 0
          ) {
            if (
              $(v).val() == "" ||
              String($(v).val()).search(
                /\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/
              ) == -1
            ) {
              $(v).addClass("has-error");
              errors +=
                "<div>" +
                $(v)
                  .prev()
                  .text() +
                "</div>";
            } else {
              $(v).removeClass("has-error");
            }
          } else {
            if ($(v).val() == "") {
              $(v).addClass("has-error");
              errors +=
                "<div>" +
                $(v)
                  .prev()
                  .text() +
                "</div>";
            } else {
              $(v).removeClass("has-error");
            }
          }
        } else if ($(v).attr("id") == "ContractSubject") {
          if ($(v).val() == 0) {
            $(v).addClass("has-error");
            errors +=
              "<div>" +
              $(v)
                .prev()
                .text() +
              "</div>";
          } else {
            $(v).removeClass("has-error");
          }
        } else if (
          $(v).attr("id") == "Theme" ||
          $(v).attr("id") == "Nomenclature" ||
          $(v).attr("id") == "QuestCharacter" ||
          $(v).attr("id") == "DocumentType" ||
          $(v).attr("id") == "SignerName" ||
          $(v).attr("id") == "OutNumber" ||
          $(v).attr("id") == "OutDate" ||
          $(v).attr("id") == "AccessList" ||
          $(v).attr("id") == "ConcludeDate" ||
          $(v).attr("id") == "Signer" ||
          $(v).attr("id") == "ContractTimeBegin" ||
          $(v).attr("id") == "ContractTimeEnd" ||
          $(v).attr("id") == "Summ" ||
          $(v).attr("id") == "ConclusionBy" ||
          $(v).attr("id") == "Kontragent" ||
          $(v).attr("id") == "ExecuterName" ||
          $(v).attr("id") == "CarrierType" ||
          $(v).attr("id") == "Languages"
        ) {
          if ($(v).val() == null || $(v).val() == "") {
            $(v).addClass("has-error");
            errors +=
              "<div>" +
              $(v)
                .prev()
                .text() +
              "</div>";
          } else $(v).removeClass("has-error");
        } else if (
          $(v).attr("id") == "ListCount" ||
          $(v).attr("id") == "AttachCount" ||
          $(v).attr("id") == "CopyCount"
        ) {
          var noty = {
            ListCount: "Количество листов",
            AttachCount: "Количество приложений",
            CopyCount: "Количество экземпляров"
          };

          if (!/^[0-9]{1,5}$/.test($(v).val())) {
            //   $(v).addClass('has-error');
            $(v).val(0);
            //   errors += '<div>' + noty[$(v).attr('id')] + '</div>';
          } else $(v).removeClass("has-error");
        } else if ($(v).attr("id") == "Recipients") {
          if ($(v).val() == "") {
            $(v).addClass("has-error");
            errors +=
              "<div>" +
              $(v)
                .parent()
                .parent()
                .prev()
                .text() +
              "</div>";
          } else {
            $(v).removeClass("has-error");
          }
        } else if (
          $(v).attr("id") == "CodeId" ||
          $(v).attr("id") == "DeclarantId"
        ) {
          if (
            $(v).val() == "" ||
            $(v).val() == null ||
            $(v).val() == "00000000-0000-0000-0000-000000000000"
          ) {
            $(v).addClass("has-error");
            errors +=
              "<div>" +
              $(v)
                .parent()
                .prev()
                .text() +
              "</div>";
          } else {
            $(v).removeClass("has-error");
          }
        } else if ($(v).attr("id") == "Address") {
          if ($(v).val() == "") {
            $(v).addClass("has-error");
            errors +=
              "<div>" +
              $(v)
                .parent()
                .prev()
                .text() +
              "</div>";
          } else {
            $(v).removeClass("has-error");
          }
        } else {
          if (($(v).val() == "" || $(v).val() == null) && $(v).attr("id")) {
            $(v).addClass("has-error");
            errors +=
              "<div>" +
              $(v)
                .prev()
                .text() +
              "</div>";
          } else {
            $(v).removeClass("has-error");
          }
        }
      }
    });

  $(".GuitEnterprise")
    .find("input, text, select")
    .each(function(k, v) {
      if ($(v).attr("required")) {
        if ($(v).attr("Name") == "Name") {
          if ($(v).val() == "" && $(".DelRec").prop("checked") == true) {
            $(v).addClass("has-error");
            errors +=
              "<div>" +
              $(v)
                .prev()
                .text() +
              "</div>";
          } else {
            $(v).removeClass("has-error");
          }
        }

        if ($(v).attr("Name") == "Email") {
          if (
            $(".DelRec").prop("checked") == true &&
            ($(v).val() == "" ||
              String($(v).val()).search(
                /\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/
              ) == -1)
          ) {
            $(v).addClass("has-error");
            errors +=
              "<div>" +
              $(v)
                .prev()
                .text() +
              "</div>";
          } else {
            $(v).removeClass("has-error");
          }
        }
      }
    });
  function replaceAll(str, find, replace) {
    return str.replace(new RegExp(find, "g"), replace);
  }

  if (errors != "") {
    var errorStr = replaceAll(errors, "</div><div>", ", ");
    errorStr = replaceAll(errorStr, "</div>", "");
    errorStr = replaceAll(errorStr, "<div>", "");
    $.notify("Ошибка проверки данных: " + errorStr, {
      type: "danger"
    });
  } else {
    return 0;
  }
}

var Kladr = {
  getDataKladr: function(param, token) {
    var res;

    $.ajax({
      url: "http://" + ip + "/EAkimat//Kladr/list?id=" + param,
      async: false,
      beforeSend: function(xhr) {
        xhr.setRequestHeader("content-type", "application/json");
        xhr.setRequestHeader("Token", token);
      },
      success: function(data) {
        res = data;
      }
    });

    return res;
  }
};

function getKladr(target, target2) {
  var token = $.session.get("token");
  var res = {};
  var html;
  var breadcrumbs = [];

  res = Kladr.getDataKladr(0, token);

  $.each(res, function(k, v) {
    html +=
      '<option value="' +
      v[0] +
      '" data-flag="' +
      v[2] +
      '">' +
      v[1] +
      "</option>";
  });

  $(target).html(html);

  function checkBCandAdd(item, target2) {
    if (typeof item == "undefined") {
      item = null;
    }
    var html = "";

    if (item != null) {
      if (breadcrumbs.length != 0) {
        var last = breadcrumbs.pop();

        if (last.flag == true) {
          breadcrumbs.push(last);
          breadcrumbs.push(item);
        } else {
          breadcrumbs.push(item);
        }
      } else {
        breadcrumbs.push(item);
      }
    }

    var len = breadcrumbs.length;
    var ii = 0;

    $.each(breadcrumbs, function(i, v) {
      if (++ii != len) {
        html += '<a data-id="' + v.id + '">' + v.text + "</a>, ";
      } else {
        if (v.flag == true) {
          html += '<a data-id="' + v.id + '">' + v.text + "</a>";
        } else {
          html += v.text;
        }
      }
    });

    $(target2).html(html);
  }

  $(target).on("click", "option", function() {
    var flag = $(this).data("flag");
    var id = $(this).val();
    var addr = $(this).text();
    $("#filterAddr").val("");

    $(".kladr-id").val(id);

    checkBCandAdd(
      {
        id: id,
        text: addr,
        flag: flag
      },
      target2
    );

    if (flag == true) {
      res = Kladr.getDataKladr(id, token);
      html = "";
      $.each(res, function(k, v) {
        html +=
          '<option value="' +
          v[0] +
          '" data-flag="' +
          v[2] +
          '">' +
          v[1] +
          "</option>";
      });

      $(target).html(html);
    } else {
      $(target).hide();
      $("#filterAddr").hide();
    }
  });

  $("#kladr-bread").on("click", "a", function() {
    $("#filterAddr").val("");
    $(target).show();
    $("#filterAddr").show();

    var id = $(this).data("id");
    $(".kladr-id").val(id);
    var new_breadcrumbs = [];

    if (id == 0) {
      $("#kladr-bread span").html("");
      breadcrumbs = [];
    } else {
      $.each(breadcrumbs, function(i, v) {
        if (id != v.id) {
          new_breadcrumbs.push(v);
        } else {
          new_breadcrumbs.push(v);
          return false;
        }
      });

      breadcrumbs = new_breadcrumbs;
      checkBCandAdd(null, target2);
    }

    res = Kladr.getDataKladr(id, token);
    html = "";
    $.each(res, function(k, v) {
      html +=
        '<option value="' +
        v[0] +
        '" data-flag="' +
        v[2] +
        '">' +
        v[1] +
        "</option>";
    });

    $(target).html(html);
  });

  $("#filterAddr").on("keyup", function() {
    $("#kladr option").hide();

    $("#kladr option").each(function(i, v) {
      if (
        ~$(v)
          .text()
          .toLowerCase()
          .indexOf(
            $("#filterAddr")
              .val()
              .toLowerCase()
          )
      ) {
        $(v).show();
      }
    });
  });

  $(".save-kladr").on("click", function() {
    $("#AddressId").val($(".kladr-id").val());
    $("#Address").val(getAddress($(".kladr-id").val()));

    if (String($(".kladr-id").val()).length == 13) {
      $(".Street").show();
    } else {
      $(".Street").hide();
    }
  });
}

function AccessList2(id, num, needGroup) {
  if (typeof num == "undefined") {
    num = null;
  }
  if (typeof needGroup == "undefined") {
    needGroup = true;
  }
  var token = $.session.get("token");

  $.ajax({
    url: " http://" + ip + "/EAkimat/SelectEmsObjects",
    method: "post",
    data: JSON.stringify({
      param: {
        LevelDirection: 1,
        ShowWorkPlace: true,
        ShowRoles: true,
        ShowGroups: true,
        SelectedEmsObjectsId: id
      }
    }),
    beforeSend: function(xhr) {
      xhr.setRequestHeader("content-type", "application/json");
      xhr.setRequestHeader("Token", token);
    },
    success: function(data) {
      var options = "";
      var options2 = "";
      var group = null;
      var groups = [];
      var selected = [];
      var noselected = [];
      var access = [];
      $(".access-select" + num).empty();
      $(".access-view" + num).empty();
      $.each(data.Data.Object.Data, function(key, val) {
        if (!needGroup) {
          if (val[2] != "Группы")
            if (val[3] == "False") {
              $(".access-select" + num).append(
                '<option data-v="' +
                  val[3] +
                  '" data-group="' +
                  val[2] +
                  '" value="' +
                  val[0] +
                  '">' +
                  val[1] +
                  "</option>"
              );
              // selected.push('<option data-group="'+val[2]+'" data-v="'+val[3]+'" value="' + val[0] + '">' + val[1] + '</option>');
            } else {
              access.push(val);
              $(".access-view" + num).append(
                '<option data-v="' +
                  val[3] +
                  '" data-group="' +
                  val[2] +
                  '" value="' +
                  val[0] +
                  '">' +
                  val[1] +
                  "</option>"
              );
              // noselected.push('<option data-group="'+val[2]+'" data-v="'+val[3]+'" value="' + val[0] + '">' + val[1] + '</option>');
            }
        } else {
          if (groups.indexOf(val[2]) == -1) {
            if (val[2]) {
              groups.push(val[2]);
              $(".access-select" + num).append(
                "<optgroup label=" + val[2] + "></optgroup>"
              );
              $(".access-view" + num).append(
                "<optgroup label=" + val[2] + "></optgroup>"
              );
            }
          }

          if (val[3] == "False") {
            $(
              ".access-select" + num + " optgroup[label=" + val[2] + "]"
            ).append(
              '<option data-group="' +
                val[2] +
                '" data-v="' +
                val[3] +
                '" value="' +
                val[0] +
                '">' +
                val[1] +
                "</option>"
            );
            // selected.push('<option data-group="'+val[2]+'" data-v="'+val[3]+'" value="' + val[0] + '">' + val[1] + '</option>');
          } else {
            access.push(val);
            $(".access-view" + num + " optgroup[label=" + val[2] + "]").append(
              '<option data-group="' +
                val[2] +
                '" data-v="' +
                val[3] +
                '" value="' +
                val[0] +
                '">' +
                val[1] +
                "</option>"
            );
            // noselected.push('<option data-group="'+val[2]+'" data-v="'+val[3]+'" value="' + val[0] + '">' + val[1] + '</option>');
          }
        }

        // if (group == null) {

        //   group = val[2];

        //   if (val[3] == "False") {
        //     options = '<optgroup label='+group+'>';
        //     options += '<option data-group="'+group+'" data-v="'+val[3]+'" value="' + val[0] + '">' + val[1] + '</option>';
        //   } else {
        //     options2 = '<optgroup label='+group+'>';
        //     options2 += '<option data-group="'+group+'" data-v="'+val[3]+'" value="' + val[0] + '">' + val[1] + '</option>';
        //   }

        // } else {

        //   if (group != val[2]){
        //     group = val[2];
        //     if (val[3] == "False") {
        //       options += '</optgroup><optgroup label='+group+'>';
        //       options += '<option data-group="'+group+'" data-v="'+val[3]+'" value="' + val[0] + '">' + val[1] + '</option>';
        //     } else {
        //       options2 += '</optgroup><optgroup label='+group+'>';
        //       options2 += '<option data-group="'+group+'" data-v="'+val[3]+'" value="' + val[0] + '">' + val[1] + '</option>';
        //     }
        //   } else {
        //     if (val[3] == "False") {
        //       options += '<option data-group="'+group+'" data-v="'+val[3]+'" value="' + val[0] + '">' + val[1] + '</option>';
        //     } else {
        //       options2 += '<option data-group="'+group+'" data-v="'+val[3]+'" value="' + val[0] + '">' + val[1] + '</option>';
        //     }
        //   }

        // }
      });
      $("#AccessList" + num).val(JSON.stringify(access));
      // $.each(groups, function(i, e) {
      //   $('.access-select'+num).append('<optgroup label='+e+'></optgroup>')
      //   $('.access-view'+num).append('<optgroup label='+e+'></optgroup>')
      // })
      // $.each(selected, function(i,e) {
      //   $('.access-select'+num+' optgroup[label='+)
      // })
      // $('.access-select'+num).html(options);
      // $('.access-view'+num).html(options2);
    }
  });
  // $('#access-view').on('click', 'option', function () {

  //   $(this).remove();
  //   $(this).appendTo("#access-select");

  // })

  // $('#access-select').on('click', 'option', function () {

  //   $(this).remove();
  //   $(this).appendTo("#access-view");

  // })

  // $('.save-access').on('click', function () {

  //   var access = [];
  //   var tr = '';

  //   $('#access-view option').each(function (i, v) {

  //     access.push($(v).val());
  //     tr += '<tr><td>' + $(v).text() + '</td></tr>';

  //   })

  //   $('#AccessList').val(JSON.stringify(access));
  //   $('#AccessListTable').html(tr);

  // })
}

function newGuid() {
  function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
  }
  return (
    s4() +
    s4() +
    "-" +
    s4() +
    "-" +
    s4() +
    "-" +
    s4() +
    "-" +
    s4() +
    s4() +
    s4()
  );
}

function LoadOutputRef(container) {
  if (typeof container == "undefined") {
    container = "";
  }
  var refs = [];
  var token = $.session.get("token");
  var param;
  var data = null;
  if (container == "") var elems = $(".refOut");
  else elems = $(container + " .refOut");

  $.each(elems, function(k, v) {
    var refid = $(v).data("refid");
    if ($(v).data("default")) {
      var def = $(v).data("default");
    } else var def = "";

    if (refid && refid != "") {
      param = "References?reftype=" + refid;
    }

    $.ajax({
      url: " http://" + ip + "/EAkimat/" + param,
      async: false,
      beforeSend: function(xhr) {
        xhr.setRequestHeader("content-type", "application/json");
        xhr.setRequestHeader("Token", token);
      },
      success: function(data) {
        var options = '<option value="">Не определено</option>';

        $.each(data, function(key, val) {
          options +=
            '<option value="' +
            val.id +
            '" ' +
            (def == val.id ? "selected" : "") +
            ">" +
            val.Value +
            "</option>";
        });

        $(v).html(options);
        $(v).chosen({
          width: "100%",
          allow_single_deselect: false
        });
        //$('#'+k).срщыут("chosen:updated");
      }
    });
  });
}

function LoadRef() {
  var elems = $(".ref");

  $.each(elems, function(k, v) {
    var ref = $(v).data("ref");
    var options = "";

    $.each(reference[ref], function(key, val) {
      options += '<option value="' + val.Value + '">' + val.Name + "</option>";
    });

    $(v).html(options);
    $(v).chosen({
      width: "100%"
    });
  });
}

function LoadRefAtOne(ref, target, selected) {
  if (typeof selected == "undefined") {
    selected = null;
  }
  var options = "";

  $.each(reference[ref], function(key, val) {
    options += '<option value="' + val.Value + '">' + val.Name + "</option>";
  });

  if (selected == null) {
    $(target).html(options);
  } else {
    $(target)
      .html(options)
      .val(selected);
  }

  $(target).chosen({
    width: "100%"
  });
}

function getDataFromRef(ref, value) {
  var res;

  $.each(reference[ref], function(k, v) {
    if (value == v.Value) {
      res = v.Name;
      return;
    }
  });

  return res;
}

function getAddress(value) {
  var token = $.session.get("token");
  var res;

  $.ajax({
    url: "http://" + ip + "/EAkimat//Kladr/address?id=" + value,
    async: false,
    beforeSend: function(xhr) {
      xhr.setRequestHeader("content-type", "application/json");
      xhr.setRequestHeader("Token", token);
    },
    success: function(data) {
      res = data;
    }
  });

  return res;
}

function DateFromMSDate(date) {
  if (date) {
    var myDate = eval(date.replace(/\/Date\((.*?)\)\//gi, "new Date($1)"));
    var month = myDate.getMonth() + 1;
    if (month < 9) {
      month = "0" + month;
    } else {
      month = month;
    }
    return myDate.getDate() + "." + month + "." + myDate.getFullYear();
  } else {
    return date;
  }
}

function DateFromMSDateForForm(date) {
  if (date) {
    var myDate = eval(date.replace(/\/Date\((.*?)\)\//gi, "new Date($1)"));
    var month = myDate.getMonth() + 1;
    var day;

    if (month <= 9) {
      month = "0" + month;
    } else {
      month = month;
    }

    if (String(myDate.getDate()).length == 1) {
      day = "0" + myDate.getDate();
    } else {
      day = myDate.getDate();
    }

    return day+'.'+month+'.'+myDate.getFullYear();
  }
}

function Ribbon(data, container) {
  var html =
    '<nav class="navbar navbar-default" style="margin-bottom: 0;background-color: #e2e2e2;border: none;position:static;">' +
    '<div class="">' +
    '<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">' +
    '<ul class="nav navbar-nav panel-rib">';

  if (data.Data.Menu) {
    $.each(data.Data.Menu, function(k, v) {
      if (v.Items && v.Items.length > 0) {
        var items = "";

        $.each(v.Items, function(key, val) {
          items +=
            '<li><a class="ribbonBtn" data-action="' +
            val.Action +
            '" data-type="' +
            val.Type +
            '"><img style="height: 16px; width:auto;margin-right: 10px;" src="img/ui/' +
            val.Icon +
            '.png">' +
            val.Text +
            "</a></li>";
        });

        html +=
          '<li class="dropdown" data-toggle="tooltip" data-placement="bottom" title="' +
          v.ToolTip +
          '">' +
          '<a class="dropdown-toggle"  data-toggle="dropdown" style="text-align: center;" role="button" aria-haspopup="true" aria-expanded="false">' +
          '<img style="display: block;margin: 0 auto;"   src="img/ui/' +
          v.Icon +
          '.png">' +
          v.Text +
          '<div class="caret-btn"><span class="caret"></span></div></a>' +
          '<ul class="dropdown-menu">' +
          items +
          "</ul>" + //alt="' + v.Text + '" title="' + v.Text + '"
          "</li>";
      } else {
        //title="' + v.Text + '" alt="' + v.Text + '"
        if(v.Action == "Close"){
          closeAfterText = 'data-closeaftertext="'+openedObjed.Data.Object.DocumentName+'"';
        }else{
          closeAfterText = "";
        }
        html +=
          '<li data-toggle="tooltip" data-placement="bottom" title="' +
          v.ToolTip +
          '"><a class="ribbonBtn"  data-action="' +
          v.Action +
          '" data-type="' +
          v.Type +
          '" '+closeAfterText+'><img  style="display: block;margin: 0 auto;" src="img/ui/' +
          v.Icon +
          '.png">' +
          v.Text +
          "</a></li>";
      }
      html += '<li class="sep"></li>';
    });

    html += "</ul></div></div></nav>";

    $(container).html(html);
    $('[data-toggle="tooltip"]').tooltip();
    $("a.dropdown-toggle").on("click", function() {
      $(".tooltip").hide();
    });
    $(".dropdown").on("mouseenter", function() {
      if ($("li.dropdown").hasClass("open")) {
        $(".tooltip").hide();
      }
    });
  }
}

function CreateNotify(type, object_id) {
  $.ajax({
    url:
      "http://" + ip + "/EAkimat/Document/createNotify?id=" + object_id + "&type=" + type,
    xhrFields: {
      withCredentials: false
    },
    beforeSend: function(xhr) {
      xhr.setRequestHeader("content-type", "application/json");
      xhr.setRequestHeader("Token", token);
    },
    success: function(data) {
      $.ajax({
        url: "views/" + data.Data.FormId + ".html",
        cache: false,
        success: function(formdata) {
          $("body").append(formdata);

          var str = '<option value="">Выберите организацию</option>';
          $.each(data.Data.AdditionalObjects[0], function(k, v) {
            str += '<option value="' + v.Id + '">' + v.Name + "</option>";
          });

          $("#FromID").html(str);

          var id = $(".modal").attr("id");
          $("#weekends").modal("show");

          $("#weekends").on("hidden.bs.modal", function() {
            $("#weekends").remove();
          });

          $("body").on("click", ".save", function() {
            var valid = validate($("#CreateNotifyForm"));
            var object = data.Data.Object;

            if (valid == 0) {
              object.FromID = $("#FromID").val();
              object.FromText = $("#FromID option:selected").text();
              object.DocumentID = object_id;

              if ($("#Text").length) {
                object.Text = $("#Text").val();
              }

              if ($("#ReceiveDate").length) {
                object.ReceiveDate =
                  "/Date(" + Date.parse($("#ReceiveDate").val()) + ")/";
              }

              if ($("#RegNumber").length) {
                object.RegNumber = $("#RegNumber").val();
              }
              if ($("#RegDate").length) {
                object.RegDate =
                  "/Date(" + Date.parse($("#RegDate").val()) + ")/";
              }
              if ($("#Executer").length) {
                object.Executer = $("#Executer").val();
              }
              if ($("#ExecuterPhone").length) {
                object.ExecuterPhone = $("#ExecuterPhone").val();
              }
              if ($("#ExecText").length) {
                object.ExecText = $("#ExecText").val();
              }

              if ($("#ExecDate").length && $("#ExecDate").val() != "") {
                object.ExecDate =
                  "/Date(" + Date.parse($("#ExecDate").val()) + ")/";
              }

              if ($("#LimitDate").length && $("#LimitDate").val() != "") {
                object.LimitDate =
                  "/Date(" + Date.parse($("#LimitDate").val()) + ")/";
              }

              var result = {
                parent: object
              };

              $.ajax({
                url: "http://" + ip + "/EAkimat/Save/" + type,
                data: JSON.stringify(result),
                method: "POST",
                beforeSend: function(xhr) {
                  xhr.setRequestHeader("content-type", "application/json");
                  xhr.setRequestHeader("Token", token);
                },
                success: function(data) {
                  if (data.Result == "OK") {
                    $.notify(data.Message, {
                      type: "success"
                    });

                    $("#" + id).modal("hide");
                    $("#" + id).remove();
                    $(".forcontent").html("");
                    View(data);
                    Ribbon(data, ".ribbon");
                  } else {
                    $.notify(
                      "Ошибка записи данных: " + data.Message,
                      {
                        type: "danger"
                      }
                    );
                  }
                }
              });
            }
          });
        }
      });
    }
  });
}

function documentPages(data) {
  var pages = data.Data.Object.Pages;
  var tabs = "";
  var tab_content = "";

  $.each(pages, function(k, v) {
    var it = k + 2;

    tabs +=
      '<li role="presentation">' +
      '<a aria-expanded="true" href="#tab' +
      it +
      '" data-item="' +
      k +
      '" aria-controls="tab' +
      it +
      '" role="tab" data-form="' +
      v.FormId +
      '" data-toggle="tab">' +
      v.Name +
      "</a>" +
      '<span class="count tab' +
      it +
      '">' +
      v.Count +
      "</span>" +
      "</li>";

    tab_content +=
      '<div id="tab' +
      it +
      '" class="tab-pane" role="tabpanel">' +
      it +
      "</div>";
  });

  $("#tabs").append(tabs);
  $(".tab-content").append(tab_content);

  var forms = {
    "0202010": "Attachment",
    "0202040": "Adjustment",
    "0202050": "Signature",
    "0202060": "History",
    "0202070": "Link",
    "0202030": "Resolution",
    "0202020": "ActionPlan"
  };

  $("#tabs").on("click", "li a", function() {
    if ($(this).data("form") && $(this).data("form") != "") {
      var item = $(this).data("item");
      var form = $(this).data("form");
      var control = $(this).attr("aria-controls");

      if (pages[item]["load"] == true) {
      } else {
        $.ajax({
          url: "views/" + form + ".html",
          method: "get",
          async: false,
          cache: false,
          success: function(data2) {
            $(".tab-content #" + control).html(data2);
            pages[item]["load"] = true;

            $.ajax({
              url:
                "http://" +
                ip +
                "/EAkimat/Document." +
                forms[form] +
                ".List?id=" +
                data.Data.Object.Document.id,
              async: false,
              cache: false,
              xhrFields: {
                withCredentials: false
              },
              beforeSend: function(xhr) {
                xhr.setRequestHeader("content-type", "application/json");
                xhr.setRequestHeader("Token", token);
              },
              success: function(data) {
                var attachments = "";
                var input;

                if (form == "0202060" || form == "0202070") {
                  input = data.Data.Object.Data;
                } else if (form == "0202030") {
                  // function TableResol(node, container1) {
                  //   $('#'+container1).append('<table style="width: 100%; max-width: 100%;"  class="table1"><thead></thead><tbody></tbody></table>')
                  //     if(node[0].text == "Резолюция")
                  //         $('#'+container1+' table thead').append('<tr><th></th><th>№ П/П</th><th>№ ПОРУЧЕНИЯ</th><th>АВТОР</th><th>ЗАДАНИЕ/ПОРУЧЕНИЕ</th><th>ИСПОЛНИТЕЛЬ</th><th>СРОК</th><th>СТАТУС</th></tr>')
                  //     else
                  //         $('#'+container1+' table thead').append('<tr><th>ИСПОЛНИТЕЛЬ</th><th>ОРГАНИЗАЦИЯ</th><th>СТАТУС</th><th>ПРИЧИНА ВОЗВРАТА/ОТМЕНЫ</th><th>ДАТА ИСПОЛНЕНИЯ</th><th>ТЕКСТ ИСПОЛНЕНИЯ</th><th>СВОД</th></tr>')
                  //   $.each(node, function(index, elem) {

                  //     if (elem.text == "Резолюция")
                  //       var val = '<td width="33px"><span data-toogle="'+elem.id+'"><i class="jstree-icon resolOpen"></i><td>'+index+1+'</td>';

                  //     $.each(elem.Values, function(ind, el) {
                  //       if (el == 'True') {
                  //         el = '<input type="checkbox" checked disabled>'
                  //       }
                  //       else if (el == 'False')
                  //         el = '<input type="checkbox" disabled>'

                  //       val+='<td>'+el+'</td>';
                  //     });

                  //     $('#'+container1+' table tbody').append('<tr>'+val+'</tr>')

                  //     if (elem.Nodes.length > 0) {
                  //       $('#'+container1+' table tbody').append('<tr><td colspan="'+(Number(elem.Values.length)+1)+'" id="'+elem.id+'"></td></tr>')
                  //       TableResol(elem.Nodes, elem.id)
                  //     }

                  //   });
                  // }
                  // TableResol(data.Data.Object.data, 'resol');
                  var container1 = "resol";
                  $("#" + container1).append(
                    '<div class="section"><table style="width: 100%; max-width: 100%;"  class="table1"><thead></thead><tbody></tbody></table></div>'
                  );
                  $("#" + container1 + "  table > thead").append(
                    "<tr><th></th><th>№ П/П</th><th>№ ПОРУЧЕНИЯ</th><th>АВТОР</th><th>ЗАДАНИЕ/ПОРУЧЕНИЕ</th><th>ИСПОЛНИТЕЛЬ</th><th>СРОК</th><th>СТАТУС</th><th></th></tr>"
                  );
                  var numpp = 0;
                  $.each(data.Data.Object.data, function(index, el) {
                    if (el.parent == "#") {
                      numpp++;
                      var val =
                        '<td width="33px"><span data-toogle="' +
                        el.id +
                        '"><i class="jstree-icon"></i><td>' +
                        numpp +
                        "</td>";
                      $.each(el.Values, function(i, e) {
                        if (e == "True") {
                          e = '<input type="checkbox" checked disabled>';
                        } else if (e == "False") e = '<input type="checkbox" disabled>';

                        val += "<td>" + e + "</td>";
                      });
                      $("#" + container1 + "  table > tbody").append(
                        "<tr>" +
                          val +
                          '<td style="width:65px;"><div class="hide">' +
                          '<a class="actionBtnList" data-action="open" data-title="Резолюция" data-type="Documents|Orders|CitizenStatements.ResolutionTask" data-id="' +
                          el.id +
                          '"><img title="Открыть" alt="Открыть" src="img/ui/open_inactive.png" ></a>' +
                          "</div>" +
                          "</td></tr>"
                      );
                      $("#" + container1 + " > table > tbody").append(
                        '<tr><td style="display: none;" colspan="' +
                          (Number(el.Values.length) + 8) +
                          '" id="' +
                          el.id +
                          '"></td></tr>'
                      );
                    } else {
                      if ($("#" + el.parent + " > table > tbody").length) {
                        var val =
                          '<td width="33px"><span data-toogle="' +
                          el.id +
                          '"><i class="jstree-icon"></i></span></td>';
                        $.each(el.Values, function(i, e) {
                          if (e == "True") {
                            e = '<input type="checkbox" checked disabled>';
                          } else if (e == "False") e = '<input type="checkbox" disabled>';

                          val += "<td>" + e + "</td>";
                        });
                        $("#" + el.parent + " > table > tbody").append(
                          "<tr>" + val + "</tr>"
                        );
                        $("#" + el.parent + " > table > tbody").append(
                          '<tr><td style="display: none;" colspan="' +
                            (Number(el.Values.length) + 1) +
                            '" id="' +
                            el.id +
                            '"></td></tr>'
                        );
                      } else {
                        $("#" + el.parent).append(
                          '<table style="width: 100%; max-width: 100%;"  class="table1 table-bordered"><thead></thead><tbody></tbody></table>'
                        );
                        if (el.text == "Резолюция") {
                          numpp++;
                          $("#" + el.parent + " > table > thead").append(
                            "<tr><th></th><th>№ П/П</th><th>№ ПОРУЧЕНИЯ</th><th>АВТОР</th><th>ЗАДАНИЕ/ПОРУЧЕНИЕ</th><th>ИСПОЛНИТЕЛЬ</th><th>СРОК</th><th>СТАТУС</th><th></th></tr>"
                          );
                        } else {
                          $("#" + el.parent + " > table > thead").append(
                            "<tr><th></th><th>ИСПОЛНИТЕЛЬ</th><th>ОРГАНИЗАЦИЯ</th><th>СТАТУС</th><th>ПРИЧИНА ВОЗВРАТА/ОТМЕНЫ</th><th>ДАТА ИСПОЛНЕНИЯ</th><th>ТЕКСТ ИСПОЛНЕНИЯ</th><th>СВОД</th></tr>"
                          );
                        }
                        var val =
                          '<td width="33px"><span data-toogle="' +
                          el.id +
                          '"><i class="jstree-icon"></i></span></td>';

                        if (el.text == "Резолюция") {
                          val += "<td>" + numpp + "</td>";
                        }

                        $.each(el.Values, function(i, e) {
                          if (e == "True") {
                            e = '<input type="checkbox" checked disabled>';
                          } else if (e == "False") e = '<input type="checkbox" disabled>';

                          val += "<td>" + e + "</td>";
                        });
                        $("#" + el.parent + " > table > tbody").append(
                          "<tr>" + val + "</tr>"
                        );
                        $("#" + el.parent + " > table > tbody").append(
                          '<tr><td style="display: none;" colspan="' +
                            (Number(el.Values.length) + 2) +
                            '" id="' +
                            el.id +
                            '"></td></tr>'
                        );
                      }
                    }
                  });

                  $(".table1 tbody").on("mouseenter", "tr", function() {
                    $(this)
                      .find(".hide")
                      .addClass("show");
                    $(this)
                      .find(".show")
                      .removeClass("hide");
                  });

                  $(".table1 tbody").on("mouseleave", "tr", function() {
                    $(this)
                      .find(".show")
                      .addClass("hide");
                    $(this)
                      .find(".hide")
                      .removeClass("show");
                  });
                  $("#resol i").on("click", function() {
                    if ($(this).hasClass("resolOpen")) {
                      $(this).removeClass("resolOpen");
                      $(
                        "#" +
                          $(this)
                            .parent()
                            .data("toogle")
                      ).hide();
                    } else {
                      $(
                        "#" +
                          $(this)
                            .parent()
                            .data("toogle")
                      ).show();
                      $(this).addClass("resolOpen");
                    }
                  });
                } else if (form == "0202020") {
                  var container1 = "resol";
                  $("#" + container1).append(
                    '<table style="width: 100%; max-width: 100%;"  class="table1"><thead></thead><tbody></tbody></table>'
                  );
                  $("#" + container1 + " > table > thead").append(
                    "<tr><th></th><th>№ П/П</th><th>№ ПОРУЧЕНИЯ</th><th>АВТОР</th><th>ЗАДАНИЕ/ПОРУЧЕНИЕ</th><th>ИСПОЛНИТЕЛЬ</th><th>СРОК</th><th>СТАТУС</th><th></th><th></th></tr>"
                  );
                  var numpp = 0;
                  $.each(data.Data.Object.data, function(index, el) {
                    if (el.parent == "#") {
                      numpp++;
                      var val =
                        '<td width="33px"><span data-toogle="' +
                        el.id +
                        '"><i class="jstree-icon"></i><td>' +
                        numpp +
                        "</td>";
                      $.each(el.Values, function(i, e) {
                        if (e == "True") {
                          e = '<input type="checkbox" checked disabled>';
                        } else if (e == "False") e = '<input type="checkbox" disabled>';

                        val += "<td>" + e + "</td>";
                      });
                      $("#" + container1 + " > table > tbody").append(
                        "<tr>" +
                          val +
                          '<td style="width:65px;"><div class="hide">' +
                          '<a class="actionBtnList" data-action="open" data-title="Резолюция" data-type="Documents|Orders.ActionPlanItem" data-id="' +
                          el.id +
                          '"><img title="Открыть" alt="Открыть" src="img/ui/open_inactive.png" ></a>' +
                          "</div>" +
                          "</td></tr>"
                      );
                      $("#" + container1 + " > table > tbody").append(
                        '<tr><td style="display: none;" colspan="' +
                          (Number(el.Values.length) + 8) +
                          '" id="' +
                          el.id +
                          '"></td></tr>'
                      );
                    } else {
                      if ($("#" + el.parent + " > table > tbody").length) {
                        var val =
                          '<td width="33px"><span data-toogle="' +
                          el.id +
                          '"><i class="jstree-icon"></i></span></td>';
                        $.each(el.Values, function(i, e) {
                          if (e == "True") {
                            e = '<input type="checkbox" checked disabled>';
                          } else if (e == "False") e = '<input type="checkbox" disabled>';

                          val += "<td>" + e + "</td>";
                        });
                        $("#" + el.parent + " > table > tbody").append(
                          "<tr>" + val + "</tr>"
                        );
                        $("#" + el.parent + " > table > tbody").append(
                          '<tr><td style="display: none;" colspan="' +
                            (Number(el.Values.length) + 1) +
                            '" id="' +
                            el.id +
                            '"></td></tr>'
                        );
                      } else {
                        $("#" + el.parent).append(
                          '<table style="width: 100%; max-width: 100%;"  class="table1 table-bordered"><thead></thead><tbody></tbody></table>'
                        );
                        if (el.text == "Резолюция") {
                          numpp++;
                          $("#" + el.parent + " > table > thead").append(
                            "<tr><th></th><th>№ П/П</th><th>№ ПОРУЧЕНИЯ</th><th>АВТОР</th><th>ЗАДАНИЕ/ПОРУЧЕНИЕ</th><th>ИСПОЛНИТЕЛЬ</th><th>СРОК</th><th>СТАТУС</th><th></th></tr>"
                          );
                        } else {
                          $("#" + el.parent + " > table > thead").append(
                            "<tr><th></th><th>ИСПОЛНИТЕЛЬ</th><th>ОРГАНИЗАЦИЯ</th><th>СТАТУС</th><th>ПРИЧИНА ВОЗВРАТА/ОТМЕНЫ</th><th>ДАТА ИСПОЛНЕНИЯ</th><th>ТЕКСТ ИСПОЛНЕНИЯ</th><th>СВОД</th><th>КТУ</th></tr>"
                          );
                        }
                        var val =
                          '<td width="33px"><span data-toogle="' +
                          el.id +
                          '"><i class="jstree-icon"></i></span></td>';

                        if (el.text == "Резолюция") {
                          val += "<td>" + numpp + "</td>";
                        }

                        $.each(el.Values, function(i, e) {
                          if (e == "True") {
                            e = '<input type="checkbox" checked disabled>';
                          } else if (e == "False") e = '<input type="checkbox" disabled>';

                          val += "<td>" + e + "</td>";
                        });
                        $("#" + el.parent + " > table > tbody").append(
                          "<tr>" + val + "</tr>"
                        );
                        $("#" + el.parent + " > table > tbody").append(
                          '<tr><td style="display: none;" colspan="' +
                            (Number(el.Values.length) + 2) +
                            '" id="' +
                            el.id +
                            '"></td></tr>'
                        );
                      }
                    }
                  });

                  $(".table1 tbody").on("mouseenter", "tr", function() {
                    $(this)
                      .find(".hide")
                      .addClass("show");
                    $(this)
                      .find(".show")
                      .removeClass("hide");
                  });

                  $(".table1 tbody").on("mouseleave", "tr", function() {
                    $(this)
                      .find(".show")
                      .addClass("hide");
                    $(this)
                      .find(".hide")
                      .removeClass("show");
                  });
                  $("#resol i").on("click", function() {
                    if ($(this).hasClass("resolOpen")) {
                      $(this).removeClass("resolOpen");
                      $(
                        "#" +
                          $(this)
                            .parent()
                            .data("toogle")
                      ).hide();
                    } else {
                      $(
                        "#" +
                          $(this)
                            .parent()
                            .data("toogle")
                      ).show();
                      $(this).addClass("resolOpen");
                    }
                  });
                } else if (form == "0202040") {
                  var statusCode = [
                    "",
                    "в ожидании",
                    "согласовано",
                    "не согласовано",
                    "согласовано с замечаниями",
                    "отменено"
                  ];
                  input = data.Data.Object;
                  $.each(input, function(ind, el) {
                    $("#AdjustmentList").append(
                      '<option data-index="' +
                        ind +
                        '" value="' +
                        el.id +
                        '">' +
                        el.Description +
                        "</option>"
                    );
                  });
                  function selectAjsInfo(ind) {
                    var items = "";
                    $.each(input[ind].Records, function(index, element) {
                      items += "<tr>";
                      items += "<td>" + element.SignerName + "</td>";
                      items += "<td>" + element.JobTitle + "</td>";
                      items += "<td>" + (element.DelRec ? "удален" : statusCode[element.StatusCode]) + "</td>";
                      items += "<td>" + (element.SignDate ? DateFromMSDate(element.SignDate) : "") + "</td>";
                      items += "<td>" + (element.Comment ? element.Comment : "") + "</td>";
                      items += "</tr>";
                    });
                    $("#adjustmentTable tbody").html(items);
                    $("#AdjInitiatorName").text(input[ind].InitiatorName);
                    $("#AdjCreateDate").text(
                      input[ind].CreateDate
                        ? DateFromMSDate(input[ind].CreateDate)
                        : "не назначено"
                    );
                    $("#AdjStatusCode").text(
                      input[ind].DelRec
                        ? "удален"
                        : statusCode[input[ind].StatusCode]
                    );
                    $("#AdjParallel").text(
                      input[ind].Parallel ? "параллельно" : "последовательно"
                    );
                  }
                  $("#AdjustmentList").on("change", function() {
                    ind = $("#AdjustmentList option:selected").data("index");
                    selectAjsInfo(ind, input);
                  });
                  selectAjsInfo(0, input);
                  $("#AdjustmentList :first").attr("selected", "selected");

                  // var list = [];
                  // $.each(input, function (index, el) {
                  //   $.each(el.Records, function (i, e) {
                  //     e["Consolidation"] = el.Description;
                  //     e.SignDate = DateFromMSDate(e.SignDate)
                  //     if (e.DelRec == true)
                  //       e.StatusCode = "удален";
                  //     else {
                  //       var status = e.StatusCode;
                  //       switch (status) {
                  //         case 0:
                  //           status = "";
                  //           break;
                  //         case 1:
                  //           status = "в ожидании";
                  //           break;
                  //         case 2:
                  //           status = "согласовано";
                  //           break;
                  //         case 3:
                  //           status = "не согласовано";
                  //           break;
                  //         case 4:
                  //           status = "согласовано с замечаниями";
                  //           break;
                  //         case 5:
                  //           status = "отменено";
                  //           break;
                  //       }
                  //       e.StatusCode = status;
                  //     }
                  //     list.push(e);
                  //   });
                  // });
                } else {
                  input = data.Data.Object.Data;
                }

                if (form == "0202010") {
                  var downloadHref = "";
                  if (open_item.Data.FormId == "0501022") {
                    $.each(input, function(k, v) {
                      var ind = k;
                      var tds = "";
                      var tp = false;
                      $.each(v, function(k, val) {
                        if (open_item.Data.FormId == "0501022")
                          if (tp == false) {
                            tds +=
                              '<td><select class="attach-type" id="attach-type' +
                              ind +
                              '">' +
                              attachTypeGen(v[4]) +
                              "</select></td>";
                            tp = true;
                          }
                        if (k != 0 && k != 4 && k != 1) {
                          var str = "";

                          if (val != null) {
                            str = val;
                          }

                          tds += "<td>" + str + "</td>";
                        } else if (k == 1) {
                          if (open_item.Data.FormId == "0501022") {
                            tds +=
                              '<td><a href="http://' +
                              ip +
                              "/EAkimat/Attachment?id=" +
                              v[0] +
                              '">' +
                              val +
                              "</a></td>";
                            downloadHref =
                              "http://" +
                              ip +
                              "/EAkimat/Attachment?id=" +
                              v[0];
                          } else {
                            if (val != null) {
                              str = val;
                            }

                            tds += "<td>" + str + "</td>";
                          }
                        }
                      });
                      if (open_item.Data.FormId == "0501022") {
                        tds +=
                          '<td class="action-cell"><div class="hide"><a class="actionBtnList" data-action="delete" data-type="Document" data-id="' +
                          v[0] +
                          '"><img title="Удалить" alt="Удалить" src="img/ui/delete_inactive.png"></a>' +
                          "<a href=" +
                          downloadHref +
                          ' style="margin-left: 20px" data-action="download" data-type="Document" data-id="' +
                          v[0] +
                          '"><img title="Скачать" alt="Скачать" src="img/ui/save_inactive.png"></a></div></td>';
                      }
                      attachments +=
                        '<tr data-id="' + v[0] + '">' + tds + "</tr>";
                    });
                  } else if (open_item.Data.FormId == "0501021") {
                    $.each(input, function(k, v) {
                      var tds = "";
                      var tp = false;
                      $.each(v, function(k, val) {
                        if (tp == false) {
                          tds += "<td>" + v[4] + "</td>";
                          tp = true;
                        }
                        if (k != 0 && k != 1 && k != 4) {
                          var str = "";

                          if (val != null) {
                            str = val;
                          }

                          tds += "<td>" + str + "</td>";
                        } else if (k == 1) {
                          var str = "";

                          if (val != null) {
                            str = val;
                          }

                          tds +=
                            '<td><a href="http://' +
                            ip +
                            "/EAkimat/Attachment?id=" +
                            v[0] +
                            '">' +
                            str +
                            "</a></td>";
                          downloadHref =
                            "http://" +
                            ip +
                            "/EAkimat/Attachment?id=" +
                            v[0];
                        }
                      });

                      tds +=
                        '<td class="action-cell"><div class="hide"><a class="actionBtnList" data-action="delete" data-type="Document" data-id="' +
                        v[0] +
                        '"><img title="Удалить" alt="Удалить" src="img/ui/delete_inactive.png"></a>' +
                        "<a href=" +
                        downloadHref +
                        ' style="margin-left: 20px" data-action="download" data-type="Document" data-id="' +
                        v[0] +
                        '"><img title="Скачать" alt="Скачать" src="img/ui/save_inactive.png"></a></div></td>';

                      attachments += "<tr>" + tds + "</tr>";
                    });
                  } else {
                    $("#" + control + " .table thead tr")
                      .children()
                      .first()
                      .remove();
                    $.each(input, function(k, v) {
                      var tds = "";

                      $.each(v, function(k, val) {
                        if (k != 0 && k != 1) {
                          var str = "";

                          if (val != null) {
                            str = val;
                          }

                          tds += "<td>" + str + "</td>";
                        } else if (k == 1) {
                          var str = "";

                          if (val != null) {
                            str = val;
                          }

                          tds +=
                            '<td><a href="http://' +
                            ip +
                            "/EAkimat/Attachment?id=" +
                            v[0] +
                            '">' +
                            str +
                            "</a></td>";
                          downloadHref =
                            "http://" +
                            ip +
                            "/EAkimat/Attachment?id=" +
                            v[0];
                        }
                      });

                      tds +=
                        '<td class="action-cell"><div class="hide"><a class="actionBtnList" data-action="delete" data-type="Document" data-id="' +
                        v[0] +
                        '"><img title="Удалить" alt="Удалить" src="img/ui/delete_inactive.png"></a>' +
                        "<a href=" +
                        downloadHref +
                        ' style="margin-left: 20px" data-action="download" data-type="Document" data-id="' +
                        v[0] +
                        '"><img title="Скачать" alt="Скачать" src="img/ui/save_inactive.png"></a></div></td>';

                      attachments += "<tr>" + tds + "</tr>";
                    });
                  }
                } else if (form == "0202060") {
                  var groups = [];

                  $.each(input, function(index, el) {
                    // $('#history tbody').append('<div>Группа '+index+'</div>')
                    if (el[1] != "") {
                      var groupEx = -1;

                      $.each(groups, function(ind, elem) {
                        if (el[1] == elem) {
                          groupEx = ind;
                        }
                      });
                      if (groupEx == -1) {
                        groups.push(el[1]);
                        $("#history tbody").prepend(
                          '<tr colspan="4" class="rowGr" id="' +
                            (groups.length - 1) +
                            '"><td colspan="4">' +
                            '<i style="width: 33px; display: inline-block; padding: 10px!important;" class="jstree-icon"></i><span>' +
                            el[1] +
                            "</span></td></tr>"
                        );
                        $("#history tbody #" + (groups.length - 1)).after(
                          '<tr class="group' +
                            (groups.length - 1) +
                            '">' +
                            /*"<td>" +
                            el[1] +
                            "</td>" +*/
                            "<td>" +
                            el[2] +
                            "</td>" +
                            "<td>" +
                            el[3] +
                            "</td>" +
                            "<td>" +
                            el[4] +
                            "</td>"
                        );
                        $(".group" + (groups.length - 1)).hide();
                      } else {
                        $("#history tbody #" + groupEx).after(
                          '<tr class="group' +
                            groupEx +
                            '">' +
                            /*"<td>" +
                            el[1] +
                            "</td>" +*/
                            "<td>" +
                            el[2] +
                            "</td>" +
                            "<td>" +
                            el[3] +
                            "</td>" +
                            "<td>" +
                            el[4] +
                            "</td>"
                        );
                        $(".group" + groupEx).hide();
                      }
                    } else {
                      $("#history tbody").append(
                        "<tr>" +
                          /*"<td>" +
                          el[1] +
                          "</td>" +*/
                          "<td>" +
                          el[2] +
                          "</td>" +
                          "<td>" +
                          el[3] +
                          "</td>" +
                          "<td>" +
                          el[4] +
                          "</td>"
                      );
                    }
                  });
                  $(document)
                    .off("click", ".rowGr")
                    .on("click", ".rowGr", function() {
                      // $('group'+$(this).attr('id')).toogle();
                      if ($(this).hasClass("rowGrOpen")) {
                        $(this).removeClass("rowGrOpen");
                        $(".group" + $(this).attr("id")).hide();
                      } else {
                        $(".group" + $(this).attr("id")).show();
                        $(this).addClass("rowGrOpen");
                      }
                    });
                } else if (form == "0202030") {
                } else if (form == "0202040") {
                } else if (form == "0202050") {
                    tds = "";
                    $.each(input, function(index, elem) {
                      tds += '<tr data-id="' + elem[0] + '">';
                      tds += "<td>" + elem[1] + "</td>";
                      tds += "<td>" + elem[2] + "</td>";
                      tds +=
                        "<td>" +
                        elem[3].substring(0, elem[3].indexOf(" ")) +
                        "</td>";
                      tds += "<td>" + elem[4] + "</td>";
                      tds += "<td>" + elem[5] + "</td>";
                      tds += "</tr>";
                    });
                    $(".signature tbody").html(tds);
                } else if (form == "0202070") {
                  $.each(input, function(k, v) {
                    var tds = "";

                    $.each(v, function(k, val) {
                      if (k != 0 && k != 7) {
                        var str = "";

                        if (val != null) {
                          if (k == 6) {
                            tds +=
                              "<td>" +
                              '<a class="actionBtnList" data-action="open" data-type="Documents.Document" data-id="' +
                              val +
                              '">' +
                              '<img title="Открыть" alt="Открыть" src="img/ui/open_inactive.png">' +
                              "</a>";
                            if (v[7] == "True") {
                              tds +=
                                '<a class="actionBtnList" style="padding-left: 16px;" data-action="deleteLink" data-type="Documents.Document" data-id="' +
                                v[0] +
                                '"><img title="Удалить" alt="Удалить" src="img/ui/delete_inactive.png"></a>';
                            }
                            tds += "</td>";
                            str = "";
                          } else {
                            str = val;
                          }
                        }

                        tds += "<td>" + str + "</td>";
                      }
                    });

                    attachments += "<tr>" + tds + "</tr>";
                  });

                  $("#link tbody").html(attachments);
                } else {
                  $.each(input, function(k, v) {
                    var tds = "";

                    $.each(v, function(k, val) {
                      if (k != 0) {
                        var str = "";

                        if (val != null) {
                          if (k == v.length - 1) {
                            tds +=
                              "<td>" +
                              '<a class="actionBtnList" data-action="open" data-type="Documents.Document" data-id="' +
                              val +
                              '">' +
                              '<img title="Открыть" alt="Открыть" src="img/ui/open_inactive.png">' +
                              "</a></td>";
                            str = "";
                          } else {
                            str = val;
                          }
                        }

                        tds += "<td>" + str + "</td>";
                      }
                    });

                    attachments += "<tr>" + tds + "</tr>";
                  });

                  $(".table tbody").html(attachments);
                }
                if (form == "0202010") {
                  $("#" + control + " .table tbody").html(attachments);
                }

                $(".table tbody").on("mouseenter", "tr", function() {
                  $(this)
                    .find(".hide")
                    .addClass("show");
                  $(this)
                    .find(".show")
                    .removeClass("hide");
                });

                $(".table tbody").on("mouseleave", "tr", function() {
                  $(this)
                    .find(".show")
                    .addClass("hide");
                  $(this)
                    .find(".hide")
                    .removeClass("show");
                });

                $(".table tbody").on("click", ".actionBtnList", function() {
                  var thisElement = $(this);
                  var thisTr = $(this)
                    .parent()
                    .parent()
                    .parent();
                  var action = $(this).data("action");
                  var id = $(this).data("id");

                  if (action == "open") {
                    dialog.confirm({
                      title: "Предупреждение",
                      message:
                        "Вы действительно хотите закрыть текущий документ и перейти на связанный?",
                      cancel: "Нет",
                      button: "Да",
                      required: true,
                      callback: function(value) {
                        if (value == true) {
                          $.ajax({
                            url:
                              "http://" +
                              ip +
                              "/EAkimat/" +
                              action +
                              "/Chancellery|Documents.Document/?id=" +
                              id,
                            cache: false,
                            beforeSend: function(xhr) {
                              xhr.setRequestHeader(
                                "content-type",
                                "application/json"
                              );
                              xhr.setRequestHeader("Token", token);
                            },
                            success: function(data) {
                              if (data.Result == "OK") {
                                $(".forcontent").html("");
                                openedObjed = data;
                                View(data);
                              } else {
                                $.notify(data.Message, {
                                  type: "danger"
                                });
                              }
                            }
                          });
                        }
                      }
                    });
                  } else if (action == "deleteLink") {
                    dialog.confirm({
                      title: "Предупреждение",
                      message: "Вы действительно хотите удалить связь?",
                      cancel: "Нет",
                      button: "Да",
                      required: true,
                      callback: function(value) {
                        if (value == true) {
                          $.ajax({
                            url:
                              "http://" +
                              ip +
                              "/EAkimat/Document/DeleteLink",
                            cache: false,
                            method: "POST",
                            data: JSON.stringify({
                              id: open_item.Data.Object.Document.id,
                              linkId: id
                            }),
                            beforeSend: function(xhr) {
                              xhr.setRequestHeader(
                                "content-type",
                                "application/json"
                              );
                              xhr.setRequestHeader("Token", token);
                            },
                            success: function(data) {
                              if (data.Result == "OK") {
                                $.notify(data.Message, {
                                  type: "success"
                                });
                                $(thisElement)
                                  .parents("tr")
                                  .remove();
                              } else {
                                $.notify(data.Message, {
                                  type: "danger"
                                });
                              }
                            }
                          });
                        }
                      }
                    });
                  } else {
                    var type = $(this).data("type");
                    var cancelAdjustment = false;
                    var isAdj = false;
                    var adj = open_item.Data.Object.IsAdjusted;

                    if (open_item.Data.Object.Document.is_new_record == false) {
                      if (adj == true) {
                        // isAdj = confirm('Изменение списка вложений приведет к отмене всех согласований после сохранения документа! Продолжить?');
                        // if (isAdj == true) {
                        dialog.confirm({
                          title: "Предупреждение",
                          message:
                            "Изменение списка вложений приведет к отмене всех согласований после сохранения документа! Продолжить?",
                          cancel: "Нет",
                          button: "Да",
                          required: true,
                          callback: function(value) {
                            if (value == true) {
                              cancelAdjustment = true;

                              deleteAdj(action, cancelAdjustment);
                            }
                          }
                        });

                        // }
                      } else {
                        dialog.confirm({
                          title: "Предупреждение",
                          message: "Удалить вложение?",
                          cancel: "Нет",
                          button: "Да",
                          required: true,
                          callback: function(value) {
                            if (value == true) {
                              deleteAdj(action);
                            }
                          }
                        });
                      }
                    } else {
                      attach = openedObjed.Data.Object.Document.Attachments;
                      if (attach.length > 1) {
                        dialog.confirm({
                          title: "Предупреждение",
                          message: "Удалить вложение?",
                          cancel: "Нет",
                          button: "Да",
                          required: true,
                          callback: function(value) {
                            if (value == true) {
                              $.each(attach, function(ind, el) {
                                if (el.id == id) {
                                  attach.splice(ind, 1);
                                  $(thisTr).remove();
                                  $(".nav-tabs li.active .count").text(
                                    Number(
                                      $(".nav-tabs li.active .count").text()
                                    ) - 1
                                  );
                                }
                              });
                            }
                          }
                        });
                      } else {
                        $.notify(
                          "В документе должно быть хотя бы одно вложение!",
                          { type: "danger" }
                        );
                      }
                    }

                    function deleteAdj(action, cancelAdjustment) {
                      if (typeof cancelAdjustment == "undefined") {
                        cancelAdjustment = false;
                      }
                      $.ajax({
                        url:
                          "http://" +
                          ip +
                          "/EAkimat/" +
                          action +
                          "/Attachment/?id=" +
                          id +
                          "&cancelAdjustment=" +
                          cancelAdjustment,
                        beforeSend: function(xhr) {
                          xhr.setRequestHeader(
                            "content-type",
                            "application/json"
                          );
                          xhr.setRequestHeader("Token", token);
                        },
                        success: function(res) {
                          if (action == "delete") {
                            if (res.Result == "OK") {
                              $.notify(res.Message, {
                                type: "success"
                              });
                              $.ajax({
                                url:
                                  "http://" +
                                  ip +
                                  "/EAkimat/Document.Attachment.List?id=" +
                                  open_item.Data.Object.Document.id,
                                async: false,
                                cache: false,
                                xhrFields: {
                                  withCredentials: false
                                },
                                beforeSend: function(xhr) {
                                  xhr.setRequestHeader(
                                    "content-type",
                                    "application/json"
                                  );
                                  xhr.setRequestHeader("Token", token);
                                },
                                success: function(data) {
                                  var attachments = "";
                                  var input;

                                  input = data.Data.Object.Data;

                                  var downloadHref = "";

                                  attachments = setAttachments(
                                    input,
                                    "tab2",
                                    "update"
                                  );

                                  $("#tab2 .table tbody").html(attachments);
                                  $(".tab2").html(input.length);
                                }
                              });
                            } else {
                              $.notify(
                                "Ошибка удаления данных: " +
                                  res.Message,
                                {
                                  type: "danger"
                                }
                              );
                            }
                          }
                        }
                      });
                    }
                  }
                });
              }
            });
          }
        });
      }
    }
  });
}

var open_item;
var open_item2;

// Генерация таблицы вложений
// input - массив вложений
// control - вкладка для вложений, для таблиц которые отличаются от стандарта (указывается без "#" и ".")
// type - update обновление без изменения структуры стаблицы
function setAttachments(input, control, type) {
  var attachments = "";
  var downloadHref = "";
  if (open_item.Data.FormId == "0501021") {
    $.each(input, function(k, v) {
      var tds = "";
      var tp = false;
      $.each(v, function(k, val) {
        if (tp == false) {
          tds += "<td>" + v[4] + "</td>";
          tp = true;
        }
        if (k != 0 && k != 1 && k != 4) {
          var str = "";

          if (val != null) {
            str = val;
          }

          tds += "<td>" + str + "</td>";
        } else if (k == 1) {
          var str = "";

          if (val != null) {
            str = val;
          }

          tds +=
            '<td><a href="http://' +
            ip +
            "/EAkimat/Attachment?id=" +
            v[0] +
            '">' +
            str +
            "</a></td>";
          downloadHref = "http://" + ip + "/EAkimat/Attachment?id=" + v[0];
        }
      });

      tds +=
        '<td class="action-cell"><div class="hide"><a class="actionBtnList" data-action="delete" data-type="Document" data-id="' +
        v[0] +
        '"><img title="Удалить" alt="Удалить" src="img/ui/delete_inactive.png"></a>' +
        "<a href=" +
        downloadHref +
        ' style="margin-left: 20px" data-action="download" data-type="Document" data-id="' +
        v[0] +
        '"><img title="Скачать" alt="Скачать" src="img/ui/save_inactive.png"></a></div></td>';

      attachments += "<tr>" + tds + "</tr>";
    });
  } else {
    if (type != "update")
      $("#" + control + " .table thead tr")
        .children()
        .first()
        .remove();
    $.each(input, function(k, v) {
      var tds = "";

      $.each(v, function(k, val) {
        if (k != 0 && k != 1) {
          var str = "";

          if (val != null) {
            str = val;
          }

          tds += "<td>" + str + "</td>";
        } else if (k == 1) {
          var str = "";

          if (val != null) {
            str = val;
          }

          tds +=
            '<td><a href="http://' +
            ip +
            "/EAkimat/Attachment?id=" +
            v[0] +
            '">' +
            str +
            "</a></td>";
          downloadHref = "http://" + ip + "/EAkimat/Attachment?id=" + v[0];
        }
      });

      tds +=
        '<td class="action-cell"><div class="hide"><a class="actionBtnList" data-action="delete" data-type="Document" data-id="' +
        v[0] +
        '"><img title="Удалить" alt="Удалить" src="img/ui/delete_inactive.png"></a>' +
        "<a href=" +
        downloadHref +
        ' style="margin-left: 20px" data-action="download" data-type="Document" data-id="' +
        v[0] +
        '"><img title="Скачать" alt="Скачать" src="img/ui/save_inactive.png"></a></div></td>';

      attachments += "<tr>" + tds + "</tr>";
    });
  }

  return attachments;
}

function Count(count) {
  $(".count").html(count);
}

function GetWorkplaceList(i, v, val) {
  var token = $.session.get("token");

  $.ajax({
    url: "http://" + ip + "/EAkimat/Workplace.List",
    async: false,
    method: "post",
    data: JSON.stringify({
      onlyBusy: true,
      ids: null,
      filter: ""
    }),
    beforeSend: function(xhr) {
      xhr.setRequestHeader("content-type", "application/json");
      xhr.setRequestHeader("Token", token);
    },
    success: function(data) {
      var have = 0;
      var field = i;
      $(data.data).each(function(ind, value) {
        $("#" + field).append(
          "<option data-workplaceid=" +
            value[0] +
            " value=" +
            value[2] +
            ">" +
            value[1] +
            "</tr>"
        );
        if (value[1] == val) {
          have++;
          $("#" + field)
            .last()
            .attr("selected", "selected");
        }
      });
      if (have == 0) {
        switch (field) {
          case "ExecuterName":
            if (v.ExecuterId && v.ExecuterName)
              $("#" + field).append(
                "<option value=" +
                  v.ExecuterId +
                  " selected>" +
                  v.ExecuterName +
                  "</tr>"
              );
            else $("#" + field).val("");
            break;
          case "SignerName":
            if (v.SignerEmployeeId && v.SignerName)
              $("#" + field).append(
                "<option value=" +
                  v.SignerEmployeeId +
                  " selected>" +
                  v.SignerName +
                  "</tr>"
              );
            else $("#" + field).val("");

            break;
          case "Signer":
            if (v.SignerEmployeeId && v.Signer)
              $("#" + field).append(
                "<option data-workplaceid=" +
                  v.SignerWorkplaceId +
                  " value=" +
                  v.SignerEmployeeId +
                  " selected>" +
                  val.Owner.FirstName +
                  " " +
                  val.Owner.MiddleName +
                  " " +
                  val.Owner.LastName +
                  " (" +
                  val.Parent.JobTitle.Name +
                  ")</option>"
              );
            else $("#" + field).val("");
            break;
          case "Responsible":
            if (v.ResponsibleEmployeeId && v.Responsible)
              $("#" + field).append(
                "<option data-workplaceid=" +
                  v.ResponsibleWorkPlaceId +
                  " value=" +
                  v.ResponsibleEmployeeId +
                  " selected>" +
                  val.Owner.FirstName +
                  " " +
                  val.Owner.MiddleName +
                  " " +
                  val.Owner.LastName +
                  " (" +
                  val.Parent.JobTitle.Name +
                  ")</option>"
              );
            else $("#" + field).val("");
            break;
        }
      }
      $("#" + field).chosen();
    }
  });
}

function saveSuccess(obj) {
  if (obj) {
    (obj.is_new_record = false), (obj.Card.is_new_record = false);
    function eachRec(data) {
      $.each(data, function(index, elem) {
        elem.is_new_record = false;
      });
    }
    eachRec(obj.Attachments);
    eachRec(obj.Signatures);
    eachRec(obj.Links);
    if (obj.Questions) {
      eachRec(obj.Questions);
    }
    if (obj.ActionPlan) {
      eachRec(obj.ActionPlan);
    }
  }
}

function TemplateParse(data) {
  var str = data;
  var target = "[";
  var target2 = "]";
  var obj = [];

  var pos = -1;

  while ((pos = str.indexOf(target)) != -1) {
    if (pos == 0) {
      var start = str.indexOf(target, 0);
      var end = str.indexOf(target2, 0);

      obj.push(str.slice(start, end + 1));
      str = str.substr(end + 1);

      pos = -1;
    } else {
      var start = str.indexOf(target, 0);
      var end = str.indexOf(target2, 0);

      obj.push(str.slice(0, start));
      obj.push(str.slice(start, end + 1));
      str = str.substr(end + 1);
    }
  }

  function showRes() {
    $("#result").val($("#Template-view").text());
  }

  var opts = "";

  $(obj).each(function(k, v) {
    opts += "<option>" + v + "</option>";
  });

  $("#Template-view").html(opts);

  showRes();
  $("#Template-view").off("click", "option");
  $("#Template-view").on("click", "option", function() {
    $(this).remove();
    showRes();
  });
  $("#Template-select").off("click", "option");
  $("#Template-select").on("click", "option", function() {
    $(this)
      .clone()
      .appendTo("#Template-view");
    showRes();
  });
  $("#add-string").off("click");
  $("#add-string").on("click", function() {
    if ($("#string").val() != "") {
      $("#Template-view").append("<option>" + $("#string").val() + "</option>");
      $("#string").val("");
      showRes();
    }
  });
  $(".save-template").off("click");
  $(".save-template").on("click", function() {
    $("#Template").val($("#result").val());
  });
}

function AccessList() {
  var token = $.session.get("token");

  $.ajax({
    url: " http://" + ip + "/EAkimat/Workplace.List",
    async: false,
    method: "post",
    data: JSON.stringify({
      onlyBusy: "true"
    }),
    beforeSend: function(xhr) {
      xhr.setRequestHeader("content-type", "application/json");
      xhr.setRequestHeader("Token", token);
    },
    success: function(data) {
      var options = "";

      $.each(data.data, function(key, val) {
        options += '<option value="' + val[0] + '">' + val[1] + "</option>";
      });

      $($("#access-select")).html(options);
    }
  });
  $("#access-view").on("click", "option", function() {
    $(this).remove();
    $(this).appendTo("#access-select");
  });

  $("#access-select").on("click", "option", function() {
    $(this).remove();
    $(this).appendTo("#access-view");
  });

  $(".save-access").on("click", function() {
    var access = [];
    var tr = "";

    $("#access-view option").each(function(i, v) {
      access.push($(v).val());
      tr += "<tr><td>" + $(v).text() + "</td></tr>";
    });

    $("#AccessList").val(JSON.stringify(access));
    $("#AccessListTable").html(tr);
    $("#RecipientListTable").html(tr);
    $("#Recipients").val(JSON.stringify(access));
  });
}

function DocumentationList() {
  var token = $.session.get("token");

  $.ajax({
    url: " http://" + ip + "/EAkimat/References?reftype=1305",
    async: false,
    beforeSend: function(xhr) {
      xhr.setRequestHeader("content-type", "application/json");
      xhr.setRequestHeader("Token", token);
    },
    success: function(data) {
      var options = "";
      $.each(data, function(key, val) {
        options += '<option value="' + val.id + '">' + val.Value + "</option>";
      });

      $("#documentation-select").html(options);
    }
  });
  $("#documentation-view").on("click", "option", function() {
    $(this).remove();
    $(this).appendTo("#documentation-select");
  });

  $("#documentation-select").on("click", "option", function() {
    $(this).remove();
    $(this).appendTo("#documentation-view");
  });

  $(".save-documentation").on("click", function() {
    var access = [];
    var tr = "";

    $("#documentation-view option").each(function(i, v) {
      access.push($(v).val());
      tr += "<tr><td>" + $(v).text() + "</td></tr>";
    });

    $("#DocumentationList").val(JSON.stringify(access));
    $("#DocumentationListTable").html(tr);
  });
}

function RecipientsList(contType, onlyOut) {
  if (typeof contType == "undefined") {
    contType = 0;
  }
  if (typeof onlyOut == "undefined") {
    onlyOut = false;
  }
  var token = $.session.get("token");

  $.ajax({
    url: " http://" + ip + "/EAkimat/Chancellery.Contractor.Select",
    async: false,
    method: "post",
    data: JSON.stringify({
      param: {
        ContractorType: contType,
        OnlyOuter: onlyOut
      }
    }),
    beforeSend: function(xhr) {
      xhr.setRequestHeader("content-type", "application/json");
      xhr.setRequestHeader("Token", token);
    },
    success: function(data) {
      var options = "";
      if (data.Result == "ERROR") {
        $.notify(data.Message, { type: "danger" });
      } else {
        $.each(data.Data.Object.Data, function(key, val) {
          options +=
            '<option data-type="' +
            val[3] +
            '" value="' +
            val[0] +
            '">' +
            val[1] +
            "</option>";
        });
        $("#recipient-select").html(options);
      }
    }
  });

  $("#recipient-view").on("click", "option", function() {
    $(this).remove();
    $(this).appendTo("#recipient-select");
  });

  $("#recipient-select").on("click", "option", function() {
    $(this).remove();
    $(this).appendTo("#recipient-view");
  });

  $(".save-rec").on("click", function() {
    var access = [];
    var recipient = [];
    var tr = "";

    $("#recipient-view option").each(function(i, v) {
      recipient.push($(v).val());
      tr += "<tr><td>" + $(v).text() + "</td></tr>";
      access.push({
        Id: $(v).val(),
        Name: $(v).text(),
        ContractorType: $(v).data("type")
      });
    });

    $("#AccessList").val(JSON.stringify(recipient));
    $("#RecipientListTable").html(tr);
    $("#Recipients").val(JSON.stringify(access));
  });
}

function attachTypeGen(sel) {
  var options = "";
  $.each(reference.AttachmentGroups, function(key, val) {
    options +=
      '<option value="' +
      val.Value +
      '" ' +
      (sel == val.Description ? "selected" : "") +
      ">" +
      val.Description +
      "</option>";
  });
  return options;
}

function QuestionParse(key, val) {
  tabIndex = $("#tabs").children().length + 1;
  tabs = "";
  tab_content = "";
  if (
    open_item.Data.Object.Document.DelRec == false &&
    open_item.Data.Object.Document.RegState == 0
  )
    var delBtn =
      '<a class="del-quest" data-action="delete" data-data="' +
      key +
      '" data-id="' +
      tabIndex +
      '"><img title="Удалить" alt="Удалить" src="img/ui/delete_inactive.png"></a>';
  else var delBtn = "";
  tabs +=
    '<li role="presentation">' +
    '<a aria-expanded="true" class="quest-item"  href="#tab' +
    tabIndex +
    '" data-tab="' +
    tabIndex +
    '" data-data="' +
    key +
    '"  aria-controls="tab' +
    tabIndex +
    '" role="tab"  data-toggle="tab">Вопрос ' +
    (key + 1) +
    "</a>" +
    //data-item="' + k + '"
    delBtn +
    "</li>";

  tab_content +=
    '<div id="tab' +
    tabIndex +
    '" class="tab-pane question-tab" data-data="' +
    key +
    '" role="tabpanel">' +
    tabIndex +
    "</div>";

  $("#tabs").append(tabs);
  $(".tab-content").append(tab_content);

  $.ajax({
    url: "views/0501022_1.html",
    cache: false,
    async: false,
    success: function(view) {
      $("#tab" + tabIndex).html(view);

      $.each(val, function(index, value) {
        if (index == "Code") {
          if (value) {
            $("#tab" + tabIndex + " #" + index).val(value.Value);
            $("#tab" + tabIndex + " #CodeId").val(value.id);
          }
          $("#tab" + tabIndex + " .select-Questions").data("id", tabIndex);
        } else if (index == "ResponseDate" || index == "TransferDate") {
          if (value) {
            $("#tab" + tabIndex + " #" + index).val(
              DateFromMSDateForForm(value)
            );
          }
        } else if (index == "ActionsTaken") {
          $("#tab" + tabIndex + " #" + index).prop("checked", value);
        } else if (index == "Attachment") {
          $("#tab" + tabIndex + " .select-attach").data("id", tabIndex);
          $("#tab" + tabIndex + " .clear-attach").data("id", tabIndex);
          $("#tab" + tabIndex + " #exampleInputFile").data("id", tabIndex);
          if (value) {
            $("#tab" + tabIndex + " #" + index).val(value.Name);
            $("#tab" + tabIndex + " .Answer").hide();
          }
          // else
          // $('#tab' + tabIndex +' .Answer').show()
        } else if (index == "Answer") {
          $("#tab" + tabIndex + " .select-doc").data("id", tabIndex);
          $("#tab" + tabIndex + " .clear-doc").data("id", tabIndex);
          if (value) {
            answTitle =
              "№" +
              value.Card.RegNumber +
              " от " +
              DateFromMSDate(value.Card.RegDate) +
              ' "' +
              value.Theme +
              '"';
            $("#tab" + tabIndex + " #" + index).val(answTitle);
            $("#tab" + tabIndex + " .Attachment").hide();
          }
          // else
          // $('#tab' + tabIndex +' .Attachment').show()
        } else if (index == "Status") {
          var opt = "";
          $.each(reference.CitizenStatementQuestionStatus, function(inde, el) {
            var a = "";
            if (value == el.Value) {
              a = "selected";
            }
            opt +=
              '<option value="' +
              el.Value +
              '"  ' +
              a +
              ">" +
              el.Description +
              "</option>";
          });
          $("#tab" + tabIndex + " #" + index).html(opt);
          $("#tab" + tabIndex + " #" + index).data("id", tabIndex);
          if (open_item.Data.Object.Document.RegState != 1) {
            $("#tab" + tabIndex + " #" + index).prop("disabled", true);
          }
          $("#tab" + tabIndex + " #" + index).chosen({
            width: "100%"
          });
          if (value == 0 || value == 10) {
            $("#tab" + tabIndex + " #Answ").hide();
            $("#tab" + tabIndex + " #Trans").hide();
          } else if (value == 50) {
            $("#tab" + tabIndex + " #Answ").hide();
          } else {
            $("#tab" + tabIndex + " #Trans").hide();
            if (value == 70) {
              $("#tab" + tabIndex + " #Answ .Answer").hide();
              $("#tab" + tabIndex + " #Answ .Attachment").hide();
            }
            if (value != 30) {
              $("#tab" + tabIndex + " #Answ .ActionsTaken").hide();
            }
          }
        } else if (index == "TransferDepartment") {
          $.ajax({
            url:
              "http://" +
              ip +
              "/EAkimat/CitizenStatements.GovDepartment.Select",
            xhrFields: {
              withCredentials: false
            },
            beforeSend: function(xhr) {
              xhr.setRequestHeader("content-type", "application/json");
              xhr.setRequestHeader("Token", token);
            },
            success: function(data) {
              opt = "";
              $.each(data.data, function(i, e) {
                if (value) {
                  if (e[0] == value.id) var sel = "selected";
                  else var sel = "";
                }
                opt +=
                  '<option value="' +
                  e[0] +
                  '" ' +
                  sel +
                  ">" +
                  e[1] +
                  "</option>";
              });
              $("#tab" + tabIndex + " #" + index).html(opt);
              $("#tab" + tabIndex + " #" + index).chosen({
                width: "100%"
              });
            }
          });
        } else if (index == "SubQuestion") {
          // LoadOutputRef('.'+index)
          if (val.Code) {
            $.ajax({
              url:
                " http://" +
                ip +
                "/EAkimat/References.ByParent?refType=1409&parentid=" +
                val.Code.id,
              async: false,
              beforeSend: function(xhr) {
                xhr.setRequestHeader("content-type", "application/json");
                xhr.setRequestHeader("Token", token);
              },
              success: function(data) {
                var options = '<option value="0"></option>';

                $.each(data, function(key, val) {
                  options +=
                    '<option value="' +
                    val.id +
                    '" ' +
                    (val.id == value.id ? "selected" : "") +
                    ">" +
                    val.Value +
                    "</option>";
                });

                $("#tab" + tabIndex + " #" + index).html(options);
              }
            });
          }
          $("#tab" + tabIndex + " #" + index).chosen({
            width: "100%",
            allow_single_deselect: false
          });

          // if (value) {
          //   $('#tab' + tabIndex+' #'+index).val(value.Value);
          //   $('#tab' + tabIndex+' #'+index).trigger('chosen:updated');
          // }
        } else {
          if (value) {
            $("#tab" + tabIndex + " #" + index).val(value);
          }
        }
      });
    }
  });
}

function Login() {
  $.ajax({
    url: "login.html",
    cache: false,
    success: function(data) {
      $("#body").html(data);
      $("body").addClass("login_body");

      $(".save").on("click", function() {
        var obj = JSON.stringify({
          Username: $("#login_input").val(),
          Password: hex_sha256($("#password").val())
        });

        $.ajax({
          url: "http://" + ip + "/EAkimat/Auth",
          xhrFields: {
            withCredentials: false
          },
          data: obj,
          method: "POST",
          beforeSend: function(xhr) {
            xhr.setRequestHeader("content-type", "application/json");
          },
          success: function(data) {
            $.session.set("token", data.Token);
            $.session.set("UserName", data.UserName);
            document.cookie = "Token=" + data.Token;
            window.location = "http://" + site_url + "/";
          },
          error: function(error, er2, er3) {
            $.notify(error.responseJSON, {
              type: "danger"
            });
          }
        });
      });
    }
  });
}

function Logout(token) {
  $.session.set("token", "");

  $.ajax({
    url: "http://" + ip + "/EAkimat/LogOut",
    xhrFields: {
      withCredentials: false
    },
    beforeSend: function(xhr) {
      xhr.setRequestHeader("content-type", "application/json");
      xhr.setRequestHeader("Token", token);
    },
    success: function(data) {
      window.location = "http://" + site_url + "/";
    }
  });

  //window.location = "http://" + site_url + "/";
}

function LeftMenu(token) {
  var leftmenuitems = [
      {
          l1: "Главная",
          action: "index",
          l2: [
              {
                  text: "00403010",
                  service: "00403010",
                  action: "0801010",
                  icon: null,
                  count: "Statement.Counter"
              }
          ]
      }
    
  ];

  var leftmenu_html = "<ul>";

  $.each(leftmenuitems, function(k, v) {
    var l2 = "";
    
    l2 += "<ul>";
    $.each(v.l2, function(k2, v2) {
      l2 += '<li><a class="item' + k2 + '" data-action="' + v2.action + '" data-service="' + v2.service + '">' +
        v2.text +
        '</a><span class="mini-menu-count badge"></span></li>';
      
    });
    l2 += "</ul>";

   
    leftmenu_html += '<li class="sub childmenu panel" id="menuItem' + k + '">';
    leftmenu_html +=
      '<a href="#" class="grouptitle" data-action="' + v.action + '"  >';
    // leftmenu_html += '<span class="glyphicon glyphicon-home icons"></span>'
    leftmenu_html += v.l1 + '<span class="fa fa-chevron-down"></span>';
    leftmenu_html += "</a>";
    leftmenu_html += l2;
    leftmenu_html += "</li>";
  
  });
  leftmenu_html += "</ul>";

  $("#leftmenu").html(leftmenu_html);
  $("#leftmenu #menuItem0").addClass("in");
  var user = $.session.get("UserName");
  var name = user.substring(0, user.indexOf("(")).toLowerCase();
  var workplace = user.substring(user.indexOf("(") + 1, user.length - 1);
  $(".user-name").html(
    name + '<span class="user-workplace">' + workplace + "</span>"
  );
  $(".menu-user-name").html(name);
  // $.each(leftmenuicons,function(index, el) {
  //   $('[data-action="'+el.action+'"]').css('background-image', 'url(../img/'+el.before+') no-repeat')

  // });
  var token = $.session.get("token");
  $.each(leftmenuitems, function(k, v) {
    $.each(v.l2, function(k2, v2) {
      if (v2.count) {
        var params = "";
        if (v2.params) {
          params = "?" + v2.params;
        }

        // $.ajax({
        //   url: "http://" + ip + "/EAkimat//" + v2.count + params,
        //   xhrFields: {
        //     withCredentials: true
        //   },
        //   beforeSend: function(xhr) {
        //     xhr.setRequestHeader("content-type", "application/json");
        //     xhr.setRequestHeader("Token", token);
        //   },
        //   success: function(data) {
        //     if (Number(data) != 0) {
             
        //       $("#menuItem" + k + " .item" + k2 + "")
        //         .parent()
        //         .find(".mini-menu-count")
        //         .text(data);
             
        //     }
        //   }
        // });

      }
    });
  });
  $(".panel").click(function() {
    $(".panel").removeClass("active");
    $(this).addClass("active");
  });
  $.ajax({
    url: "views/index.html",
    cache: false,
    //async: false,
    success: function(data) {
      $(".forcontent").html(data);
    }
  });

  var menuarr = {
    "0104010": ["item1", "Chancellery.Document.Counter", "menuItem0"],
    "0102010": ["item2", "Chancellery.Counter.Counter", "menuItem0"],
    "0401010": ["item1", "Orders.Counter", "menuItem2"],
    "0501010": [
      "item1",
      "CitizenStatements.CitizenStatement.Counter",
      "menuItem3"
    ],
    "0501060": [
      "item2",
      "CitizenStatements.GovDepartment.Counter",
      "menuItem3"
    ],
    "0203010": ["item1", "Documents.Draft.Counter", "menuItem1"],
    "0203020": [
      "item2",
      "Documents.DocumentsOnAdjustment.Counter",
      "menuItem1"
    ],
    "0203030": [
      "item3",
      "Documents.DocumentsOnRegistration.Counter",
      "menuItem1"
    ],
    "0203040": ["item4", "Documents.Correspondence.Counter", "menuItem1"],
    "0203050": ["item6", "Documents.ControllingDocuments.Counter", "menuItem1"],
    "0204010": ["item7", "Documents.Storage.Counter", "menuItem1"],
    "0205010": ["item8", "Documents.Resolutions.Counter", "menuItem1"],
    "0206010": ["item9", "Documents.ExecuterGroup.Counter", "menuItem1"],
    "0103010": ["item4", "Chancellery.Nomenclature.Counter", "menuItem1"],
    "0701010": ["item5", "Documents.Act.Counter", "menuItem1"]
  };

  $("#leftmenu .childmenu li a").on("click", function() {
    var view = $(this).data("action");
    var service = $(this).data("service");

    // if (menuarr[view]) {
    //   $.ajax({
    //     url: "http://" + ip + "/EAkimat/Collection/" + menuarr[view][1],
    //     xhrFields: {
    //       withCredentials: true
    //     },
    //     beforeSend: function(xhr) {
    //       xhr.setRequestHeader("content-type", "application/json");
    //       xhr.setRequestHeader("Token", token);
    //     },
    //     success: function(data) {
    //       $(
    //         "#" + menuarr[view][2] + " ." + menuarr[view][0] + " .itemcount a"
    //       ).text(data);
    //     }
    //   });
    // }
    $("#leftmenu .childmenu li").removeClass("active");
    $(this)
      .parent()
      .addClass("active");
    $.ajax({
      url: "views/" + view + ".html",
      async:true,
      cache: false,
      //async: false,
      success: function(data) {
        $(".modal").remove();
        $(".forcontent").html(data);
        $('#service').val(service)
      }
    });
  });
  //   BENCHMARK

  var search = location.search.substring(1);
  if (search) {
    loadTime = 0;
    // window.setInterval = workerTimers.setInterval;
    myWorker = new Worker("js/worker.js");
    myWorker.addEventListener(
      "message",
      function(e) {
        console.log(e.data);
        var get = JSON.parse(
          '{"' +
            decodeURI(search)
              .replace(/"/g, '\\"')
              .replace(/&/g, '","')
              .replace(/=/g, '":"') +
            '"}'
        );
        if (get.tab) {
          // myWorker.postMessage('0');
          localStorage.setItem(get.tab, e.data);
        }
        loadTime = e.data;
      },
      false
    );
    var get = JSON.parse(
      '{"' +
        decodeURI(search)
          .replace(/"/g, '\\"')
          .replace(/&/g, '","')
          .replace(/=/g, '":"') +
        '"}'
    );

    if (get.view) {
      $("#leftmenu .childmenu li a[data-action=" + get.view + "]").click();
    } else if (get.type) {
      // $.ajax({
      //   url:"http://" + ip +"/EAkimat/" + get.action + "/" + get.type + "/?id=" +get.id,
      //   beforeSend: function(xhr) {
      //     xhr.setRequestHeader("content-type", "application/json");
      //     xhr.setRequestHeader("Token", token);
      //   },
      //   success: function(data) {
      //     if (data.Result == "OK") {
      //       $(".forcontent").html("");
      //       openedObjed = data;
      //       View(data);
      //     } else {
      //       $.notify("" + data.Message, { type: "danger" });
      //     }
      //   }
      // });
    }
  }

  //
}

$(document).on("click", ".notifyCenter", function() {
  var view = $(this).data("action");

  $.ajax({
    url: "views/" + view + ".html",
    cache: false,
    async: false,
    success: function(data) {
      $(".forcontent").html(data);
    }
  });
});

function newFolderAction() {
  $("body").append(
    '<div class="modal" id="folderModal" tabindex="-1" role="dialog">' +
      '<div class="modal-dialog" role="document">' +
      '<div class="modal-content">' +
      '<div class="modal-header">' +
      '<div class="title">Новая папка</div>' +
      '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times-rectangle"></i></button>' +
      "</div>" +
      '<div class="modal-body">' +
      '<div class="col-xs-12 col-sm-12 folderNewSelect">' +
      '<label class="required">Родительская папка</label>' +
      '<input id="FolderNewValue" class="w100 text-field form-control">' +
      '<input type="hidden" id="FolderNew">' +
      '<div id="FolderNewList" style="display: none;"></div>' +
      "</div>" +
      '<div class="col-xs-12 col-sm-12" style="margin-bottom:15px;">' +
      '<label class="required">Наименование</label>' +
      '<input id="NewFolderName" class="w100 text-field form-control">' +
      "</div>" +
      "</div>" +
      '<div class="formsep" style="clear:both;"></div>' +
      '<div class="modal-footer">' +
      '<div class="buttons"><a class="save save-new-folder" data-dismiss="modal">ОТПРАВИТЬ</a><a class="cancel" data-dismiss="modal">Отмена</a></div>' +
      "</div>" +
      "</div>" +
      "</div>" +
      "</div>"
  );
  $("#folderModal").modal("show");
  var newObj;
  $.ajax({
    url: "http://" + ip + "/EAkimat/new/Documents.Folder",
    async: false,
    cache: false,
    method: "GET",
    beforeSend: function(xhr) {
      xhr.setRequestHeader("content-type", "application/json");
      xhr.setRequestHeader("Token", token);
    },
    success: function(data) {
      newObj = data;
    }
  });
  $(".folderNewSelect").hover(
    function() {
      $("#FolderNewList").show();
    },
    function() {
      $("#FolderNewList").hide();
    }
  );
  var L;
  $.ajax({
    url: "http://" + ip + "/EAkimat/Documents.Storage.Folder.List",
    xhrFields: {
      withCredentials: false
    },
    beforeSend: function(xhr) {
      xhr.setRequestHeader("content-type", "application/json");
      xhr.setRequestHeader("Token", token);
    },
    success: function(data) {
      L = data.data;

      $("#FolderNewList").jstree({
        core: {
          data: L,
          multiple: false
        }
      });
    }
  });
  $("#FolderNewList").on("changed.jstree", function(e, data) {
    if (data.selected[0] != null)
      if (data.instance.get_node(data.selected[0]).state.disabled == false) {
        $("#FolderNewValue").val(data.instance.get_node(data.selected[0]).text);
        $("#FolderNew").val(data.selected[0]);
      }
  });

  $(document).one("click", ".save-new-folder", function() {
    if ($("#NewFolderName").val() != "") {
      if ($("#FolderNew").val() == "") {
        var parentObj = null;
      } else {
        var parentObj = {
          __type: "StorageFolder:#Avrora.Objects.Storage",
          id: $("#FolderNew").val()
        };
      }

      newObj.Data.Object.Name = $("#NewFolderName").val();
      newObj.Data.Object.Parent = parentObj;

      $.ajax({
        url: "http://" + ip + "/EAkimat/save/Documents.Folder",
        async: false,
        cache: false,
        method: "POST",
        data: JSON.stringify({
          parent: newObj.Data.Object,
          childrens: null,
          type: "Documents.Folder"
        }),
        beforeSend: function(xhr) {
          xhr.setRequestHeader("content-type", "application/json");
          xhr.setRequestHeader("Token", token);
        },
        success: function(data) {
          if (data.Result == "OK") {
            $.notify(data.Message, {
              type: "success"
            });
            var L;
            $.ajax({
              url: "http://" + ip + "/EAkimat/Documents.Storage.Folder.List",
              xhrFields: {
                withCredentials: false
              },
              beforeSend: function(xhr) {
                xhr.setRequestHeader("content-type", "application/json");
                xhr.setRequestHeader("Token", token);
              },
              success: function(data) {
                L = data.data;
                $("#Folder").jstree(true).settings.core.data = L;
                $("#Folder")
                  .jstree(true)
                  .refresh();
              }
            });
          } else {
            $.notify(data.Message, {
              type: "danger"
            });
          }
          $("#folderModal").modal("hide");
          $("#folderModal").remove();
        }
      });
    }
  });
}

function EditFolderAction(data) {
  var newObj;
  $.ajax({
    url: "http://" + ip + "/EAkimat/edit/Documents.Folder?id=" + data,
    //async: false,
    cache: false,
    method: "GET",
    beforeSend: function(xhr) {
      xhr.setRequestHeader("content-type", "application/json");
      xhr.setRequestHeader("Token", token);
    },
    success: function(data) {
      if (data.Result == "OK") {
        $("body").append(
          '<div class="modal" id="folderModal" tabindex="-1" role="dialog">' +
            '<div class="modal-dialog" role="document">' +
            '<div class="modal-content">' +
            '<div class="modal-header">' +
            '<div class="title">Новая папка</div>' +
            '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times-rectangle"></i></button>' +
            "</div>" +
            '<div class="modal-body">' +
            '<div class="col-xs-12 col-sm-12 folderNewSelect">' +
            '<label class="required">Родительская папка</label>' +
            '<input id="FolderNewValue" class="w100 text-field form-control">' +
            '<input type="hidden" id="FolderNew">' +
            '<div id="FolderNewList" style="display: none;"></div>' +
            "</div>" +
            '<div class="col-xs-12 col-sm-12" style="margin-bottom:15px;">' +
            '<label class="required">Наименование</label>' +
            '<input id="NewFolderName" class="w100 text-field form-control">' +
            "</div>" +
            "</div>" +
            '<div class="formsep" style="clear:both;"></div>' +
            '<div class="modal-footer">' +
            '<div class="buttons"><a class="save save-new-folder" data-dismiss="modal">ОТПРАВИТЬ</a><a class="cancel" data-dismiss="modal">Отмена</a></div>' +
            "</div>" +
            "</div>" +
            "</div>" +
            "</div>"
        );
        $("#folderModal").modal("show");

        newObj = data;
        var L;
        $.ajax({
          url: "http://" + ip + "/EAkimat/Documents.Storage.Folder.List",
          xhrFields: {
            withCredentials: false
          },
          beforeSend: function(xhr) {
            xhr.setRequestHeader("content-type", "application/json");
            xhr.setRequestHeader("Token", token);
          },
          success: function(data) {
            L = data.data;

            $("#FolderNewList").jstree({
              core: {
                data: L,
                multiple: false
              }
            });
          }
        });
        if (data.Data.Object.Parent) {
          $("#FolderNew").val(data.Data.Object.Parent.id);
          $("#FolderNewValue").val(data.Data.Object.Parent.Name);
        }
        $("#NewFolderName").val(data.Data.Object.Name);

        $(".folderNewSelect").hover(
          function() {
            $("#FolderNewList").show();
          },
          function() {
            $("#FolderNewList").hide();
          }
        );

        $("#FolderNewList").on("changed.jstree", function(e, data) {
          if (data.selected[0] != null)
            if (
              data.instance.get_node(data.selected[0]).state.disabled == false
            ) {
              $("#FolderNewValue").val(
                data.instance.get_node(data.selected[0]).text
              );
              $("#FolderNew").val(data.selected[0]);
            }
        });

        $(document).one("click", ".save-new-folder", function() {
          if ($("#NewFolderName").val() != "") {
            if ($("#FolderNew").val() == "") {
              var parentObj = null;
            } else {
              var parentObj = {
                __type: "StorageFolder:#Avrora.Objects.Storage",
                id: $("#FolderNew").val()
              };
            }

            newObj.Data.Object.Name = $("#NewFolderName").val();
            newObj.Data.Object.Parent = parentObj;

            $.ajax({
              url: "http://" + ip + "/EAkimat/save/Documents.Folder",
              // async: false,
              cache: false,
              method: "POST",
              data: JSON.stringify({
                parent: newObj.Data.Object,
                childrens: null
              }),
              beforeSend: function(xhr) {
                xhr.setRequestHeader("content-type", "application/json");
                xhr.setRequestHeader("Token", token);
              },
              success: function(data) {
                if (data.Result == "OK") {
                  $.notify(data.Message, {
                    type: "success"
                  });

                  //$('#Folder').jstree(true).refresh(true);
                  // var tree = $.jstree._reference("#Folder");
                  // tree.refresh();
                  var L;
                  $.ajax({
                    url:
                      "http://" +
                      ip +
                      "/EAkimat/Documents.Storage.Folder.List",
                    xhrFields: {
                      withCredentials: false
                    },
                    beforeSend: function(xhr) {
                      xhr.setRequestHeader("content-type", "application/json");
                      xhr.setRequestHeader("Token", token);
                    },
                    success: function(data) {
                      L = data.data;
                      $("#Folder").jstree(true).settings.core.data = L;
                      $("#Folder")
                        .jstree(true)
                        .refresh();
                    }
                  });
                } else {
                  $.notify(data.Message, {
                    type: "danger"
                  });
                }
                $("#folderModal").modal("hide");
                $("#folderModal").remove();
              }
            });
          }
        });
      } else {
        $.notify(data.Message, { type: "danger" });
      }
    }
  });
}

function refToSelect(container) {
  if (typeof container == "undefined") {
    container = "";
  }
  if (container == "") var elems = $(".refToSelect");
  else elems = $(container + " .refToSelect");
  $.each(elems, function(ind, el) {
    var opts = "";
    var type = $(el).data("type");
    var selected = "";
    if ($(el).data("default")) {
      selected = $(el).data("default");
    }
    $.each(reference[type], function(index, elem) {
      if (elem.Name)
        opts +=
          '<option value="' +
          elem.Value +
          '" ' +
          (elem.Value == selected ? "selected" : "") +
          ">" +
          elem.Name +
          "</option>";
      else if (elem.Description)
        opts +=
          '<option value="' +
          elem.Value +
          '" ' +
          (elem.Value == selected ? "selected" : "") +
          ">" +
          elem.Description +
          "</option>";
    });
    $(el).html(opts);
    $(el).chosen({
      width: "100%",
      allow_single_deselect: false,
      disable_search: true
    });
  });
}

function TaskParse(key, val, obj, type) {
  if (type == "0401022") {
    var AL2Links = "";
    var AL2Tabs = "";
    var active = "";

    if (key == 0) {
      active = "active";
    }
    if (val.TaskNumber == "") {
      var text = $('a[role="tab"]').text();
      var spl = text.split(" ");

      val.TaskNumber = Number(spl[spl.length - 1]) + 1;
    }
    var delBtn =
      '<a class="del-task" data-action="delete" data-id="' +
      key +
      '"><img title="Удалить" alt="Удалить" src="img/ui/delete_inactive.png"></a>';
    AL2Links =
      '<li role="presentation" class="' +
      active +
      '">' +
      '<a aria-expanded="true" href="#tab' +
      key +
      '" data-id="' +
      key +
      '" aria-controls="tab' +
      key +
      '" role="tab" data-toggle="tab">Задача</a>' +
      delBtn +
      "</li>";
    AL2Tabs = '<div id="tab' +  key + '" data-id="' + key +'" class="tab-pane ' + active +  '" role="tabpanel"><div class="section row">' +
      '<div class="col-xs-12 col-sm-12 col-lg-12 col-md-12">' +
      '<div style="margin-bottom: 15px;">' +
      '<label class="required" style="float: left;display: block; margin-right: 20px;width: auto;">Текст задачи</label>' +
      '<a class="add-access add-text' +
      key +
      '" data-index="' +
      key +
      '"><i class="fa fa-plus-circle"></i> Выбрать</a>' +
      '<div style="clear: both;"></div>' +
      "</div>" +
      '<textarea class="text-field w100"  name="ResolutionText" id="ResolutionText">' +
      getTaskData(val, "ResolutionText") +
      "</textarea>" +
      "</div> " +
      '<div class="col-xs-12 col-sm-12 col-lg-6 col-md-6">' +
      '<label class="required">Тип контроля </label>' +
      '<select data-default="' +
      getTaskData(val, "ControlType") +
      '" value="' +
      getTaskData(val, "ControlType") +
      '" type="text" data-placeholder="Выберите значение..." class="text-field w100 refOut chosen-ref" data-refid="1004" name="" id="ControlType" required></select>' +
      "</div>" +
      '<div class="col-xs-12 col-sm-12 col-lg-6 col-md-6">' +
      '<label class="required">Контрольный срок</label>' +
      '<input type="date" class="text-field col-xs-4" name="" value="' +
      getTaskData(val, "InnerLimit") +
      '" id="InnerLimit">' +
      "</div>" +
      '<div class="col-xs-12 col-sm-12 col-lg-6 col-md-6">' +
      '<label class="required">Периодичность</label>' +
      '<select data-default="' +
      getTaskData(val, "Periodicity") +
      '" class="text-field refToSelect w100" data-type="ResolutionPeriods" id="Periodicity" required></select>' +
      "</div>" +
      '<div class="col-xs-12 col-sm-12 col-lg-6 col-md-6">' +
      '<label class="required">Конец периода</label>' +
      '<input type="date" class="text-field col-xs-4" name="" value="' +
      getTaskData(val, "PeriodEndDate") +
      '" id="PeriodEndDate">' +
      "</div>" +
      '<div class="col-xs-12 col-sm-12 col-lg-6 col-md-6">' +
      '<label class="required">Приоритет</label>' +
      '<select data-default="' +
      getTaskData(val, "ControlPriority") +
      '" type="text" class="text-field w100 refOut chosen-ref" data-placeholder="Выберите значение..." data-refid="1008" id="ControlPriority"></select>' +
      "</div>" +
      '<div class="col-xs-12 col-sm-12 col-lg-6 col-md-6">' +
      '<label class="required">Контролирующий</label>' +
      '<select type="text" class="text-field w100 chosen-ref" value="' +
      getTaskData(val, "ControlerName") +
      '"  id="ControlerName"></select>' +
      "</div>" +
      '<div class="col-xs-12 col-sm-12 col-lg-6 col-md-6">' +
      '<label class="required">Статус</label>' +
      '<input class="text-field w100" value="' +
      getTaskData(val, "WorkStatus") +
      '" id="WorkStatus" readonly>' +
      "</div>" +
      '<div class="col-xs-12 col-sm-12 col-lg-6 col-md-6" style="height:78px;">' +
      '<label class="required">Личный контроль</label>' +
      '<input type="checkbox" ' +
      (val.IsSignerControl ? "checked" : "") +
      ' id="IsSignerControl">' +
      "</div>" +
      '<div class="col-xs-12">' +
      '<div style="margin-bottom: 15px;">' +
      '<label class="required" style="float: left;display: block; margin-right: 20px;width: auto;">Исполнители</label>' +
      '<input type="hidden" id="AccessList' +
      key +
      '" class="AccessList2" name="AccessList' +
      key +
      '" required>' +
      '<input type="hidden" id="NewExecuterList' +
      key +
      '"  required>' +
      '<a style="padding-top:5px;" class="add-access" data-toggle="modal" data-target="#access-list' + key + '"> Изменить</a>' +
      '<div style="clear: both;"></div>' +
      "</div>" +
      '<div class="modal" id="access-list' +
      key +
      '" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">' +
      '<div class="modal-dialog modal-lg" role="document">' +
      '<div class="modal-content">' +
      '<div class="modal-header">' +
      '<div class="title">Исполнители</div>' +
      '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times-rectangle"></i></button>' +
      "</div>" +
      '<div class="modal-body">' +
      '<div class="row">' +
      '<div class="col-xs-6">' +
      '<select class="w100 form-control access-select' +
      key +
      '" data-key="' +
      key +
      '" id="access-select" size="10" data-placeholder="Выберите значение..."></select>' +
      "</div>" +
      '<div class="col-xs-6">' +
      '<select class="w100 form-control access-view' +
      key +
      '" data-key="' +
      key +
      '" id="access-view" size="10" data-placeholder="Выберите значение..."></select>' +
      "</div>" +
      "</div>" +
      "</div>" +
      '<div class="formsep"></div>' +
      '<div class="modal-footer"> ' +
      '<div class="buttons"><a class="save save-access' +
      key +
      '" data-key="' +
      key +
      '" data-dismiss="modal">ОТПРАВИТЬ</a><a class="cancel" data-dismiss="modal">Отмена</a></div>' +
      "</div>" +
      "</div>" +
      "</div>" +
      "</div>" +
      "</div>" +
      '<div class="col-xs-12 col-md-12">' +
      '<table class="table table-bordered no-sorted table-hover">' +
      "<thead>" +
      "<th>ИСПОЛНИТЕЛЬ</th>" +
      "<th>ОРГАНИЗАЦИЯ</th>" +
      "<th>СВОД</th>" +
      "</thead>" +
      '<tbody id="AccessListTable' +
      key +
      '">' +
      getTaskData(val, "Executers", "0401022") +
      "</tbody>" +
      "</table>" +
      "</div>" +
      "</div>"+
      "</div>";
    AccessList2(val.SelectedEmsObjectsId, key);
    $(".AL2Links").append(AL2Links);
    $(".AL2Tabs").append(AL2Tabs);
    $(".add-text" + key).on("click", function() {
      $("#add-text").remove();
      $("body").append(
        '<div class="modal" id="add-text" tabindex="-1" role="dialog">' +
          '<div class="modal-dialog" role="document">' +
          '<div class="modal-content">' +
          '<div class="modal-header">' +
          '<div class="title">Текст поручения</div>' +
          '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times-rectangle"></i></button>' +
          "</div>" +
          '<div class="modal-body">' +
          '<div class="row">' +
          '<div class="col-xs-12 col-sm-12 col-lg-12 col-md-12">' +
          '<label class="required">Текст задачи</label>' +
          '<select class="text-field w100" size="8" name="" id="task-text"></select>' +
          "</div>" +
          '<input type="hidden" id="fieldIndex"/>' +
          "<div>" +
          "</div>" +
          '<div class="formsep" style="clear:both;"></div>' +
          '<div class="modal-footer">' +
          '<div class="buttons"><a class="save save-text">ОТПРАВИТЬ</a><a class="cancel" data-dismiss="modal">Отмена</a></div>' +
          "</div>" +
          "</div>" +
          "</div>" +
          "</div>"
      );
      $("#add-text").modal("show");
      $("#fieldIndex").val($(this).data("index"));
      $.ajax({
        url: " http://" + ip + "/EAkimat/References?reftype=1017",
        async: false,
        beforeSend: function(xhr) {
          xhr.setRequestHeader("content-type", "application/json");
          xhr.setRequestHeader("Token", token);
        },
        success: function(data) {
          var options = "";

          $.each(data, function(key, val) {
            options +=
              '<option value="' + val.id + '">' + val.Value + "</option>";
          });

          $("#task-text").html(options);
        }
      });
      $(".save-text").on("click", function() {
        $("#tab" + $("#fieldIndex").val() + " #ResolutionText").val(
          $("#task-text option:selected").text()
        );
        $("#add-text").modal("hide");
        $("#add-text").remove();
      });
    });
    taskState = jQuery.extend(true, {}, obj.Tasks);
    $("#tab" + key + " #NewExecuterList" + key).val(
      JSON.stringify(val.Executers)
    );
    $(document).on("click", ".save-access" + key, function() {
      var id = $(this).data("key");
      var data = [];
      $(".access-view" + id + " option").each(function(i, v) {
        data.push([
          $(v).val(),
          $(v).text(),
          $(v)
            .parent()
            .attr("label"),
          $(v).data("v")
        ]);
      });

      $("#tab" + id + " #AccessList" + id).val(JSON.stringify(data));

      $.ajax({
        url:
          "http://" + ip + "/EAkimat/Documents.Resolution.NewExecuterList",

        method: "POST",
        data: JSON.stringify({
          resolutionId: obj.id,
          taskId: obj.Tasks[id].id,
          executerDataList: {
            __type: "DataList:#Avrora.Objects.Web",
            Data: data
          }
        }),
        beforeSend: function(xhr) {
          xhr.setRequestHeader("content-type", "application/json");
          xhr.setRequestHeader("Token", token);
        },
        success: function(data2) {
          $("#tab" + id + " #NewExecuterList" + id).val(
            JSON.stringify(data2.Data.Object)
          );
          var nummpp = 0;
          var tr = "";
          $.each(data2.Data.Object, function(k, v) {
            nummpp++;
            tr +=
              "<tr>" +
              "<td>" +
              v.ExecuterName +
              "</td>" +
              "<td>" +
              v.EnterpriseName +
              "</td>";

            tr +=
              '<td><input id="' +
              v.id +
              '" value="' +
              nummpp +
              '" type="radio" ' +
              (v.Svod ? "checked" : "") +
              ' name="r"></td>' +
              "</tr>";
          });

          $("#AccessListTable" + id).html(tr);
        }
      });

    });
  } else if (type == "0205022") {
    var AL2Links = "";
    var AL2Tabs = "";
    var active = "";

    if (key == 0) {
      active = "active";
    }
    if (val.TaskNumber == "") {
      var text = $('a[role="tab"]').text();
      var spl = text.split(" ");

      val.TaskNumber = Number(spl[spl.length - 1]) + 1;
    }
    var delBtn = '<a class="del-task" data-action="delete" data-id="' + key + '"><img title="Удалить" alt="Удалить" src="img/ui/delete_inactive.png"></a>';
    AL2Links = '<li role="presentation" class="' + active + '">' + '<a aria-expanded="true" href="#tab' + key + '" data-id="' + key + '" aria-controls="tab' + key + '" role="tab" data-toggle="tab">Пункт ' +
      val.TaskNumber +"</a>" + delBtn +"</li>";
    AL2Tabs = '<div id="tab' + key + '" data-id="' + key + '" class="tab-pane ' + active + '" role="tabpanel"><div class="section row">' +
      '<div class="col-xs-12 col-sm-12 col-lg-12 col-md-12">' +
      '<label class="required">Текст поручения</label>' +
      '<textarea class="text-field w100" name="" id="ResolutionText" required>' +
      getTaskData(val, "ResolutionText") +
      "</textarea>" +
      "</div>" +
      '<div class="col-xs-12 col-sm-12 col-lg-6 col-md-6">' +
      '<label class="required">Тип контроля </label>' +
      '<select data-default="' +
      getTaskData(val, "ControlType") +
      '" type="text" class="text-field w100 refOut chosen-ref" data-refid="1004" name="" id="ControlType" required></select>' +
      "</div>" +
      '<div class="col-xs-12 col-sm-12 col-lg-6 col-md-6">' +
      '<label class="required">Контролирующий</label>' +
      '<select type="text" class="text-field w100 chosen-ref" value="' +
      getTaskData(val, "ControlerName") +
      '"  id="ControlerName"></select>' +
      "</div>" +
      '<div class="col-xs-12 col-sm-12 col-lg-6 col-md-6">' +
      '<label class="required">Периодичность</label>' +
      '<select data-default="' +
      getTaskData(val, "Periodicity") +
      '"  class="text-field refToSelect w100" data-type="ResolutionPeriods" id="Periodicity" required></select>' +
      "</div>" +
      '<div class="col-xs-12 col-sm-12 col-lg-6 col-md-6">' +
      '<label class="required">Конец периода</label>' +
      '<input type="date" class="text-field col-xs-4" name="" value="' +
      getTaskData(val, "PeriodEndDate") +
      '" id="PeriodEndDate">' +
      "</div>" +
      '<div class="col-xs-12 col-sm-12 col-lg-6 col-md-6">' +
      '<label class="required">Контрольный срок</label>' +
      '<input type="date" class="text-field col-xs-4" name="" value="' +
      getTaskData(val, "InnerLimit") +
      '" id="InnerLimit">' +
      "</div>" +
      '<div class="col-xs-12 col-sm-12 col-lg-6 col-md-6">' +
      '<label class="required">Сроки продлений</label>' +
      '<input type="text" class="text-field w100" name="" value="' +
      getTaskData(val, "ProlongationDateList") +
      '" id="ProlongationDateList" readonly>' +
      "</div>" +
      '<div class="col-xs-12 col-sm-12 col-lg-6 col-md-6">' +
      '<label class="required">Приоритет</label>' +
      '<select data-default="' +
      getTaskData(val, "ControlPriority") +
      '" type="text" class="text-field w100 refOut chosen-ref" data-placeholder="Выберите значение..." data-refid="1008" id="ControlPriority"></select>' +
      "</div>" +
      '<div class="col-xs-12 col-sm-12 col-lg-6 col-md-6" style="height:78px;">' +
      '<label class="required">Личный контроль</label>' +
      '<input type="checkbox" ' +
      (val.IsSignerControl ? "checked" : "") +
      ' id="IsSignerControl">' +
      "</div>" +
      '<div class="col-xs-12 col-sm-12 col-lg-6 col-md-6">' +
      '<label class="required">Статус</label>' +
      '<input class="text-field w100" value="' +
      getTaskData(val, "WorkStatus") +
      '" id="WorkStatus" readonly>' +
      "</div>" +
      '<div class="col-xs-12">' +
      '<div style="margin-bottom: 15px;">' +
      '<label class="required" style="float: left;display: block; margin-right: 20px;width: auto;">Исполнители</label>' +
      '<input type="hidden" id="AccessList' +
      key +
      '" class="AccessList2" name="AccessList' +
      key +
      '" required>' +
      '<input type="hidden" id="NewExecuterList' +
      key +
      '"  required>' +
      '<a style="padding-top:5px;" class="add-access" data-toggle="modal" data-target="#access-list' + key +  '"> Изменить</a>' +
      '<div style="clear: both;"></div>' +
      "</div>" +
      '<div class="modal" id="access-list' +
      key +
      '" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">' +
      '<div class="modal-dialog modal-lg" role="document">' +
      '<div class="modal-content">' +
      '<div class="modal-header">' +
      '<div class="title">Исполнители</div>' +
      '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times-rectangle"></i></button>' +
      "</div>" +
      '<div class="modal-body">' +
      '<div class="row">' +
      '<div class="col-xs-6">' +
      '<select class="w100 form-control access-select' +
      key +
      '" data-key="' +
      key +
      '" id="access-select" size="10" data-placeholder="Выберите значение..."></select>' +
      "</div>" +
      '<div class="col-xs-6">' +
      '<select class="w100 form-control access-view' +
      key +
      '" data-key="' +
      key +
      '" id="access-view" size="10" data-placeholder="Выберите значение..."></select>' +
      "</div>" +
      "</div>" +
      "</div>" +
      '<div class="formsep"></div>' +
      '<div class="modal-footer"> ' +
      '<div class="buttons"><a class="save save-access' +
      key +
      '" data-key="' +
      key +
      '" data-dismiss="modal">ОТПРАВИТЬ</a><a class="cancel" data-dismiss="modal">Отмена</a></div>' +
      "</div>" +
      "</div>" +
      "</div>" +
      "</div>" +
      "</div>" +
      '<div class="col-xs-12 col-md-12">' +
      '<table class="table table-bordered no-sorted table-hover">' +
      "<thead>" +
      "<th>№ П/П</th>" +
      "<th>ИСПОЛНИТЕЛЬ</th>" +
      "<th>ОРГАНИЗАЦИЯ</th>" +
      "<th>СТАТУС</th>" +
      "<th>ДАТА ИСПОЛНЕНИЯ</th>" +
      "<th>СВОД</th>" +
      "</thead>" +
      '<tbody id="AccessListTable' +
      key +
      '">' +
      getTaskData(val, "Executers") +
      "</tbody>" +
      "</table>" +
      "</div></div>" +
      "</div>";
    AccessList2(val.SelectedEmsObjectsId, key);
    $(".AL2Links").append(AL2Links);
    $(".AL2Tabs").append(AL2Tabs);
    taskState = jQuery.extend(true, {}, obj.Tasks);
    $("#tab" + key + " #NewExecuterList" + key).val(
      JSON.stringify(val.Executers)
    );

    $(document).on("click", ".save-access" + key, function() {
      var id = $(this).data("key");
      var data = [];
      $(".access-view" + id + " option").each(function(i, v) {
        data.push([
          $(v).val(),
          $(v).text(),
          $(v)
            .parent()
            .attr("label"),
          $(v).data("v")
        ]);
      });

      $("#tab" + id + " #AccessList" + id).val(JSON.stringify(data));

      $.ajax({
        url:
          "http://" + ip + "/EAkimat/Documents.Resolution.NewExecuterList",

        method: "POST",
        data: JSON.stringify({
          resolutionId: obj.id,
          taskId: obj.Tasks[id].id,
          executerDataList: {
            __type: "DataList:#Avrora.Objects.Web",
            Data: data
          }
        }),
        beforeSend: function(xhr) {
          xhr.setRequestHeader("content-type", "application/json");
          xhr.setRequestHeader("Token", token);
        },
        success: function(data2) {
          $("#tab" + id + " #NewExecuterList" + id).val(
            JSON.stringify(data2.Data.Object)
          );
          var nummpp = 0;
          var tr = "";
          $.each(data2.Data.Object, function(k, v) {
            nummpp++;
            tr +=
              "<tr>" +
              "<td>" +
              nummpp +
              "</td>" +
              "<td>" +
              v.ExecuterName +
              "</td>" +
              "<td>" +
              v.EnterpriseName +
              "</td>" +
              "<td>" +
              getDataFromRef("WorkStatus", v.WorkStatus) +
              "</td>" +
              "<td>";
            if (v.CompleteDate != null) {
              tr += DateFromMSDate(v.CompleteDate);
            }

            tr +=
              "</td>" +
              '<td><input id="' +
              v.id +
              '" value="' +
              nummpp +
              '" type="radio" ' +
              (v.Svod ? "checked" : "") +
              ' name="r"></td>' +
              "</tr>";
          });

          $("#AccessListTable" + id).html(tr);
          //console.log( tr)
        }
      });

      // $('#AccessList'+id).val(JSON.stringify(access));
      // $('#AccessListTable'+id).html(tr);
    });
  }
  function getTaskData(task, type, f) {
    if (task[type] != null) {
      if (type == "ControlType") {
        return task[type].id;
      } else if (type == "PeriodEndDate" || type == "InnerLimit") {
        return DateFromMSDateForForm(task[type]);
      } else if (type == "WorkStatus") {
        return getDataFromRef("WorkStatus", task[type]);
      } else if (type == "ControlPriority") {
        return task[type].id;
      } else if (type == "Executers") {
        if (f == "0401022") {
          if (task[type].length != 0) {
            var tr = "";
            var nummpp = 0;

            $.each(task[type], function(k, v) {
              nummpp++;
              tr +=
                "<tr>" +
                "<td>" +
                v.ExecuterName +
                "</td>" +
                "<td>" +
                v.EnterpriseName +
                "</td>";

              tr +=
                '<td><input id="' +
                v.id +
                '" value="' +
                nummpp +
                '" type="radio" ' +
                (v.Svod ? "checked" : "") +
                ' name="r"></td>' +
                "</tr>";
            });

            return tr;
          } else {
            return "";
          }
        } else {
          if (task[type].length != 0) {
            var tr = "";
            var nummpp = 0;

            $.each(task[type], function(k, v) {
              nummpp++;
              tr +=
                "<tr>" +
                "<td>" +
                nummpp +
                "</td>" +
                "<td>" +
                v.ExecuterName +
                "</td>" +
                "<td>" +
                v.EnterpriseName +
                "</td>" +
                "<td>" +
                getDataFromRef("WorkStatus", v.WorkStatus) +
                "</td>" +
                "<td>";
              if (v.CompleteDate != null) {
                tr += DateFromMSDate(v.CompleteDate);
              }

              tr +=
                "</td>" +
                '<td><input id="' +
                v.id +
                '" value="' +
                nummpp +
                '" type="radio" ' +
                (v.Svod ? "checked" : "") +
                ' name="r"></td>' +
                "</tr>";
            });

            return tr;
          } else {
            return "";
          }
        }
      } else {
        return task[type];
      }
    } else {
      return "";
    }
  }
}

function TaskParseView(key, val, obj, type) {
    var AL2Links = "";
    var AL2Tabs = "";
  if (type == "0401021") {
    AL2Links = "";
    AL2Tabs = "";
    var active = "";
    if (obj.ActiveTask) {
      if (val.id == obj.ActiveTask.id) {
        active = "active";
      }
    } else if (val.Index == 0) active = "active";

    if (val.TaskNumber == "") {
      var text = $('a[role="tab"]').text();
      var spl = text.split(" ");

      val.TaskNumber = Number(spl[spl.length - 1]) + 1;
    }
    AL2Links = '<li data-id="' + val.id + '" role="presentation" class="' + active + '">' + '<a aria-expanded="true" href="#tab' + key + '" data-id="' + key + '" aria-controls="tab' +
      key + '" role="tab" data-toggle="tab">Задача</a>' + "</li>";
    AL2Tabs = '<div id="tab' + key + '" data-id="' + key + '" class="tab-pane ' + active +  '" role="tabpanel">' +
      '<div class="col-xs-12 col-sm-12 col-lg-12 col-md-12">' +
      '<label class="required">Текст поручения</label>' +
      '<div id="ResolutionText" >' +
      getTaskData(val, "ResolutionText") +
      "&nbsp;</div>" + "</div>" +
      '<div class="col-xs-12 col-sm-12 col-lg-6 col-md-6">' +
      '<label class="required">Тип контроля </label>' +
      '<div id="ControlType">' +
      getTaskData(val, "ControlType") +
      "&nbsp;</div>" + "</div>" +
      '<div class="col-xs-12 col-sm-12 col-lg-6 col-md-6">' +
      '<label class="required">Контрольный срок</label>' +
      '<div id="InnerLimit">' +
      getTaskData(val, "InnerLimit") +
      "&nbsp;</div>" + "</div>" +
      '<div class="col-xs-12 col-sm-12 col-lg-6 col-md-6">' +
      '<label class="required">Периодичность</label>' +
      '<div id="Periodicity" >' +
      getTaskData(val, "Periodicity") +
      "&nbsp;</div>" +
      "</div>" +
      '<div class="col-xs-12 col-sm-12 col-lg-6 col-md-6">' +
      '<label class="required">Конец периода</label>' +
      '<div id="PeriodEndDate">' +
      getTaskData(val, "PeriodEndDate") +
      "&nbsp;</div>" +
      "</div>" +
      '<div class="col-xs-12 col-sm-12 col-lg-6 col-md-6">' +
      '<label class="required">Приоритет</label>' +
      '<div id="ControlPriority">' +
      getTaskData(val, "ControlPriority") +
      "&nbsp;</div>" +
      "</div>" +
      '<div class="col-xs-12 col-sm-12 col-lg-6 col-md-6">' +
      '<label class="required">Контролирующий</label>' +
      '<div  id="ControlerName">' +
      getTaskData(val, "ControlerName") +
      "&nbsp;</div>" +
      "</div>" +
      '<div class="col-xs-12 col-sm-12 col-lg-6 col-md-6">' +
      '<label class="required">Статус</label>' +
      '<div id="WorkStatus">' +
      getTaskData(val, "WorkStatus") +
      "&nbsp;</div>" +
      "</div>" +
      '<div class="col-xs-12 col-sm-12 col-lg-6 col-md-6" style="">' +
      '<label class="required">Личный контроль</label>' +
      '<input type="checkbox" ' +
      (val.IsSignerControl ? "checked" : "") +
      ' id="IsSignerControl" disabled>' +
      "</div>" +
      '<div class="col-xs-12">' +
      '<label class="required">Исполнители</label>' +
      '<div class="doclist">' +
      '<table id="table-task" class="table table-bordered no-sorted table-hover">' +
      "<thead>" +
      "<th>ИСПОЛНИТЕЛЬ</th>" +
      "<th>ОРГАНИЗАЦИЯ</th>" +
      "<th>СТАТУС</th>" +
      "<th>СВОД</th>" +
      "</thead>" +
      '<tbody id="AccessListTable' +
      key +
      '">' +
      getTaskData(val, "Executers", "0401021") +
      "</tbody>" +
      "</table>" +
      "</div>" +
      "</div>" +
      "</div>";
    // AccessList2(val.SelectedEmsObjectsId, key);
    $(".AL2Links").append(AL2Links);
    $(".AL2Tabs").append(AL2Tabs);
  } else if (type == "0701031") {

    AL2Tabs = "";
    if (obj.ActiveTask) {
      if (val.id == obj.ActiveTask.id) {
        active = "active";
      }
    } else if (val.Index == 0) active = "active";

    if (val.TaskNumber == "") {
      var text = $('a[role="tab"]').text();
      var spl = text.split(" ");

      val.TaskNumber = Number(spl[spl.length - 1]) + 1;
    }
    AL2Tabs = '<div class="section row">'+
    '<div class="secttitle col-xs-12">Пункт</div>'+
    '<div class="col-md-6">' +
      '<label class="required">№ пункта</label>' +
      '<div id="TaskNumber" >' +
      getTaskData(val, "TaskNumber") +
      "&nbsp;</div>" +
      "</div>" +
      '<div class="col-md-6">' +
      '<label class="required">Дата создания </label>' +
      '<div id="ResolutionDate">' +
      DateFromMSDateForForm(obj.ResolutionDate) +
      "&nbsp;</div>" +
      "</div>" +
      '<div class="col-xs-12 col-sm-12 col-lg-12 col-md-12">' +
      '<label class="required">Содержание пункта</label>' +
      '<div id="ResolutionText">' +
      getTaskData(val, "ResolutionText") +
      "&nbsp;</div>" +
      "</div>" +
      '<div class="col-xs-12 col-sm-12 col-lg-6 col-md-6" style="">' +
      '<label class="required">Личный контроль</label>' +
      '<input type="checkbox" ' +
      (val.IsSignerControl ? "checked" : "") +
      ' id="IsSignerControl" disabled>' +
      "</div>" +
      '<div class="col-xs-12 col-sm-12 col-lg-6 col-md-6">' +
      '<label class="required">Тип контроля</label>' +
      '<div id="ControlType">' +
      getTaskData(val, "ControlType") +
      "&nbsp;</div>" +
      "</div>" +
      '<div class="col-xs-12 col-sm-12 col-lg-12 col-md-12">' +
      '<label class="required">Первоначальный срок</label>' +
      '<div id="InnerLimit">' +
      getTaskData(val, "InnerLimit") +
      "&nbsp;</div>" +
      "</div>" +
      '<div class="col-xs-12 col-sm-12 col-lg-6 col-md-6">' +
      '<label class="required">Продление сроков</label>' +
      '<div  id="ProlongationDate">' +
      getTaskData(val, "ProlongationDate") +
      "&nbsp;</div>" +
      "</div>" +
      '<div class="col-xs-12 col-sm-12 col-lg-6 col-md-6">' +
      '<label class="required">Срок для соисполнителей</label>' +
      '<div id="OuterLimit">' +
      getTaskData(val, "OuterLimit") +
      "&nbsp;</div>" +
      "</div>" +
      '<div class="col-xs-12 col-sm-12 col-lg-12 col-md-12">'+
                    '<div class="row">'+
                        '<div class="col-md-6">'+
                            '<label class="required">Периодичность</label>'+
                            '<div id="Periodicity">'+getTaskData(val, "Periodicity") +'</div>'+
                        '</div>'+
                        '<div class="col-md-6">'+
                            '<label class="required">Конец периода</label>'+
                            '<div id="PeriodEndDate">'+getTaskData(val, "PeriodEndDate") +'</div>'+
                        '</div>'+
                    '</div>'+
                '</div>'+
                '<div class="col-xs-12 col-sm-12 col-lg-12 col-md-12">'+
                    '<div class="row">'+
                        '<div class="col-md-6">'+
                            '<label class="required">Степень сложности</label>'+
                            '<div id="Difficulty">'+getTaskData(val, "Difficulty") +'</div>'+
                        '</div>'+
                        '<div class="col-md-6">'+
                            '<label class="required">Приоритет</label>'+
                            '<div id="ControlPriority">'+getTaskData(val, "ControlPriority") +'</div>'+
                        '</div>'+
                    '</div>'+
                '</div>'+
                '<div class="col-xs-12 col-sm-12 col-lg-12 col-md-12">'+
                    '<label class="required">Ответственный в аппарате акима</label>'+
                    '<div id="ControlerName">'+getTaskData(val, "ControlerName") +'</div>'+
                '</div>'+
      '<div class="col-xs-12">' +
      '<label class="required">Исполнители</label>' +
      '<div class="doclist">' +
      '<table id="table-task" class="table table-bordered no-sorted table-hover">' +
      "<thead>" +
      "<th>ИСПОЛНИТЕЛЬ</th>" +
      "<th>ОРГАНИЗАЦИЯ</th>" +
      "<th>СТАТУС</th>" +
      "<th>СВОД</th>" +
      "</thead>" +
      '<tbody id="AccessListTable' +
      key +
      '">' +
      getTaskData(val, "Executers", "0401021") +
      "</tbody>" +
      "</table>" +
      "</div>" +
      "</div>" +
      "</div>";
    $(".AL2Tabs").append(AL2Tabs);
  } else {
    var AL2Links = "";
    var AL2Tabs = "";
    var active = "";
    if (val.id == obj.ActiveTask.id) {
      active = "active";
    }
    if (val.TaskNumber == "") {
      var text = $('a[role="tab"]').text();
      var spl = text.split(" ");

      val.TaskNumber = Number(spl[spl.length - 1]) + 1;
    }
    AL2Links =
      '<li data-id="' +
      val.id +
      '" role="presentation" class="' +
      active +
      '">' +
      '<a aria-expanded="true" href="#tab' +
      key +
      '" data-id="' +
      key +
      '" aria-controls="tab' +
      key +
      '" role="tab" data-toggle="tab">Пункт ' +
      val.TaskNumber +
      "</a>" +
      "</li>";
    AL2Tabs = '<div id="tab' + key + '" data-id="' +  key +'" class="tab-pane ' + active + '" role="tabpanel"><div class="section row">' +
      '<div class="col-xs-12 col-sm-12 col-lg-12 col-md-12">' +
      '<label class="required">Текст поручения</label>' +
      '<div id="ResolutionText" >' +
      getTaskData(val, "ResolutionText") +
      "&nbsp;</div>" +
      "</div>" +
      '<div class="col-xs-12 col-sm-12 col-lg-6 col-md-6">' +
      '<label class="required">Тип контроля </label>' +
      '<div id="ControlType">' +
      getTaskData(val, "ControlType") +
      "&nbsp;</div>" +
      "</div>" +
      '<div class="col-xs-12 col-sm-12 col-lg-6 col-md-6">' +
      '<label class="required">Контролирующий</label>' +
      '<div  id="ControlerName">' +
      getTaskData(val, "ControlerName") +
      "&nbsp;</div>" +
      "</div>" +
      '<div class="col-xs-12 col-sm-12 col-lg-6 col-md-6">' +
      '<label class="required">Периодичность</label>' +
      '<div id="Periodicity" >' +
      getTaskData(val, "Periodicity") +
      "&nbsp;</div>" +
      "</div>" +
      '<div class="col-xs-12 col-sm-12 col-lg-6 col-md-6">' +
      '<label class="required">Конец периода</label>' +
      '<div id="PeriodEndDate">' +
      getTaskData(val, "PeriodEndDate") +
      "&nbsp;</div>" +
      "</div>" +
      '<div class="col-xs-12 col-sm-12 col-lg-6 col-md-6">' +
      '<label class="required">Контрольный срок</label>' +
      '<div id="InnerLimit">' +
      getTaskData(val, "InnerLimit") +
      "&nbsp;</div>" +
      "</div>" +
      '<div class="col-xs-12 col-sm-12 col-lg-6 col-md-6">' +
      '<label class="required">Сроки продлений</label>' +
      '<div id="ProlongationDateList">' +
      getTaskData(val, "ProlongationDateList") +
      "&nbsp;</div>" +
      "</div>" +
      '<div class="col-xs-12 col-sm-12 col-lg-6 col-md-6">' +
      '<label class="required">Приоритет</label>' +
      '<div id="ControlPriority">' +
      getTaskData(val, "ControlPriority") +
      "&nbsp;</div>" +
      "</div>" +
      '<div class="col-xs-12 col-sm-12 col-lg-6 col-md-6" style="">' +
      '<label class="required">Личный контроль</label>' +
      '<input type="checkbox" ' +
      (val.IsSignerControl ? "checked" : "") +
      ' id="IsSignerControl" disabled>' +
      "</div>" +
      '<div class="col-xs-12 col-sm-12 col-lg-6 col-md-6">' +
      '<label class="required">Статус</label>' +
      '<div id="WorkStatus">' +
      getTaskData(val, "WorkStatus") +
      "&nbsp;</div>" +
      "</div>" +
      '<div class="col-xs-12">' +
      '<div style="margin-bottom: 15px;">' +
      '<label class="required" style="float: left;display: block; margin-right: 20px;width: auto;">Исполнители</label>' +
      '<input type="hidden" id="AccessList' +
      key +
      '" class="AccessList2" name="AccessList' +
      key +
      '" required>' +
      '<input type="hidden" id="NewExecuterList' +
      key +
      '" required>' +
      '<div style="clear: both;"></div>' +
      "</div>" +
      '<div class="modal" id="access-list' +
      key +
      '" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">' +
      '<div class="modal-dialog modal-lg" role="document">' +
      '<div class="modal-content">' +
      '<div class="modal-header">' +
      '<div class="title">Исполнители</div>' +
      '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times-rectangle"></i></button>' +
      "</div>" +
      '<div class="modal-body">' +
      '<div class="row">' +
      '<div class="col-xs-6">' +
      '<select class="w100 form-control access-select' +
      key +
      '" data-key="' +
      key +
      '" required id="access-select" size="10" data-placeholder="Выберите значение..."></select>' +
      "</div>" +
      '<div class="col-xs-6">' +
      '<select class="w100 form-control access-view' +
      key +
      '" data-key="' +
      key +
      '" required id="access-view" size="10" data-placeholder="Выберите значение..."></select>' +
      "</div>" +
      "</div>" +
      "</div>" +
      '<div class="formsep"></div>' +
      '<div class="modal-footer"> ' +
      '<div class="buttons"><a class="save save-access' +
      key +
      '" data-key="' +
      key +
      '" data-dismiss="modal">ОТПРАВИТЬ</a><a class="cancel" data-dismiss="modal">Отмена</a></div>' +
      "</div>" +
      "</div>" +
      "</div>" +
      "</div>" +
      "</div>" +
      '<div class="">' +
      '<div class="col-xs-12 col-sm-12">' +
      '<table id="table-task" class="table table-bordered no-sorted table-hover">' +
      "<thead>" +
      "<th>№ П/П</th>" +
      "<th>ИСПОЛНИТЕЛЬ</th>" +
      "<th>ОРГАНИЗАЦИЯ</th>" +
      "<th>СТАТУС</th>" +
      "<th>ДАТА ИСПОЛНЕНИЯ</th>" +
      "<th>СВОД</th>" +
      "</thead>" +
      '<tbody id="AccessListTable' +
      key +
      '">' +
      getTaskData(val, "Executers") +
      "</tbody>" +
      "</table>" +
      "</div>" +
      "</div></div>" +
      "</div>";
    // AccessList2(val.SelectedEmsObjectsId, key);
    $(".AL2Links").append(AL2Links);
    $(".AL2Tabs").append(AL2Tabs);
  }

  // taskState = jQuery.extend(true, {}, obj.Tasks);

  function getTaskData(task, type, f) {
    if (type == "ControlType") {
      if (task[type]) {
        return task[type].Value;
      } else {
        return "<i>не заполнено</i>";
      }
    } else if (type == "ControlPriority") {
      if (task[type]) {
        return task[type].Value;
      } else {
        return "<i>не заполнено</i>";
      }
    } else if (type == "PeriodEndDate" || type == "InnerLimit" || type == "ProlongationDate" || type == 'OuterLimit') {
      if (task[type]) {
        return DateFromMSDateForForm(task[type]);
      } else {
        return "<i>не заполнено</i>";
      }
    } else if (type == "Periodicity") {
      if (task[type]) {
        return getDataFromRef("ResolutionPeriods", task[type]);
      } else {
        return "<i>не заполнено</i>";
      }
    } else if (type == "WorkStatus") {
      return getDataFromRef("WorkStatus", task[type]);
    } else if (type == "Executers") {
      if (f == "0401021") {
        if (task[type].length != 0) {
          var tr = "";
          var nummpp = 0;

          $.each(task[type], function(k, v) {
            nummpp++;
            tr +=
              '<tr data-id="' +
              v.id +
              '">' +
              "<td>" +
              v.ExecuterName +
              "</td>" +
              "<td>" +
              v.EnterpriseName +
              "</td>" +
              "<td>" +
              getDataFromRef("WorkStatus", v.WorkStatus) +
              "</td>";

            tr +=
              '<td><input value="' +
              nummpp +
              '" type="radio" ' +
              (v.Svod ? "checked" : "") +
              ' disabled name="r"></td>' +
              "</tr>";
          });

          return tr;
        } else {
          return "";
        }
      } else {
        if (task[type].length != 0) {
          var tr = "";
          var nummpp = 0;

          $.each(task[type], function(k, v) {
            nummpp++;
            tr +=
              '<tr data-id="' +
              v.id +
              '">' +
              "<td>" +
              nummpp +
              "</td>" +
              "<td>" +
              v.ExecuterName +
              "</td>" +
              "<td>" +
              v.EnterpriseName +
              "</td>" +
              "<td>" +
              getDataFromRef("WorkStatus", v.WorkStatus) +
              "</td>" +
              "<td>";
            if (v.CompleteDate != null) {
              tr += DateFromMSDate(v.CompleteDate);
            }

            tr +=
              "</td>" +
              '<td><input value="' +
              nummpp +
              '" type="radio" ' +
              (v.Svod ? "checked" : "") +
              ' disabled name="r"></td>' +
              "</tr>";
          });

          return tr;
        } else {
          return "";
        }
      }
    } else {
      if (task[type]) {
        return task[type];
      } else {
        return "<i>не заполнено</i>";
      }
    }
  }
}

function check_validate_rules(item) {
  if (!item) return false;
  var elem = document.getElementById(item.id);
  if (!elem) return false;
  for (var key in item.rules) {
    switch (key) {
      case "required":
        if (elem.value == "") return false;
        break;
      case "regex":
        break;
    }
  }
}

function main_valid(options) {
  var done = options.done;
  var error = options.error;
  options.fields.map(function(item) {
    if (check_validate_rules(item)) {
      done();
    } else {
      error();
    }
  });
}

function addTab(item, container, container_content, token) {

  var static_pages = ['recInfo', 'tab1', 'tab0'];
  var loaded_pages = [];
  
  item.forEach(function(i, v){

    var arr = i.FormId.split('.');
    var tabname = '';
    if (arr.length > 1) {
      tabname = arr[1];
    } else {
      tabname = arr[0];
    }

    $(container).append('<li role="presentation"><a aria-expanded="true" href="#'+tabname+'" aria-controls="'+tabname+'" role="tab" data-toggle="tab">'+i.Name+'</a></li>')
  }) 

  $('body').on('click', container+' a', function(){
  
    var action = $(this).attr('aria-controls')
    
     if (!static_pages.includes(action) && !loaded_pages.includes(action)) {
  
      $.ajax({
        "url": "http://" + ip + "/EAkimat/"+$('#ServiceCode').val()+"/Statement."+action+"?id="+$('#id').val(),
        cache: false,
        async:false,
        beforeSend: function (xhr) {
          xhr.setRequestHeader("content-type", "application/json");
          xhr.setRequestHeader("Token", token);
        },
        success: function (data) {
  
          loaded_pages.push(action);
          View(data, container_content);
  
        }
      });
     }
    
  })

}



function Loader(data) {
  
  $('body').append('<div class="eclipse"></div>');

  setTimeout(function(){

    $('.eclipse').remove();
    console.log(data);

  }, 4000, data)

}
