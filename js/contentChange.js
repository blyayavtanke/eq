$( document ).ajaxComplete(function( event, request, settings ) {
   // Инициализация air-datepicker
   $('.air-datepicker').datepicker();
     
   //Скрываем панель действий если она не имеет элементов
   if($(".panel-rib li").length > 0){
        $(".ribbon").show();
        $(".contentCard").removeClass("contentCard-noribbon");
   }else{
        $(".ribbon").hide();
        $(".contentCard").addClass("contentCard-noribbon");
   } 
   //Решаем проблему с позиционированием модального окна относительно полупрозрачной обложки
//    $('.modal-dialog').parent().on('show.bs.modal', function(e){ $(e.relatedTarget.attributes['data-target'].value).appendTo('body'); });

   //Гриды на главной одной высоты при загрузке вьюшки
   oneHeightToBox(".main_widgets .widget", null);

   /*if($('body').is('input[type="date"]')){
     $('input[type="date"]').each(function(){
          var airObj = {dateFormat: "dd.mm.yyyy"};
          if($(this).attr("min") && $(this).attr("min") != ""){
               airObj['minDate'] = $(this).attr("min");
          }
          if($(this).attr("max") && $(this).attr("max") != ""){
               airObj['maxDate'] = $(this).attr("max");
          }
          if($(this).val() && $(this).val() != ""){
               airObj['startDate'] = $(this).val();
          }
          $(this).attr('type', 'text');
          $(this).datepicker(airObj);
     });
   }*/
});
//Гриды на главной одной высоты при загрузке окна и ресайзе
$(document).ready(function(){
     if(window.innerWidth < 1400){
          oneHeightToBox(".main_widgets .widget", 130);
          $('.item6 a').css("height", $('.item1').height()+"px");
     }else{
          oneHeightToBox(".main_widgets .widget", null);
          $('.item6 a').css("height", $('.item1').height()+"px");
     }

     
});


$(window).on('resize', function(){
     // oneHeightToBox(".main_widgets .widget", null);
});

function oneHeightToBox(boxName, minHeight){
     var mainDivs = $(boxName);
     
     var maxHeight = 0;
     for (var i = 0; i < mainDivs.length; ++i) {
          $(mainDivs[i]).css("height", "auto");
          
     }
     for (var i = 0; i < mainDivs.length; ++i) {
          if (maxHeight < $(mainDivs[i]).height()) { 
               maxHeight = $(mainDivs[i]).height(); 
               
          }
          
     }
     for (var i = 0; i < mainDivs.length; ++i) {
          if(minHeight != null){
               $(mainDivs[i]).height(minHeight);     
          }else{
               $(mainDivs[i]).height(maxHeight);
               if ($(mainDivs[i]).hasClass('item6')) {
                    // $(mainDivs[i]).height(maxHeight*2);
                    $(mainDivs[i]).find('a').height(maxHeight/3);
               }
               
          }
     }
}