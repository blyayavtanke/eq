var ip = '192.168.1.51:8081'; 
var site_url = 'eq.integro'; 
// var service = '00403010';

var items = [{action:"index", title:"Главная"}];

function genBreads(data) {

	var count = data.length;
	var str2 = '';

	for (var i = 0; i <= count - 1; i++) {

		var ind = i + 1;
		if (ind != count) {
			str2 += '<li class="modules"><a data-action="' + data[i].action + '" data-service="' + data[i].service + '">' + data[i].title + '</a></li>';
		} else {
			str2 += '<li class="active">' +
				'<table width="100%">' +
				'<tr>' +
				'<td align="left" valign="middle">' + data[i].title + '</td>' +
				
				'</tr>' +
				'</table>' +
				'</li>';
		}
	}
	var str = '<ul>' + str2 + '</ul>';
	$('.breadcrumbs').html(str);
}

function breads() {

	$('body').on('click', '#leftmenu .childmenu li a', function () {
		items = [];
		items[0] = {
			"title": $(this).parents('.panel').find('.grouptitle').text(),
			"action": $(this).parents('.panel').find('.grouptitle').data("action")
		};
		if ($(this).text() != 'Главная')
		items[1] = {
			"title": $(this).text(),
			"action": $(this).data("action"),
			"service": $(this).data("service")
		};

		if (items.length == 3) {
			items.pop();
		}

		genBreads(items);

	});
    $(document).on('click','.ribbon .ribbonBtn', function() {
        var item = $(this);
		var action = item.data('action');
		var text = '';
		if(action == "Close"){
			text = item.data('closeaftertext');	
		}else{
			text = item.text();
		}

        items[2] = {
            "title": text,
            "action": action
        };

        genBreads(items);
    });
	$(document).on('click', '.itemtitle .ribbonBtn, .main_widgets .ribbonBtn', function () {

		item = $(this).data('action');
		item = $( '#leftmenu .childmenu li a[data-action='+item+']');
		items[0] = {
			"title": item.parents('.panel').find('.grouptitle').text(),
			"action": item.parents('.panel').find('.grouptitle').data("action")
		};
		items[1] = {
			"title": item.text(),
			"action": item.data("action")
		};

		if (items.length == 3) {
			items.pop();
		}

		genBreads(items);

	});

	

	$(document).on('click', '.ibig .ribbonBtn2', function () {

		action = $(this).data('action');
		service = $(this).data('service');
		count = $(this).data('count');
		// items[0] = {
		// 	"title": item.parents('.panel').find('.grouptitle').text(),
		// 	"action": item.parents('.panel').find('.grouptitle').data("action")
		// };
		// items[1] = { 
		// 	"title": item.text(),
		// 	"action": item.data("action")
		// };
		if (count != 0) {
			items.push({
				"title": service,
				"action": action,
				"service": service
			});

		if (items.length == 3) {
			items.pop();
		}

		genBreads(items);
		}
		

	});

	$(document).on('click', '.actionBtnList', function () {
		if ($(this).data("title")) {
			items[2] = {
				"title": $(this).data("title")
			};
			genBreads(items);
		}

    });
    
    
    $(document).on('click', '.item .brd', function () {
		if ($(this).data("title")) {
			items[1] = {
				"title": $(this).data("title")
			};
			genBreads(items);
		}

	});

}

$(function () {

	breads();
        
    $(document).on('click','.breadcrumbs .back',function() {
        if ($('.breadcrumbs ul').children().length > 2) {
            $('.breadcrumbs ul').children().last().prev().children().click();
        }
    });

	$('body').on('click', '.breadcrumbs li.modules a', function () {

		// var sel_bread = $(this).text();
		var sel_bread = $(this).data('action');
		var sel_service = $(this).data('service');
		var sel_key;

		$.each(items, function (k, v) {

			// if (sel_bread == v.title) {
			if (sel_bread == v.action) {
				sel_key = k;
			}

		});

		items.splice(sel_key + 1);
		var view = $(this).data('action');

		$.ajax({
			url: "views/" + view + ".html",
			cache: false,
			success: function (data) {
				$('.forcontent').html(data);
				$('#service').val(sel_service);
				$('#leftmenu .childmenu .active').removeClass('active');
				$('#leftmenu .childmenu a[data-action='+view+']').parent().addClass('active');
				genBreads(items);

			}

		});

	});

});