$.ajaxSetup({
    async:false,
    xhrFields: {
      withCredentials: false
    }
  });

function View(data, container) {
    var token = $.session.get('token');
    var action2 = data.Data.Action;
    openedObjed = data;
    open_item = data;
    
    if (data.Result == 'OK') {

        if (container == '') {
            $('.forcontent').html('');
        }

        $.ajax({
            url: "views/" + data.Data.FormId + ".html",
            async:false,
            cache: false,
            success: function (view) {

                $('.forcontent ' + container).append(view);
                Ribbon(data, '.ribbon');
                genBreads(items);

                $('.ribbon').off('click','.ribbonBtn').on('click', '.ribbonBtn', function () {

                    var action = $(this).data('action');
                    var type = $(this).data('type');
                        
                    openedObjed = data;

                    if (action == 'StartProcess') {
                        
                        $.ajax({

                            url: "http://"+ip+"/EAkimat/"+$('#ServiceCode').val()+"/Statement.StartProcess",
                            xhrFields: {
                              withCredentials: false
                           },
                            data:JSON.stringify({"id":$('#id').val()}),
                           method:'post',
                            async:true,
                            cache: false,
                            beforeSend: function(xhr){
                             xhr.setRequestHeader("content-type", "application/json");
                             xhr.setRequestHeader("Token", token);
                             $('body').append('<div class="eclipse"></div>');
                           },
                            
                             success:function(data) {
                                if (data.Result == 'OK') {
                                    $.notify(data.Message, {
                                        type: 'success'
                                    });
                                    
                                    View(data, "");
                                    $('.eclipse').remove();
                                } else {
                                    $.notify(data.Message, {
                                        type: 'danger'
                                    });
                                    $('.eclipse').remove();
                                }
                                // Loader(data);
                             }
                        
                          });

                    } else if (action == 'SetExecuter') {

                        $.ajax({
                            url: 'http://' + ip + '/EAkimat/SelectEmsObjects',
                            method: 'POST',
                            data: JSON.stringify({
                                "param": {
                                    "SelectMode": 1,
                                    "ShowGroups": false,
                                    "ShowRoles": false,
                                    "ShowWorkPlace": true
                                },
                                "securityParam": {
                                    "Locked": false,
                                    "HasRule": 5305
                                }
                            }),
                            xhrFields: {
                                withCredentials: true
                            },
                            beforeSend: function (xhr) {
                                xhr.setRequestHeader("content-type", "application/json");
                                xhr.setRequestHeader("Token", token);
                            },
                            success: function (data) {
                                $('body').append('<div class="modal" id="workplace" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">' +
                                    '<div class="modal-dialog" role="document">' +
                                    '<div class="modal-content">' +
                                    '<div class="modal-header">' +
                                    '<div class="title">Выбор сотрудника</div>' +
                                    '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times-rectangle"></i></button>' +
                                    '</div>' +
                                    '<div class="modal-body">' +
                                    '<div class="form-group">' +
                                    '<label class="required">Сотрудник</label>' +
                                    '<select class="w100 form-control" required  id="WorkplaceList" name="" data-placeholder="Выберите значение..."></select>' +
                                    '</div>' +
                                    '</div>' +
                                    '<div class="formsep"></div>' +
                                    '<div class="modal-footer">' +
                                    '<div class="buttons"><a class="save save-workplace">ОТПРАВИТЬ</a><a class="cancel" data-dismiss="modal">Отмена</a></div>' +
                                    '</div>' +
                                    '</div>' +
                                    '</div>' +
                                    '</div>');
                                $('#workplace').modal('show');
                                $(data.Data.Object.Data).each(function (ind, value) {
                                    $('#WorkplaceList').append('<option value=' + value[0] + '>' + value[1] + '</tr>');
                                });
                                $('#WorkplaceList').chosen();

                                $(document).off('click', '.save-workplace').on('click', '.save-workplace', function (e) {
                                    $('body').append('<div class="eclipse"></div>');
                                    if ($('#WorkplaceList').val()) {
                                        $.ajax({
                                            url: 'http://' + ip + '/EAkimat/'+$('#ServiceCode').val()+'/Statement.SetExecuter',
                                            method: 'POST',
                                            data: JSON.stringify({
                                                id: $('#id').val(),
                                                workPlaceId: $('#WorkplaceList').val()
                                            }),
                                            xhrFields: {
                                                withCredentials: true
                                            },
                                            beforeSend: function (xhr) {
                                                xhr.setRequestHeader("content-type", "application/json");
                                                xhr.setRequestHeader("Token", token);
                                                
                                            },
                                            success: function (data) {
                                                if (data.Result == 'OK') {
                                                    $.notify(data.Message, {
                                                        type: 'success'
                                                    });
                                                    $('#workplace').modal('hide');
                                                    $('#workplace').remove();
                                                    View(data, "");
                                                    $('.eclipse').remove();
                                                } else {
                                                    $.notify(data.Message, {
                                                        type: 'danger'
                                                    });
                                                    $('#workplace').modal('hide');
                                                    $('#workplace').remove();
                                                    $('.eclipse').remove();
                                                }
                                            }
                                        });
                                    }
                                });
                            }
                        });

                    } else if (action == "SendToSign") {

                        $.ajax({
                            url: 'http://' + ip + '/EAkimat/SelectEmsObjects',
                            method: 'POST',
                            data: JSON.stringify({
                                "param": {
                                    "SelectMode": 1,
                                    "ShowGroups": false,
                                    "ShowRoles": false,
                                    "ShowWorkPlace": true
                                },
                                "securityParam": {
                                    "Locked": false,
                                    "HasRule": 5305
                                }
                            }),
                            xhrFields: {
                                withCredentials: true
                            },
                            beforeSend: function (xhr) {
                                xhr.setRequestHeader("content-type", "application/json");
                                xhr.setRequestHeader("Token", token);
                            },
                            success: function (data) {
                                $('body').append('<div class="modal" id="workplace" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">' +
                                    '<div class="modal-dialog" role="document">' +
                                    '<div class="modal-content">' +
                                    '<div class="modal-header">' +
                                    '<div class="title">Выбор сотрудника</div>' +
                                    '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times-rectangle"></i></button>' +
                                    '</div>' +
                                    '<div class="modal-body">' +
                                    '<div class="form-group">' +
                                    '<label class="required">Сотрудник</label>' +
                                    '<select class="w100 form-control" required  id="WorkplaceList" name="" data-placeholder="Выберите значение..."></select>' +
                                    '</div>' +
                                    '</div>' +
                                    '<div class="formsep"></div>' +
                                    '<div class="modal-footer">' +
                                    '<div class="buttons"><a class="save save-workplace">ОТПРАВИТЬ</a><a class="cancel" data-dismiss="modal">Отмена</a></div>' +
                                    '</div>' +
                                    '</div>' +
                                    '</div>' +
                                    '</div>');
                                $('#workplace').modal('show');
                                $(data.Data.Object.Data).each(function (ind, value) {
                                    $('#WorkplaceList').append('<option value=' + value[0] + '>' + value[1] + '</tr>');
                                });
                                $('#WorkplaceList').chosen();

                                $(document).off('click', '.save-workplace').on('click', '.save-workplace', function (e) {
                                    $('body').append('<div class="eclipse"></div>');
                                    if ($('#WorkplaceList').val()) {
                                        $.ajax({
                                            url: 'http://' + ip + '/EAkimat/'+$('#ServiceCode').val()+'/Statement.SendToSign',
                                            method: 'POST',
                                            data: JSON.stringify({
                                                id: $('#id').val(),
                                                workPlaceId: $('#WorkplaceList').val()
                                            }),
                                            xhrFields: {
                                                withCredentials: true
                                            },
                                            beforeSend: function (xhr) {
                                                xhr.setRequestHeader("content-type", "application/json");
                                                xhr.setRequestHeader("Token", token);
                                                
                                            },
                                            success: function (data) {
                                                if (data.Result == 'OK') {
                                                    $.notify(data.Message, {
                                                        type: 'success'
                                                    });
                                                    $('#workplace').modal('hide');
                                                    $('#workplace').remove();
                                                    View(data, "");
                                                    $('.eclipse').remove();
                                                
                                                } else {
                                                    $.notify(data.Message, {
                                                        type: 'danger'
                                                    });
                                                    $('.eclipse').remove();
                                                }
                                            }
                                        });
                                    }
                                });
                            }
                        });

                    } else if (action == 'GiveOut') {

                        $.ajax({

                            url: "http://"+ip+"/EAkimat/"+$('#ServiceCode').val()+"/Statement.GiveOut",
                            xhrFields: {
                              withCredentials: false
                           },
                            data:JSON.stringify({"id":$('#id').val()}),
                           method:'post',
                            async:true,
                            cache: false,
                            beforeSend: function(xhr){
                             xhr.setRequestHeader("content-type", "application/json");
                             xhr.setRequestHeader("Token", token);
                             $('body').append('<div class="eclipse"></div>');
                           },
                            
                             success:function(data) {
                                if (data.Result == 'OK') {
                                    $.notify(data.Message, {
                                        type: 'success'
                                    });
                                    $('.eclipse').remove();
                                    View(data, "");
                                } else {
                                    $.notify(data.Message, {
                                        type: 'danger'
                                    });
                                    $('.eclipse').remove();
                                }
                             }
                        
                          });

                    } else if (action == 'Sign') {

                        $.ajax({

                            url: "http://"+ip+"/EAkimat/"+$('#ServiceCode').val()+"/Statement.DataForSign?id="+$('#id').val(),
                            xhrFields: {
                              withCredentials: false
                           },
                            async:true,
                            cache: false,
                            beforeSend: function(xhr){
                             xhr.setRequestHeader("content-type", "application/json");
                             xhr.setRequestHeader("Token", token);
                             
                           },
                            
                             success:function(data) {
                                
                                if (data.Result == 'OK') {
                                    
                                   signXml("PKCS12", "SIGNATURE", data.Message, function(result){
                                    if (result['code'] === "500") {
                                        alert(result['message']);
                                    } else if (result['code'] === "200") {

                                        var xml = result['responseObject'];
                                        
                                        $.ajax({

                                            url: "http://"+ip+"/EAkimat/"+$('#ServiceCode').val()+"/Statement.Sign",
                                            xhrFields: {
                                              withCredentials: false
                                           },
                                            data:JSON.stringify({"id":$('#id').val(), "xml":xml}),
                                           method:'post',
                                            async:true,
                                            cache: false,
                                            beforeSend: function(xhr){
                                             xhr.setRequestHeader("content-type", "application/json");
                                             xhr.setRequestHeader("Token", token);
                                             $('body').append('<div class="eclipse"></div>');
                                           },
                                            
                                             success:function(data) {
                                                if (data.Result == 'OK') {
                                                    $.notify(data.Message, {
                                                        type: 'success'
                                                    });
                                                    $('.eclipse').remove();
                                                    View(data, "");
                                                   
                                                } else {
                                                    $.notify(data.Message, {
                                                        type: 'danger'
                                                    });
                                                    $('.eclipse').remove();
                                                }
                                             }
                                        
                                          });
                                        
                                    }
                                   });
                                   
                                } else {
                                    $.notify(data.Message, {
                                        type: 'danger'
                                    });
                                }
                             }
                        
                          });

                    } else if (action == 'Return') {

                        $.ajax({

                            url: "http://"+ip+"/EAkimat/"+$('#ServiceCode').val()+"/Statement.Return",
                            xhrFields: {
                              withCredentials: false
                           },
                            data:JSON.stringify({"id":$('#id').val()}),
                            method:'post',
                            async:true,
                            cache: false,
                            beforeSend: function(xhr){
                             xhr.setRequestHeader("content-type", "application/json");
                             xhr.setRequestHeader("Token", token);
                             $('body').append('<div class="eclipse"></div>');
                           },
                            
                             success:function(data) {
                                if (data.Result == 'OK') {
                                    $.notify(data.Message, {
                                        type: 'success'
                                    });
                                    $('.eclipse').remove();
                                    View(data, "");
                                } else {
                                    $.notify(data.Message, {
                                        type: 'danger'
                                    });
                                    $('.eclipse').remove();
                                }
                             }
                        
                          });

                    } else if (action == 'MakeAnswer') {

                        var src_to_view = '';
                        

                        if ($('#Types').val() == "electronic") {

                            src_to_view = $('#ServiceCode').val();

                        } else if ($('#Types').val() == "personal") {

                            src_to_view = $('#ServiceCode').val()+"p";

                        }

                            $.ajax({
                                url: "views/answers/"+src_to_view+".html",
                                async:false,
                                cache: false,
                                success: function (view) {
    
                                    if ($('#ServiceCode').val() != '02303009') {
                                        $('body').append('<div class="modal" id="Answer" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">' +
                                        '<div class="modal-dialog" role="document">' +
                                        '<div class="modal-content">' +
                                        '<div class="modal-header">' +
                                        '<div class="title">Сформировать ответ</div>' +
                                        '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times-rectangle"></i></button>' +
                                        '</div>' +
                                        '<div class="modal-body">' + view +
                                        '</div>' +
                                        '<div class="formsep"></div>' +
                                        '<div class="modal-footer">' +
                                        '<div class="buttons"><a class="save save-answer" data-dismiss="modal">ОТПРАВИТЬ</a><a class="cancel" data-dismiss="modal">Отмена</a></div>' +
                                        '</div>' +
                                        '</div>' +
                                        '</div>' +
                                        '</div>');
                                        $('#Answer').modal('show');

                                        $('#Answer').on('hidden.bs.modal', function (e) {
                                            $('#Answer').remove();
                                          });
                                }

                                    if ($('#ServiceCode').val() == '00803006') {
                                        $('#Answer #SchoolDocTypes').html($('#SchoolDocTypes2').val());
                                    }

                                    $('.save-answer').on('click', function(){
                                        
                                        $('body').append('<div class="eclipse"></div>');

                                        var obj = null;
                                        if ($('#ServiceCode').val() == '00403010') {

                                            obj = JSON.stringify({"id":$('#id').val(),
                                            "response":{
                                                "__type": "Response"+$('#ServiceCode').val()+":#Avrora.Objects.EAkimat.Models",
                                                "Excerpt" : 
                                                    {
                                                      "ru": $('#answer_ru').val(),
                                                      "kk": $('#answer_kz').val()
                                                    }
                                                  
                                            }});

                                        } else if ($('#ServiceCode').val() == '00705002' || 
                                        $('#ServiceCode').val() == '00705003' ||
                                        $('#ServiceCode').val() == '00705004' ||
                                        $('#ServiceCode').val() == '00705005' ||
                                        $('#ServiceCode').val() == '00402009' || 
                                        $('#ServiceCode').val() == '00402010' ||
                                        $('#ServiceCode').val() == '00403004' ||
                                        $('#ServiceCode').val() == '00403005' ||
                                        $('#ServiceCode').val() == '00403009' ||
                                        $('#ServiceCode').val() == '00403011' ||
                                        $('#ServiceCode').val() == '00704007' || 
                                        $('#ServiceCode').val() == '00801015' || 
                                        $('#ServiceCode').val() == '00803006' || 
                                        $('#ServiceCode').val() == '00803007' || 
                                        $('#ServiceCode').val() == '00803009' || 
                                        $('#ServiceCode').val() == '00803013' || 
                                        $('#ServiceCode').val() == '00803019') {

                                            obj = JSON.stringify({"id":$('#id').val(),
                                            "response":{
                                                "__type": "Response"+$('#ServiceCode').val()+":#Avrora.Objects.EAkimat.Models",
                                                "ServiceLocation" : 
                                                    {
                                                      "ru": $('#ServiceLocation_ru').val(),
                                                      "kk": $('#ServiceLocation_ru').val()
                                                    },
                                                "ServiceTerm" : 
                                                    {
                                                      "ru": $('#ServiceTerm_ru').val(),
                                                      "kk": $('#ServiceTerm_ru').val()
                                                    },
                                                "ServiceProvisionEnterprise": {
                                                        "ru": $('#ServiceProvisionEnterprise_ru').val(),
                                                        "kk": $('#ServiceProvisionEnterprise_ru').val()
                                                    }
                                            }});

                                        } else if ($('#ServiceCode').val() == '00403007') {

                                            if ($('#Types').val() == "electronic") {

                                                obj = JSON.stringify({"id":$('#id').val(),
                                                "response":{
                                                    "__type": "Response"+$('#ServiceCode').val()+":#Avrora.Objects.EAkimat.Models",
                                                    "AcademicYear": $('#AcademicYear').val(),
                                                    "ServiceProvisionEnterprise": {
                                                            "ru": $('#ServiceProvisionEnterprise_ru').val(),
                                                            "kk": $('#ServiceProvisionEnterprise_ru').val()
                                                        },
                                                        "ServiceLocation" : 
                                                    {
                                                      "ru": " ",
                                                      "kk": " "
                                                    },
                                                "ServiceTerm" : 
                                                    {
                                                      "ru": " ",
                                                      "kk": " "
                                                    },
                                                }});
                                                
                                            } else if ($('#Types').val() == "personal") {

                                                obj = JSON.stringify({"id":$('#id').val(),
                                            "response":{
                                                "__type": "Response"+$('#ServiceCode').val()+":#Avrora.Objects.EAkimat.Models",
                                                "ServiceLocation" : 
                                                    {
                                                      "ru": $('#ServiceLocation_ru').val(),
                                                      "kk": $('#ServiceLocation_ru').val()
                                                    },
                                                "ServiceTerm" : 
                                                    {
                                                      "ru": $('#ServiceTerm_ru').val(),
                                                      "kk": $('#ServiceTerm_ru').val()
                                                    },
                                                "ServiceProvisionEnterprise": {
                                                        "ru": $('#ServiceProvisionEnterprise_ru').val(),
                                                        "kk": $('#ServiceProvisionEnterprise_ru').val()
                                                    }
                                            }});

                                            }

                                        } else if ($('#ServiceCode').val() == '00403003') {
                                            
                                            if ($('#Types').val() == "electronic") {

                                                obj = JSON.stringify({"id":$('#id').val(),
                                            "response":{
                                                "__type": "Response"+$('#ServiceCode').val()+":#Avrora.Objects.EAkimat.Models",
                                                "ServiceLocation" : 
                                                    {
                                                      "ru": $('#ServiceLocation_ru').val(),
                                                      "kk": $('#ServiceLocation_ru').val()
                                                    },
                                                "ServiceTerm" : 
                                                    {
                                                      "ru": $('#ServiceTerm_ru').val(),
                                                      "kk": $('#ServiceTerm_ru').val()
                                                    },
                                                "ServiceProvisionEnterprise": {
                                                        "ru": $('#ServiceProvisionEnterprise_ru').val(),
                                                        "kk": $('#ServiceProvisionEnterprise_ru').val()
                                                    },
                                                "OrderNumber":$('#OrderNumber').val(),
                                                "OrderDate":"/Date(" + Date.parse($("#OrderDate").val()) + ")/",
                                                "ClassNumber":$('#ClassNumber').val()
                                                
                                            }});
                    
                                            } else if ($('#Types').val() == "personal") {
                    
                                                obj = JSON.stringify({"id":$('#id').val(),
                                            "response":{
                                                "__type": "Response"+$('#ServiceCode').val()+":#Avrora.Objects.EAkimat.Models",
                                                "ServiceLocation" : 
                                                    {
                                                      "ru": $('#ServiceLocation_ru').val(),
                                                      "kk": $('#ServiceLocation_ru').val()
                                                    },
                                                "ServiceTerm" : 
                                                    {
                                                      "ru": $('#ServiceTerm_ru').val(),
                                                      "kk": $('#ServiceTerm_ru').val()
                                                    },
                                                "ServiceProvisionEnterprise": {
                                                        "ru": $('#ServiceProvisionEnterprise_ru').val(),
                                                        "kk": $('#ServiceProvisionEnterprise_ru').val()
                                                    }
                                            }});
                    
                                            }

                                        } else if ($('#ServiceCode').val() == '02303009') {
                                            obj = JSON.stringify({"id":$('#id').val(),
                                            "response":{
                                                "__type": "Response"+$('#ServiceCode').val()+":#Avrora.Objects.EAkimat.Models"
                                                
                                            }});
                                        } else if ($('#ServiceCode').val() == '00404003') {

                                            if ($('#Types').val() == "electronic") {

                                                obj = JSON.stringify({"id":$('#id').val(),
                                                "response":{
                                                    "__type": "Response"+$('#ServiceCode').val()+":#Avrora.Objects.EAkimat.Models",
                                                    "ActNumber":$('#ActNumber').val(),
                                                    "BirthSertificateNumber":$('#BirthSertificateNumber').val(),
                                                    "BirthSertificateDate":"/Date(" + Date.parse($("#BirthSertificateDate").val()) + ")/",
                                                    "BirthSertificateIssue":$('#BirthSertificateIssue').val(),
                                                    "RecipientBirthDate":"/Date(" + Date.parse($("#RecipientBirthDate").val()) + ")/",
                                                    "DecisionText":{
                                                        "ru": $('#DecisionText').val(),
                                                        "kk": $('#DecisionText').val()
                                                    },
                                                    "DateOfAppointment":"/Date(" + Date.parse($("#DateOfAppointment").val()) + ")/",
                                                    "Summa":$('#Summa').val(),
                                                    "FromDate" :"/Date(" + Date.parse($("#FromDate").val()) + ")/",
                                                    "ToDate" :"/Date(" + Date.parse($("#ToDate").val()) + ")/",
                                                    "RejectionReason":{
                                                        "ru": $('#RejectionReason').val(),
                                                        "kk": $('#RejectionReason').val()
                                                    },
                                                    "ServiceLocation" : {
                                                        "ru": " ",
                                                        "kk": " "
                                                    },
                                                    "ServiceTerm" : {
                                                        "ru": " ",
                                                        "kk": " "
                                                    },
                                                    "ServiceProvisionEnterprise": {
                                                        "ru": " ",
                                                        "kk": " "
                                                    },
                                                }});

                                            } else if ($('#Types').val() == "personal") {

                                                obj = JSON.stringify({"id":$('#id').val(),
                                                "response":{
                                                    "__type": "Response"+$('#ServiceCode').val()+":#Avrora.Objects.EAkimat.Models",
                                                    "ActNumber":" ",
                                                    "BirthSertificateNumber":" ",
                                                    "BirthSertificateDate":"/Date(0)/",
                                                    "BirthSertificateIssue":" ",
                                                    "RecipientBirthDate":"/Date(0)/",
                                                    "DecisionText":{
                                                        "ru": " ",
                                                        "kk": " "
                                                    },
                                                    "DateOfAppointment":"/Date(0)/",
                                                    "Summa":" ",
                                                    "FromDate" :"/Date(0)/",
                                                    "ToDate" :"/Date(0)/",
                                                    "RejectionReason":{
                                                        "ru": " ",
                                                        "kk": " "
                                                    },
                                                    "ServiceLocation" :  {
                                                        "ru": $('#ServiceLocation_ru').val(),
                                                        "kk": $('#ServiceLocation_ru').val()
                                                      },
                                                    "ServiceTerm" : {
                                                        "ru": $('#ServiceTerm_ru').val(),
                                                        "kk": $('#ServiceTerm_ru').val()
                                                      },
                                                    "ServiceProvisionEnterprise": {
                                                        "ru": $('#ServiceProvisionEnterprise_ru').val(),
                                                        "kk": $('#ServiceProvisionEnterprise_ru').val()
                                                    },
                                                }});

                                            }
                                        } else if ($('#ServiceCode').val() == '00403008') {

                                            if ($('#Types').val() == "electronic") {

                                                obj = JSON.stringify({"id":$('#id').val(),
                                                "response":{
                                                    "__type": "Response"+$('#ServiceCode').val()+":#Avrora.Objects.EAkimat.Models",
                                                    "AcademicYear":$('#AcademicYear').val(),
                                                    "ServiceLocation" : {
                                                        "ru": " ",
                                                        "kk": " "
                                                    },
                                                    "ServiceTerm" : {
                                                        "ru": " ",
                                                        "kk": " "
                                                    },
                                                    "ServiceProvisionEnterprise": {
                                                        "ru": " ",
                                                        "kk": " "
                                                    },
                                                }});

                                            } else if ($('#Types').val() == "personal") {

                                                obj = JSON.stringify({"id":$('#id').val(),
                                                "response":{
                                                    "__type": "Response"+$('#ServiceCode').val()+":#Avrora.Objects.EAkimat.Models",
                                                    "AcademicYear":" ",
                                                    "ServiceLocation" :  {
                                                        "ru": $('#ServiceLocation_ru').val(),
                                                        "kk": $('#ServiceLocation_ru').val()
                                                      },
                                                    "ServiceTerm" : {
                                                        "ru": $('#ServiceTerm_ru').val(),
                                                        "kk": $('#ServiceTerm_ru').val()
                                                      },
                                                    "ServiceProvisionEnterprise": {
                                                        "ru": $('#ServiceProvisionEnterprise_ru').val(),
                                                        "kk": $('#ServiceProvisionEnterprise_ru').val()
                                                    },
                                                }});

                                            }

                                        } else if ($('#ServiceCode').val() == '02303010') {

                                            if ($('#Types').val() == "electronic") {

                                                obj = JSON.stringify({"id":$('#id').val(),
                                                "response":{
                                                    "__type": "Response"+$('#ServiceCode').val()+":#Avrora.Objects.EAkimat.Models",
                                                    "KindOfActivity":$('#KindOfActivity').val(),
                                                    "RegNumber":$('#RegNumber').val(),
                                                    "RegDate":"/Date(" + Date.parse($("#Answer #RegDate").val()) + ")/",
                                                    "City":$('#City').val(),
                                                    "NameMemorial":$('#NameMemorial').val(),
                                                    "AddressMemorial":$('#AddressMemorial').val(),
                                                    "ServiceLocation" : {
                                                        "ru": " ",
                                                        "kk": " "
                                                    },
                                                    "ServiceTerm" : {
                                                        "ru": " ",
                                                        "kk": " "
                                                    },
                                                    "ServiceProvisionEnterprise": {
                                                        "ru": " ",
                                                        "kk": " "
                                                    },
                                                }});

                                            } else if ($('#Types').val() == "personal") {

                                                obj = JSON.stringify({"id":$('#id').val(),
                                                "response":{
                                                    "__type": "Response"+$('#ServiceCode').val()+":#Avrora.Objects.EAkimat.Models",
                                                    "KindOfActivity":" ",
                                                    "RegNumber":" ",
                                                    "RegDate":"/Date(0)/",
                                                    "City":" ",
                                                    "NameMemorial":" ",
                                                    "AddressMemorial":" ",
                                                    "ServiceLocation" :  {
                                                        "ru": $('#ServiceLocation_ru').val(),
                                                        "kk": $('#ServiceLocation_ru').val()
                                                      },
                                                    "ServiceTerm" : {
                                                        "ru": $('#ServiceTerm_ru').val(),
                                                        "kk": $('#ServiceTerm_ru').val()
                                                      },
                                                    "ServiceProvisionEnterprise": {
                                                        "ru": $('#ServiceProvisionEnterprise_ru').val(),
                                                        "kk": $('#ServiceProvisionEnterprise_ru').val()
                                                    },
                                                }});

                                            }
                                        }

                                        $.ajax({

                                            url: "http://"+ip+"/EAkimat/"+$('#ServiceCode').val()+"/Statement.MakeAnswer",
                                            xhrFields: {
                                              withCredentials: false
                                           },
                                            data:obj,
                                            method:'post',
                                            async:true,
                                            cache: false,
                                            beforeSend: function(xhr){
                                             xhr.setRequestHeader("content-type", "application/json");
                                             xhr.setRequestHeader("Token", token);
                                           },
                                            
                                             success:function(data) {
                                                if (data.Result == 'OK') {
                                                    $.notify(data.Message, {
                                                        type: 'success'
                                                    });
                                                    $('.eclipse').remove();
                                                    View(data, "");
                                                    $('#Answer').modal('hide');
                                                } else {
                                                    $.notify(data.Message, {
                                                        type: 'danger'
                                                    });
                                                    $('.eclipse').remove();
                                                    $('#Answer').modal('hide');
                                                }
                                             }
                                        
                                          });

                                    });
    
                                }
                            });

                        
                    } else if (action == 'MakeReject') {

                        $.ajax({
                            url: "views/answers/reject.html",
                            async:false,
                            cache: false,
                            success: function (view) {

                                $('body').append('<div class="modal" id="Reject" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">' +
                                '<div class="modal-dialog" role="document">' +
                                '<div class="modal-content">' +
                                '<div class="modal-header">' +
                                '<div class="title">Мотивированный отказ</div>' +
                                '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times-rectangle"></i></button>' +
                                '</div>' +
                                '<div class="modal-body">' + view +
                                '</div>' +
                                '<div class="formsep"></div>' +
                                '<div class="modal-footer">' +
                                '<div class="buttons"><a class="save addRow">ДОБАВИТЬ</a><a class="save save-reject">ОТПРАВИТЬ</a><a class="cancel" data-dismiss="modal">Отмена</a></div>' +
                                '</div>' +
                                '</div>' +
                                '</div>' +
                                '</div>');
                                $('#Reject').modal('show');

                                $('.save-reject').on('click', function(){

                                    var arr = [];
                                    var rows = $('.tReject tbody tr');

                                    for (var i = 0; i < rows.length; i++) {
                                        arr.push({"ru":$(rows[i]).find('.reject-ru').val(), "kk": $(rows[i]).find('.reject-kz').val()});
                                    }

                                    $.ajax({

                                        url: "http://"+ip+"/EAkimat/"+$('#ServiceCode').val()+"/Statement.MakeReject",
                                        xhrFields: {
                                          withCredentials: false
                                       },
                                        data:JSON.stringify({"id":$('#id').val(),
                                                            "response":{
                                                                "ReasonsRefusal" : arr
                                                            }}),
                                        method:'post',
                                        async:true,
                                        cache: false,
                                        beforeSend: function(xhr){
                                         xhr.setRequestHeader("content-type", "application/json");
                                         xhr.setRequestHeader("Token", token);
                                         $('body').append('<div class="eclipse"></div>');
                                       },
                                        
                                         success:function(data) {
                                            if (data.Result == 'OK') {
                                                $.notify(data.Message, {
                                                    type: 'success'
                                                });
                                                $('.eclipse').remove();
                                                View(data, "");
                                                $('#Reject').modal('hide');
                                            } else {
                                                $.notify(data.Message, {
                                                    type: 'danger'
                                                });
                                                $('.eclipse').remove();
                                                $('#Reject').modal('hide');
                                            }
                                         }
                                    
                                      });

                                });

                                $('#Reject').on('hidden.bs.modal', function () {
                                    $('#Reject').remove();
                                  });

                            }
                        });

                    } else if (action == 'Refresh') {

                        $.ajax({

                            url: "http://"+ip+"/EAkimat/"+$('#ServiceCode').val()+"/Statement.Refresh",
                            xhrFields: {
                              withCredentials: false
                           },
                            data:JSON.stringify({"id":$('#id').val()}),
                            method:'post',
                            async:true,
                            cache: false,
                            beforeSend: function(xhr){
                             xhr.setRequestHeader("content-type", "application/json");
                             xhr.setRequestHeader("Token", token);
                             $('body').append('<div class="eclipse"></div>');
                           },
                            
                             success:function(data) {
                                if (data.Result == 'OK') {
                                    $.notify(data.Message, {
                                        type: 'success'
                                    });
                                    $('.eclipse').remove();
                                    View(data, "");
                                } else {
                                    $.notify(data.Message, {
                                        type: 'danger'
                                    });
                                    $('.eclipse').remove();
                                }
                             }
                        
                          });

                    }

                   
                });
                var obj = '';
                var DocumentIds = [
                    "0501021","0201011","0201021",
                    "0201031","0201041","0201051",
                    "0201061","0201071", "0701021"
                ];
                if (DocumentIds.indexOf(data.Data.FormId) != -1) {
                    obj = data.Data.Object.Document;
                    documentPages(data);
                } else {
                    obj = data.Data.Object;
                }

                if(data.Data.AdditionalObjects != null){
                    $.each(data.Data.AdditionalObjects[0], function (k, v) {
                        if (k == 'IsMinisterstvo' || k == 'IsOblOrgan' || k == 'IsRegionAkimat' || k == 'IsOblBusiness') {
                            $('#' + k).prop('checked', v);
                        }
                    });    
                }
                

                /*Заполняем формы*/
                if (data.Data.FormId == "0801021") {
                    // console.log(data);
                    tabView = Number(data.Data.FormId.substr(-4, 3));
                    if(tabView > 0){
                        $('<li role="presentation">'+
                        '<a aria-expanded="true" href="#recInfo" aria-controls="recInfo" role="tab" data-toggle="tab">' + 
                        'Заявление</a></li>').insertAfter('ul.cust-tabs li:eq(1)');
                        $('.tab-content').append('<div id="recInfo" class="tab-pane" role="tabpanel"></div>');
                        $.ajax({
                            url: 'views/'+data.Data.Object.ServiceCode+'.html',
                            method: 'get',
                            async: false,
                            cache: false,
                            success: function (data) {
                                $('#recInfo').html(data);
                            }
                        });

                    }
                    $.each(obj.Statement, function (k, v) {
                        if(obj.Statement.DeclarantType == 1){
                            $('.fiz-data').show();
                        }else if(obj.Statement.DeclarantType == 2){
                            $('.ur-data').show();
                        }

                        if(k == "RegDate"){
                            $('#' + k).val(DateFromMSDateForForm(v));
                        }else{
                            $('#' + k).val(v);
                        }
                        
                    });
                }

                $.each(obj, function (k, v) {
                    if (k == 'Pages') {
                        addTab(v, '#tabs', '.tab-content', token);
                    } else if (k == 'Data') {

                        var arr = action2.split('.');
                        var tabname = '';
                        if (arr.length > 1) {
                        tabname = arr[1];
                        } else {
                        tabname = arr[0];
                        }
                        $('.tab-content #'+tabname+" table tbody").html("");

                        v.forEach(function(i, val){

                            if (tabname == 'Documents') {
                                $('.tab-content #'+tabname+" table tbody").append('<tr><td>'+i[1]+'</td><td>'+i[2]+'</td><td>'+i[3]+'</td></tr>');
                            } else if (tabname == 'Attachments') {
                                $('.tab-content #'+tabname+" table tbody").append('<tr><td>'+i[1]+'</td><td>'+i[2]+'</td><td>'+i[3]+'</td><td class="action-cell"><div class="hide"><a href="http://'+ip+'/EAkimat/'+$('#ServiceCode').val()+'/Attachment?id='+i[0]+'" style="margin-left: 20px" data-action="download" data-type="Document" data-id="' + i[0] + '"><img title="Скачать" alt="Скачать" src="img/ui/save_inactive.png"></a></div></td></tr>');
                            } else if (tabname == 'Processes') {
                                $('.tab-content #'+tabname+" table tbody").append('<tr><td>'+i[1]+'</td><td>'+i[2]+'</td><td>'+i[3]+'</td><td>'+i[4]+'</td></tr>');
                            } else {
                                $('.tab-content #'+tabname+" table tbody").append('<tr><td>'+i[1]+'</td><td>'+i[2]+'</td><td>'+i[3]+'</td></tr>');
                            }
                            
                        });
                    } else if (k == 'Statement') {
                        $.each(v, function (kd, vd) {
                             if (kd == 'Declarant') {
                                $.each(vd, function (k1, v1) {
                                    if(k1 == 'IdentityDate'){
                                        $('#tab1 #' + k1).val(DateFromMSDateForForm(v1));
                                    }else {
                                        $('#tab1 #' + k1).val(v1);
                                    }
                                    
                                });
                            }else if(kd == 'ObuchTypes'){
                                ot_arr = new Map([['p1', 'По состоянию здоровья'],
                                ['p2', 'Временное пребывание за рубежом'],
                                ['p3', 'Обучение за рубежом']]);
                                if(v != null){
                                    $('#' + kd).val(ot_arr.get(vd));
                                }
                            }else if(kd == 'LaborActivityRelation'){
                                lar_arr = new Map([['Related', 'Связано с трудовой деятельностью. Юр.лицо действующее'],
                                ['RelatedEmployerNotExist', 'Связано с трудовой деятельностью. Юр.лицо ликвидировано'],
                                ['NotRelated', 'Не связано с трудовой деятельностью']]);
                                if(v != null){
                                    $('#' + kd).val(lar_arr.get(vd));
                                }
                            }else if(kd == 'RehabilitationService'){
                                rs_arr = new Map([['personal_assistant', 'Индивидуальный помощник'],
                                ['signlang_specialist', 'Специалист жестового языка']]);
                                if(v != null){
                                    $('#' + kd).val(rs_arr.get(vd));
                                }
                            }else if(kd == 'ReasonAcadem'){
                                ra_arr = new Map([['c1', 'По болезни продолжительностью сроком от 6 до 12 месяцевИндивидуальный помощник'],
                                ['c2', 'В случае болезни туберкулезом продолжительностью сроком не более 36 месяцев'],
                                ['c3', 'До достижения ребенком возраста трех лет'],
                                ['c4', 'Призыв на воинскую службу']]);
                                if(v != null){
                                    $('#' + kd).val(ra_arr.get(vd));
                                }
                            }else if(kd == 'ObuchSpecialCategory'){
                                osc_arr = new Map([['c1', 'Ребенок из семьи, имеющей право на получение адресной соц. помощи'],
                                ['c2', 'Ребенок из семьи, не получающей адресную соц. помощь, в которой среднедушевой доход ниже величины прожиточного минимума'],
                                ['c3', 'Ребенок-сирота и ребенок, оставшийся без попечения родителей, проживающий в семьях'],
                                ['c4', 'Ребенок из семьи, требующей экстренной помощи в результате чрезвычайных ситуаций'],
                                ['c5', 'Иная категория обучающегося, определяемая коллегиальным органом управления организации образования']]);
                                if(v != null){
                                    $('#' + kd).val(osc_arr.get(vd));
                                }
                            }else if(kd == 'SchoolDocTypes'){
                                sdt_arr = new Map([['attestat', 'Аттестат'],
                                ['svidetelstvo', 'Свидетельство']]);
                                if(v != null){
                                    $('#' + kd).val(sdt_arr.get(vd));
                                    $('#SchoolDocTypes2').val(vd);
                                }
                            }else if(kd == 'TypeSocialSupport'){
                                tss_arr = new Map([['s1', 'Получение подъемного пособия'],
                                ['s2', 'Получение подъемного пособия и бюджетного кредита на приобретение жилья'],
                                ['s3', 'Получение подъемного пособия и бюджетного кредита на строительство жилья']]);
                                if(v != null){
                                    $('#' + kd).val(tss_arr.get(vd));
                                }
                            }else if(kd == 'StudentCategory'){
                                sc_arr = new Map([['c1', 'Ребенок-сирота'],
                                ['c2', 'Ребенок из многодетной семьи'],
                                ['c3', 'Ребенок-инвалид'],
                                ['c4', 'Ребенок из семьи, в которой среднедушевой доход ниже величины прожиточного минимума'],
                                ['c5', 'Иная категория']]);
                                if(v != null){
                                    $('#' + kd).val(sc_arr.get(vd));
                                }
                            }else if(kd == 'FormaObucheniya'){
                                fo_arr = new Map([['fl', 'Очная'],
                                ['f2', 'Заочная'],
                                ['f3', 'Вечерняя']]);
                                if(v != null){
                                    $('#' + kd).val(fo_arr.get(vd));
                                }
                            }else if(kd == 'ObuchenieNaOsnove'){
                                ono_arr = new Map([['ol', 'Бюджетной'],
                                ['o2', 'Платной']]);
                                if(v != null){
                                    $('#' + kd).val(ono_arr.get(vd));
                                }
                            }else if(kd == 'IsCulturalMonument' || kd == 'WarVeteranRelation' || kd == 'Invalid' || kd == 'HaveQualificationCategory' || kd == 'Familiarization' || kd == 'SpecialEducationalNeeds'){
                                if(vd){
                                    $('#' + kd).prop('checked', true);
                                }
                            } else {
                                $('#recInfo #' + kd).val(vd);
                            }
                        });
                    } else {
                        $('#'+k).val(v);
                    }
                   
                });
            }
        });
    } else {
        $.notify(data.Message, {type: 'danger'});
    }
}