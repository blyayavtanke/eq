$(function() {

  var token = $.session.get("token");

  $.ajax({

    url: "http://"+ip+"/EAkimat/CheckAuth",
    xhrFields: {
      withCredentials: false
   },
    async:true,
    cache: false,
    beforeSend: function(xhr){
     xhr.setRequestHeader("content-type", "application/json");
     xhr.setRequestHeader("Token", token);
   },
    error: function (xhr, ajaxOptions, thrownError) {
        
        if (xhr.status == 401 || xhr.status == 403) {

          Login();

        }
        
    },
     success:function(data) {
        if (!$.session.get("token")) {
            $.session.set("token", data.Token);
            $.session.set("UserName", data.UserName);
        }

        $.ajax({

              url: "content.html",
              cache: false,
              async:true,
              success:function(data) {

                $('#body').html(data);

                LeftMenu(token);

                $('#logout').on('click', function(){

                  Logout(token);
                            
                });
                
              }

            });

     }

  });

});