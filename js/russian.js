{
	"sProcessing":   "Подождите...",
	"sLengthMenu":   "Показать  _MENU_",
	"sZeroRecords":  "Записи отсутствуют.",
	"sInfo":         "Элементов   _START_ - _END_ из _TOTAL_",
	"sInfoEmpty":    "Записи с 0 до 0 из 0",
	"sInfoFiltered": "(отфильтровано из _MAX_ записей)",
	"sInfoPostFix":  "",
	"sSearch":       "Фильтр",
	"sUrl":          "",  
	"oPaginate": {
		"sFirst": "<i class='fa fa-angle-double-left'>",
		"sPrevious": "<i class='fa fa-angle-left'>",
		"sNext": "<i class='fa fa-angle-right'>",
		"sLast": "<i class='fa fa-angle-double-right'>"
	},
	"oAria": {
		"sSortAscending":  ": активировать для сортировки столбца по возрастанию",
		"sSortDescending": ": активировать для сортировки столбцов по убыванию"
	}
}